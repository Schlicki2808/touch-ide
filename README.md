# Touch IDE

A mobile development environment to create apps and scripts right on your phone.

Touch IDE is a touch-optimized, mobile-friendly development environment. Its programming language is easy to use and doesn't require tedious keyboard input. You can create and run your scripts on your mobile device without using a PC. The scripts are converted to pure HTML/JavaScript-based applications in the background and are therefore executed on your device immediately, without further compilation.

Touch IDE is a pure HTML/JavaScript app that should run quite well on your Ubuntu Touch device. Feel free to put an issue if you ran into problems.

## Motivation

The idea of Touch IDE is not completely new. There are several blocky environments like Scratch and there was also TouchDevelop from Microsoft Research which was more like a "real" programming language, but unfortunately retired in 2019. That's why I decided to take this idea and turn it into a whole new project, optimized for the requirements of mobile software development. The result is Touch IDE.

## What you can do with Touch IDE

You can develop scripts and small apps right on your phone. There is JavaScript in the background, but Touch IDE presents a mobile-friendly user interface with its own set of commands. Touch IDE is simple to use but powerful enough to develop scripts that can be used for several things.

## Limitations

Touch IDE is not a high-level programming language like C#. It also does not offer a complete JavaScript environment.
If you want to use a function as a command parameter (like toUpper, toLower etc.), store it in a variable and use the variable in the command parameter.
For a documentation, refer to the [Touch IDE wiki](https://gitlab.com/Schlicki2808/touch-ide/-/wikis/home).

## Download

[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/touchide.daniel)

## License

Copyright (C) 2022 - 2023  Daniel Schlieckmann

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
