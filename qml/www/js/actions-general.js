function setNamespace(namespace, event) {
    currentCommand.Type = "command";
    currentCommand.Expressions = [{ "Id": generateGuid(), "Type": "namespace", "Name": namespace }];
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    updateCommandPanel(event);
    displayCodeContainer();
        if (document.getElementById("cmd-" + currentCommand.Id) != null)
    setTimeout(function() { document.getElementById("cmd-" + currentCommand.Id).scrollIntoView({ "behavior" : "smooth", "block": "center"}); }, 250);
}

function addToNamespace(namespace, commandName, event) {
    currentExpression = null;
    var command = commandsReference.find((item) => item.BelongsToNamespace == namespace && item.Name == commandName);
    var newExpression = {
        "Id": generateGuid(),
        "Type": command.Type,
        "Name": command.Name,
        "Expressions": []
    };
    if (command.Arguments != null && command.Arguments.length > 0) {
        for (var i = 0; i < command.Arguments.length; i++) {
            var subExpression = {
                "Id": generateGuid(),
                "Type": "value",
                "DataType": command.Arguments[i].DataType,
                "Name": command.Arguments[i].DefaultValue,
                "Expressions": []
            }
            newExpression.Expressions.push(subExpression);
            if (i < command.Arguments.length - 1) {
                newExpression.Expressions.push({ "Id": generateGuid(), "Type": "operator", "Name": ",", "Expressions": [] });
            }
        }
        currentParentExpression = newExpression;
        currentExpression = newExpression.Expressions[0];
    }
    if (command.Type == "function") {
        currentCommand.AllowMakeVariable = true;
        currentCommand.VariableType = command.ReturnType;    
    }
    currentCommand.Expressions.push(newExpression);
    updateCommandPanel(event);
    displayCodeContainer();
}

function addCommand(namespace, commandName, event) {
    currentExpression = null;
    var command = commandsReference.find((item) => item.BelongsToNamespace == namespace && item.Name == commandName);
    var newExpression = {
        "Id": generateGuid(),
        "Type": command.Type,
        "Name": command.Name,
        "Namespace": namespace,
        "Expressions": []
    };
    if (command.Arguments != null && command.Arguments.length > 0) {
        for (var i = 0; i < command.Arguments.length; i++) {
            var subExpression = {
                "Id": generateGuid(),
                "Type": "value",
                "DataType": command.Arguments[i].DataType,
                "Name": command.Arguments[i].DefaultValue,
                "Expressions": []
            }
            newExpression.Expressions.push(subExpression);
            if (i < command.Arguments.length - 1) {
                newExpression.Expressions.push({ "Id": generateGuid(), "Type": "operator", "Name": ",", "Expressions": [] });
            }
        }
        //currentParentExpression = newExpression;
        //currentExpression = newExpression.Expressions[0];
    }
    currentParentExpression.Expressions.push({ "Id": generateGuid(), "Type": "operator", "Name": "&rarr;", "Expressions": [] });
    currentParentExpression.Expressions.push(newExpression);
    updateCommandPanel(event);
    displayCodeContainer();
}

function editOk() {
    closeModalInput();
    currentExpression.Name = document.getElementById("modal-input-value").value;
    currentExpression.Type = "value";
    if (document.getElementById("modal-input-value").type == "text")
        currentExpression.DataType = "string";
    else if (document.getElementById("modal-input-value").type == "number")
        currentExpression.DataType = "number";
    displayCodeContainer();
}

function addString(event) {
    currentParentExpression.Expressions.push({ "Id" : generateGuid(), "Type": "operator", "Name": "+" });
    currentParentExpression.Expressions.push({ "Id" : generateGuid(), "Type": "value", "Name": "New string", "DataType": "string" });
    selectExpression(currentCommand.Id, currentParentExpression.Expressions[currentParentExpression.Expressions.length - 1].Id, event);
    showModalInput('Edit string:', currentExpression.Name, 'text', editOk, event);
}

function addNumber(event) {
    currentParentExpression.Expressions.push({ "Id" : generateGuid(), "Type": "operator", "Name": "+" });
    currentParentExpression.Expressions.push({ "Id" : generateGuid(), "Type": "value", "Name": "0", "DataType": "number" });
    selectExpression(currentCommand.Id, currentParentExpression.Expressions[currentParentExpression.Expressions.length - 1].Id, event);
}

function addNumberOp(op, event) {
    currentParentExpression.Expressions.push({ "Id" : generateGuid(), "Type": "operator", "Name": op });
    currentParentExpression.Expressions.push({ "Id" : generateGuid(), "Type": "value", "Name": "1", "DataType": "number" });
    selectExpression(currentCommand.Id, currentParentExpression.Expressions[currentParentExpression.Expressions.length - 1].Id, event);
}

function addOperator(name) {
    currentCommand.Expressions.push({ "Id" : generateGuid(), "Type": "operator", "Name": name })
    displayCodeContainer();
}

function addStringForCondition(event) {
    currentCommand.Expressions.push({ "Id" : generateGuid(), "Type": "value", "Name": "New string", "DataType": "string" });
    selectExpression(currentCommand.Id, currentCommand.Expressions[currentCommand.Expressions.length - 1].Id, event);
    showModalInput('Edit string:', currentExpression.Name, 'text', editOk, event);
}

function addNumberForCondition(event) {
    currentCommand.Expressions.push({ "Id" : generateGuid(), "Type": "value", "Name": "0", "DataType": "number" });
    selectExpression(currentCommand.Id, currentCommand.Expressions[currentCommand.Expressions.length - 1].Id, event);
    showModalInput('Edit number:', currentExpression.Name, 'number', editOk, event);
}

function setValue(value, dataType, event) {
    currentExpression.Name = value;
    currentExpression.Type = "value";
    currentExpression.DataType = dataType;
    updateCommandPanel(event);
    displayCodeContainer();
}

function setComment() {
    closeModalInput();
    currentCommand.Comment = document.getElementById("modal-input-value").value;
    displayCodeContainer();
}

function deleteLastSubExpression(event) {
    if (currentParentExpression != null && currentParentExpression.Expressions != null) {
        if (currentParentExpression.Expressions.length > 1) {
            currentParentExpression.Expressions.pop();
            if (currentParentExpression.Expressions.length > 0 && currentParentExpression.Expressions[currentParentExpression.Expressions.length - 1].Type == "operator")
                currentParentExpression.Expressions.pop();
            currentExpression = null;
            selectExpression(currentCommand.Id, currentParentExpression.Expressions[currentParentExpression.Expressions.length - 1].Id, event);
        }
    }
    else if (currentCommand != null && currentCommand.Expressions != null && currentCommand.Expressions.length > 1) {
        currentCommand.Expressions.pop();
        if (currentCommand.Expressions.length > 0)
            selectExpression(currentCommand.Id, currentCommand.Expressions[currentCommand.Expressions.length - 1].Id, event);
    }
}

function recreateFileInput() {
    var fileInput = document.getElementById("file");
    if (fileInput != null)
        fileInput.parentNode.removeChild(fileInput);
    fileInput = document.createElement("input");
    fileInput.type = "file";
    fileInput.id = "file";
    fileInput.style.display = "none";
    document.body.appendChild(fileInput);
    return fileInput;
}