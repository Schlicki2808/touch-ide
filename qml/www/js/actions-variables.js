var currentDataType = "";
var currentVariableSelectEvent = null;
var currentEvent = null;
var currentSelectedVariable = "";
var currentVariableClass = "";
var currentVariableNameClass = "";
var allowCloseVariablesList = true;

function createVariable(variableName, dataType, defaultValue, event) {
    var expressions = [
        {
            "Id": generateGuid(),
            "Type": "variable-declaration",
            "Name": getFreeVariableName(variableName),
            "Expressions": [
                {
                    "Id": generateGuid(),
                    "Type": "value",
                    "DataType": dataType,
                    "Name": defaultValue
                }
            ]
        }
    ];
    currentCommand.Type = "command";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = dataType;
    selectExpression(currentCommand.Id, expressions[0].Expressions[0].Id, event);
    updateCommandPanel(event);
    displayCodeContainer();
}

function createNamespace(variableName, commandName, dataType, event) {
    var expressions = [
        {
            "Id": generateGuid(),
            "Type": "variable-declaration",
            "Name": getFreeVariableName(variableName),
            "Expressions": [
                {
                    "Id": generateGuid(),
                    "Type": "namespace",
                    "Name": dataType
                },
                {
                    "Id": generateGuid(),
                    "Type": "method",
                    "Name": commandName
                }
            ]
        }
    ];
    currentCommand.Type = "command";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = dataType;
    currentExpression = null;
    updateCommandPanel(event);
    displayCodeContainer();
}

function iterateExpressionForVarRename(expression, oldVariableName, newVariableName) {
    if ((expression.Type == "variable" || expression.Type == "variable-declaration" || expression.Type == "variable-assign" || expression.Type == "variable-assign-namespace") && (expression.Name == oldVariableName))
        expression.Name = newVariableName;
    
    if (expression.ClassVariable != null && expression.ClassVariable == oldVariableName)
        expression.ClassVariable = newVariableName;
    
    if (expression.Expressions != null && expression.Expressions.length > 0) {
        for (var e = 0; e < expression.Expressions.length; e++) {
            iterateExpressionForVarRename(expression.Expressions[e], oldVariableName, newVariableName);
        }
    }
}

function renameVariable() {
    closeModalInput();
    var oldVariableName = currentDefaultValue;
    var newVariableName = getFreeVariableName(document.getElementById("modal-input-value").value);

    for (var i = 0; i < commands.length; i++) {
        for (var e = 0; e < commands[i].Expressions.length; e++) {
            if (commands[i].ClassVariable != null && commands[i].ClassVariable == oldVariableName)
                commands[i].ClassVariable = newVariableName;
            iterateExpressionForVarRename(commands[i].Expressions[e], oldVariableName, newVariableName);
        }    
    }
    displayCodeContainer();
}

function getVariableCommand(func, varName) {
    return func.Commands.find((item) => item.Expressions.find((item) => item.Type == "variable-declaration" && item.Name == varName) != null);
}

function getVariableTypeByClass(className, propertyName) {
    var result = "";
    var classFunc = functions.find((item) => item.Name == className);
    if (classFunc != null) {
        var propCommand = classFunc.Commands.find((item) => item.Expressions != null && item.Expressions.length > 0 && item.Expressions[0].Name.replace(/ /g, "_") == propertyName.replace(/ /g, "_"));
        if (propCommand != null) {
            result = propCommand.Expressions[0].Expressions[0].DataType;
            if (result == null)
                result = propCommand.Expressions[0].Expressions[0].Name;
        }
    }
    return result;
}

function getFreeVariableName(varName) {
    var existingVar = getVariableCommand(currentFunction, varName);
    if (existingVar != null) {
        var counter = 1;
        var basicVarName = varName;
        while (existingVar != null) {
            varName = basicVarName + " " + counter.toString();
            existingVar = getVariableCommand(currentFunction, varName);
            counter += 1;
        }
    }
    return varName;
}

function getClass(variableType) {
    return functions.find((item) => item.Type == "class" && item.Name == variableType);
}

function showVariablesList(dataType, variableSelectEvent, event) {
    currentDataType = dataType;
    currentVariableSelectEvent = variableSelectEvent;
    currentEvent = event;
    currentVariableClass = "";
    currentVariableNameClass = "";
    var vars = [];
    for (var i = 0; i < commands.length; i++) {
        if (commands[i].Expressions != null && 
            commands[i].Expressions.length > 0 && 
            commands[i].Expressions[0].Type == "variable-declaration" &&
            (commands[i].VariableType == dataType || dataType == "*" || dataType == "object" || getClass(commands[i].VariableType) != null || (dataType == "collection" && (commands[i].VariableType == "collection" || commands[i].VariableType.endsWith(" list")) ))) {
            var existingVar = vars.find((item) => item.Name == commands[i].Expressions[0].Name && item.DataType == commands[i].VariableType);
            if (existingVar == null) {
                vars.push({ "Name": commands[i].Expressions[0].Name, "DataType": commands[i].VariableType })
            }
        }
    }
    for (var i = 0; i < currentFunction.Parameters.length; i++) {
        if (currentFunction.Parameters[i].DataType == dataType || dataType == "*") {
            vars.push({ "Name": currentFunction.Parameters[i].Name, "DataType": currentFunction.Parameters[i].DataType })
        }
    }
    
    var varListHTML = "<div class=\"panel-header panel-header-variables\">Variables</div>";
    for (var i = 0; i < vars.length; i++) {
        varListHTML += "<div onclick=\"variableSelected('" + vars[i].Name + "', '" + vars[i].DataType + "', event)\"><span class=\"variable-name-list\">" + vars[i].DataType + "</span> " + vars[i].Name;
        if (getClass(vars[i].DataType) != null && dataType != "object") {
            varListHTML += " ...";
        }
        varListHTML += "</div>";
    }
    document.getElementById("variables-list").innerHTML = varListHTML;
    document.getElementById("variables-list-container").style.display = "";
    event.stopImmediatePropagation();
}

function variableSelected(variableName, dataType) {
    var classFunc = getClass(dataType);
    if (classFunc != null && currentDataType != "object") {
        allowCloseVariablesList = false;
        currentVariableClass = classFunc.Name;
        currentVariableNameClass = variableName;
        var varListHTML = "<div class=\"panel-header panel-header-variables\">Variables</div>";
        for (var i = 0; i < classFunc.Commands.length; i++) {
            if (classFunc.Commands[i].Expressions != null && classFunc.Commands[i].Expressions[0] != null) {
                var exp = classFunc.Commands[i].Expressions[0];
                var dataType = exp.Expressions[0].DataType;
                if (dataType == null)
                    dataType = exp.Expressions[0].Name;
                if (dataType == currentDataType || currentDataType == "*" || currentDataType == "object") {
                    varListHTML += "<div onclick=\"variableSelected('" + exp.Name + "', '" + dataType + "', event)\"><span class=\"variable-name-list\">" + dataType + "</span> " + exp.Name;
                    if (getClass(exp.Expressions[0].DataType) != null) {
                        varListHTML += " ...";
                    }
                    varListHTML += "</div>";
                }
            }
        }
        document.getElementById("variables-list").innerHTML = varListHTML;
        window.setTimeout(function() { allowCloseVariablesList = true }, 200);
    }
    else {
        currentSelectedVariable = variableName;
        currentDataType = dataType;
        currentVariableSelectEvent.call();
    }
}

function closeVariablesList() {
    if (allowCloseVariablesList) {
        document.getElementById("variables-list-container").style.display = "none";
    }
}

function convertToVariable() {
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    var expressions = [
        {
            "Id": generateGuid(),
            "Type": "variable-assign",
            "Name": currentSelectedVariable,
            "Expressions": currentCommand.Expressions
        }
    ];
    currentCommand.Expressions = expressions;
    currentExpression = currentCommand.Expressions[0].Expressions[0];
    updateCommandPanel(currentEvent);
    displayCodeContainer();
}

function convertToFunction() {
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    currentCommand.ClassName = currentVariableClass;
    currentCommand.ClassVariable = currentVariableNameClass;
    var expressions = [
        {
            "Id": generateGuid(),
            "Type": "variable-assign-namespace",
            "Name": currentSelectedVariable,
            "Expressions": currentCommand.Expressions
        }
    ];
    currentCommand.Expressions = expressions;
    updateCommandPanel(currentEvent);
    displayCodeContainer();
}

function makeVariable(event) {
    var newVariableName = "item";
    if (currentCommand.Expressions != null) {
        if (currentCommand.Expressions.length > 1) {
            var part1 = currentCommand.Expressions[0].Name;
            var part2 = currentCommand.Expressions[1].Name.split(" ");
            newVariableName = part1 + " " + part2[part2.length - 1];
        }
        else if (currentCommand.Expressions.length == 1) {
            newVariableName = currentCommand.Expressions[0].Name + " result";
        }
    }
    currentCommand.AllowMakeVariable = false;
    var expressions = [
        {
            "Id": generateGuid(),
            "Type": "variable-declaration",
            "Name": getFreeVariableName(newVariableName),
            "Expressions": currentCommand.Expressions
        }
    ];
    currentCommand.Expressions = expressions;
    updateCommandPanel(event);
    displayCodeContainer();
    event.stopImmediatePropagation();
}

function addVariable() {
    if (currentParentExpression != null && currentParentExpression.Expressions != null) {
        currentParentExpression.Expressions.push({ "Id" : generateGuid(), "Type": "operator", "Name": "+" });
        currentParentExpression.Expressions.push({ "Id" : generateGuid(), "Type": "variable", "Name": currentSelectedVariable, "DataType": currentDataType, "ClassName": currentVariableClass, "ClassVariable": currentVariableNameClass });
        selectExpression(currentCommand.Id, currentParentExpression.Expressions[currentParentExpression.Expressions.length - 1].Id, currentEvent);    
    }
    else if (currentCommand != null && currentCommand.Expressions != null) {
        currentCommand.Expressions.push({ "Id" : generateGuid(), "Type": "variable", "Name": currentSelectedVariable, "DataType": currentDataType, "ClassName": currentVariableClass, "ClassVariable": currentVariableNameClass });
        selectExpression(currentCommand.Id, currentCommand.Expressions[currentCommand.Expressions.length - 1].Id, currentEvent);    
    }
}

function setCurrentExpressionToVariable() {
    currentExpression.Type = "variable";
    currentExpression.Name = currentSelectedVariable;
    currentExpression.DataType = currentDataType;
    currentExpression.ClassName = currentVariableClass;
    currentExpression.ClassVariable = currentVariableNameClass;
    updateCommandPanel(currentEvent);
    displayCodeContainer();
}

function setVariableAssignment() {
    currentCommand.Type = "command";
    currentCommand.AllowMakeVariable = false;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    convertToFunction();
}

function setCollectionInForEach() {
    currentCommand.Expressions[1].Name = currentSelectedVariable;
    if (currentDataType.endsWith(" list"))
        currentCommand.VariableType = currentDataType.replace(" list", "");
    else
        currentCommand.VariableType = "string";
    updateCommandPanel(currentEvent);
    displayCodeContainer();
}

function setListInForEach() {
    currentCommand.Expressions[1].Name = currentSelectedVariable;
    currentCommand.Expressions[1].ClassName = currentVariableClass;
    currentCommand.Expressions[1].ClassVariable = currentVariableNameClass;
    if (currentDataType.endsWith(" list"))
        currentCommand.VariableType = currentDataType.replace(" list", "");
    else
        currentCommand.VariableType = "string";
    updateCommandPanel(currentEvent);
    displayCodeContainer();
}