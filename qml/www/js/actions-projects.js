var selectedProjectId = "";

function getObjects(name) {
    var jsonString = localStorage.getItem(name);
    if (jsonString != null)
        return JSON.parse(jsonString);
    else
        return [];
}

function putObjects(name, objectList) {
    localStorage.setItem(name, JSON.stringify(objectList));
}

function getProjects() {
    projects = getObjects("projects");
    var container = document.getElementById("projects-container");
    var containerHTML = "";
    if (projects.length > 0) {
        for (var i = 0; i < projects.length; i++) {
            containerHTML += "<div onclick=\"selectProject('" + projects[i].Id + "', event)\"><div>" + projects[i].Name + "</div>";
            if (projects[i].Sync)
                containerHTML += "<div class=\"cloud-icon\"></div>";
            containerHTML += "</div>";
        }
    }
    else {
        containerHTML = "It's empty here. Click on \"Create new project\" to start your first Touch IDE project."
    }
    container.innerHTML = containerHTML;
}

function backupProjects() {
    var projectsJson = localStorage.getItem('projects');
    var element1 = document.createElement('a');
    //element1.setAttribute('href', 'data:application/json;charset=utf-8,' + encodeURIComponent(projectsJson));
    element1.setAttribute('href', 'data:text/plain;,' + encodeURIComponent(projectsJson));
    element1.setAttribute('download', "touch-ide-projects.json");
  
    element1.style.display = 'none';
    document.body.appendChild(element1);
  
    element1.click();
  
    document.body.removeChild(element1);
}

function restoreProjects() {
    //console.log("RestoreProjects");
    var fileInput = recreateFileInput();
    fileInput.addEventListener("change", function() { restoreProjects_do(); });
    fileInput.click();
}

function restoreProjects_do() {
    var input = document.getElementById("file");
    if (input.files.length > 0) {
        var reader = new FileReader();
        reader.addEventListener("load", function() {
            projects = JSON.parse(reader.result);
            putObjects("projects", projects);
            getProjects();
        });
        reader.readAsText(input.files[0]);
    }
}

function handleProjectButton(event) {
    showModalInput('Name of new project:', 'my awesome project', 'text', createProject, event);
}

function createProject(event) {
    closeModalInput();
    var projectName = document.getElementById("modal-input-value").value;
    var newProject = {
        "Id": generateGuid(),
        "Name": projectName,
        "Functions" : [
            { "Id": generateGuid(), "Type": "function", "Name": "main", "Commands": [ generateEmptyCommand() ], "Parameters": [] }
        ]
    };
    projects.push(newProject);
    putObjects("projects", projects);
    getProjects();
    selectProject(newProject.Id, event);
}

function selectProject(id, event) {
    selectedProjectId = id;
    document.getElementById("projects").style.display = "none";
    currentProject = projects.find((item) => item.Id == id);
    if (syncenabled && currentProject.Sync)
        getCurrentProjectFromCloud(event);
    else
        selectProject_finish(event);
}

function selectProject_finish(event) {
    document.getElementById("menu-title").innerText = currentProject.Name;
    functions = currentProject.Functions;
    selectionMode = false;
    document.getElementById("ide-default").style.display = "";
    document.getElementById("ide-selection-mode").style.display = "none";
    document.getElementById("output-window").contentWindow.document.open();
    document.getElementById("output-window").contentWindow.document.write("<!DOCTYPE html><html><body></body></html>");
    document.getElementById("output-window").contentWindow.document.close();
    if (desktopMode)
        document.getElementById("output-container").style.display = '';
    document.getElementById("liveexec").checked = liveexecution;
    setSyncMenuItem();
    selectedCommands = [];
    copiedCommands = [];
    for (var i = 0; i < currentProject.Functions.length; i++) {
        if (currentProject.Functions[i].Type == null) {
            currentProject.Functions[i].Type = "function";
        }
    }
    selectFunction(currentProject.Functions[0].Id, event);
    displayCodeContainer();
}

function getCurrentProjectFromCloud(event) {
    var username = localStorage.getItem("username");
    var userappid = localStorage.getItem("appid");
    var scriptappid = currentProject.Id;
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (req.responseText != null && req.responseText.length > 0) {
                currentProject = JSON.parse(req.responseText);
                var currentProjectIdx = projects.findIndex((item) => item.Id == currentProject.Id);
                if (currentProjectIdx > -1)
                    projects[currentProjectIdx] = currentProject;
                putObjects("projects", projects);
                getProjects();
                currentProject = projects.find(item => item.Id == currentProject.Id);
            }
            selectProject_finish(event);
        }
    }
    req.onerror = function(e) {
        showModalConfirm("Warning: Project could not be fetched from cloud. Disable sync for this project?", switchSyncFromModal);
        selectProject_finish(event);
    }
    req.onabort = function(e) {
        console.log('ABORT');
    }
    req.open("POST", "https://touchide.net?u=" + encodeURIComponent(username) + "&a=" + encodeURIComponent(userappid) + "&s=" + encodeURIComponent(scriptappid), true);
    req.send();
}

function setSyncMenuItem() {
    if (syncenabled) {
        if (currentProject.Sync)
            document.getElementById("sync-menu-item").innerHTML = "<span class=\"icon btn-sync\"></span>&nbsp;Disable sync</div>";
        else
            document.getElementById("sync-menu-item").innerHTML = "<span class=\"icon btn-sync\"></span>&nbsp;Enable sync</div>";
    }
}

function deleteProject() {
    closeModalConfirm();
    var index = projects.findIndex((item) => item.Id == selectedProjectId);
    projects.splice(index, 1);
    putObjects("projects", projects);
    getProjects();
    if (document.getElementById("projects").style.display == "none") {
        document.getElementById("projects").style.display = "";
    }
}

function renameProject() {
    closeModalInput();
    var projectName = document.getElementById("modal-input-value").value;
    currentProject.Name = projectName;
    putObjects("projects", projects);
    getProjects();
    document.getElementById("menu-title").innerText = currentProject.Name;
}

function switchSyncFromModal()
{
    switchSync();
    closeModalConfirm();
}

function switchSync() {
    if (currentProject.Sync == null)
        currentProject.Sync = true;
    else
        currentProject.Sync = !currentProject.Sync;
    putObjects("projects", projects);
    getProjects();
    setSyncMenuItem();
}