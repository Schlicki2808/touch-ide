var currentEvent = null;
var currentDefaultValue = "";
var recreateCodeContainer = false;

function showModalInput(prompt, defaultValue, inputType, okEvent, event) {
    currentEvent = okEvent;
    currentDefaultValue = defaultValue;
    recreateCodeContainer = false;
    if (document.getElementById("code-container").style.bottom == "220px")
        recreateCodeContainer = true;
    document.getElementById("command-panel-container").style.display = "none";
    document.getElementById("code-container").style.bottom = "0";
    document.getElementById("modal-input-prompt").innerText = prompt;
    document.getElementById("modal-input").style.display = "";
    document.getElementById("modal-input-value").type = inputType;
    document.getElementById("modal-input-value").value = defaultValue;
    document.getElementById("ok-button").addEventListener("click", okEvent);
    document.getElementById("modal-input-value").focus();
    if (inputType == "text")
        document.getElementById("modal-input-value").setSelectionRange(0, document.getElementById("modal-input-value").value.length);
    event.stopImmediatePropagation();
}

function closeModalInput() {
    document.getElementById("command-panel-container").style.display = "";
    if (recreateCodeContainer)
        document.getElementById("code-container").style.bottom = "220px";
    document.getElementById("ok-button").removeEventListener("click", currentEvent);
    document.getElementById("modal-input").style.display = "none";
    if (currentCommand != null && document.getElementById("cmd-" + currentCommand.Id) != null)
        setTimeout(function() { document.getElementById("cmd-" + currentCommand.Id).scrollIntoView({ "behavior" : "smooth", "block": "center"}); }, 250);
}

function showModalConfirm(prompt, yesEvent) {
    currentEvent = yesEvent;
    document.getElementById("command-panel-container").style.display = "none";
    document.getElementById("code-container").style.bottom = "0";
    document.getElementById("modal-confirm-prompt").innerText = prompt;
    document.getElementById("modal-confirm").style.display = "";
    document.getElementById("yes-button").addEventListener("click", yesEvent);
}

function closeModalConfirm() {
    document.getElementById("command-panel-container").style.display = "";
    document.getElementById("code-container").style.bottom = "0";
    document.getElementById("yes-button").removeEventListener("click", currentEvent);
    document.getElementById("modal-confirm").style.display = "none";
    if (currentCommand != null && document.getElementById("cmd-" + currentCommand.Id) != null)
        setTimeout(function() { document.getElementById("cmd-" + currentCommand.Id).scrollIntoView({ "behavior" : "smooth", "block": "center"}); }, 250);
}

function showModalDialog(prompt) {
    document.getElementById("command-panel-container").style.display = "none";
    document.getElementById("code-container").style.bottom = "0";
    document.getElementById("modal-dialog-prompt").innerHTML = prompt;
    document.getElementById("modal-dialog").style.display = "";
}

function closeModalDialog() {
    document.getElementById("command-panel-container").style.display = "";
    document.getElementById("code-container").style.bottom = "0";
    document.getElementById("modal-dialog").style.display = "none";
    if (currentCommand != null && document.getElementById("cmd-" + currentCommand.Id) != null)
        setTimeout(function() { document.getElementById("cmd-" + currentCommand.Id).scrollIntoView({ "behavior" : "smooth", "block": "center"}); }, 250);
}

function showWaitDialog() {
    document.getElementById("wait-dialog").style.display = "";
}

function closeWaitDialog() {
    document.getElementById("wait-dialog").style.display = "none";
}