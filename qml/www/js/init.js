function init(event) {
    initSession();
    getProjects();
    setSync();
    initOptions();
    setOptions();
}

function initSession() {
    sessionId = localStorage.getItem("SessionID");
    if (sessionId == null || sessionId.length == 0) {
        sessionId = generateGuid();
        localStorage.setItem("SessionID", sessionId);
    }
}

function setSync() {
    var username = localStorage.getItem("username");
    var appid = localStorage.getItem("appid");
    if (username != null && appid != null && username.length > 0 && appid.length > 0) {
        syncenabled = true;
        document.getElementById("sync-menu-item").style.display = "";
    }
    else {
        syncenabled = false;
        document.getElementById("sync-menu-item").style.display = "none";
    }
}

function initOptions() {
    var optionsJson = localStorage.getItem("options");
    if (optionsJson != null && optionsJson.length > 0)
        options = JSON.parse(optionsJson);
    if (options == null) {
        options = {
            smallerFont: false,
            enableSplitView: true,
            wrapLines: false,
            apikey: ''
        };
        localStorage.setItem("options", JSON.stringify(options));
    }
}

function setOptions() {
    if (options.smallerFont)
        document.getElementById("code-container").style.fontSize = "1.0rem";
    else
        document.getElementById("code-container").style.fontSize = "";

    if (options.enableSplitView) {
        desktopMode = (window.innerWidth > window.innerHeight);
        if (desktopMode) {
            var css = document.querySelectorAll("head > link");
            css[1].href = "css/ide-landscape.css";
        }
    }
    else {
        desktopMode = false;
        var css = document.querySelectorAll("head > link");
        css[1].href = "";
    }

    if (options.wrapLines) {
        var css = document.querySelectorAll("head > link");
        css[2].href = "css/ide-command-wrap.css";
    }
    else {
        var css = document.querySelectorAll("head > link");
        css[2].href = "";
    }
}