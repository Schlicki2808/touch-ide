var currentNamespace = "";
var currentClass = "";
var currentClassVariableName = "";
var currentVariableName = "";

function exportJs() {
    var jsString = "var currentPageName = \"Main Page\";\n"
                 + "var dialogClosed = false;\n"
                 + "var globDialogValue = \"\";\n"
                 + "var windowWidth = window.innerWidth;\n"
                 + "var windowHeight = window.innerHeight;\n"
                 + "var globImage = { \"width\": 0, \"height\": 0, \"zoom\": 100, \"customWidth\": 0, \"customHeight\": 0, \"data\": \"\" };\n"
                 + "var globScanFinished = true;\n"
                 + "var globScannedCode = \"\";\n"
                 + "var globPosition = null;\n"
                 + "var checkGetLocationInProgress = false;\n"
                 + "var lastDeviceId = \"\";\n"
                 + "var lastTouchX = 0;\n"
                 + "var lastTouchY = 0;\n"
                 + "var pointerDown = false;\n"
                 + "var maps = [];\n"
                 + "var startDate = new Date();\n"
                 + "var storageMode = 'G';\n"
                 + "var appMenuItems = [];\n"
                 + "var appMenuIndex = -1;\n"
                 + "var appMenuOpen = false;\n"
                 + "var darkMode = false;\n"
                 + "var sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay));\n\n"
                 + "window.addEventListener(\"resize\", handleResize);\n"
                 + "window.addEventListener(\"mousedown\", handleMouseDown);\n"
                 + "window.addEventListener(\"mousemove\", handleMouseMove);\n"
                 + "window.addEventListener(\"mouseup\", handleMouseUp);\n"
                 + "window.addEventListener(\"touchstart\", handleTouchStart);\n"
                 + "window.addEventListener(\"touchmove\", handleTouchMove);\n"
                 + "window.addEventListener(\"touchend\", handleTouchEnd);\n"
                 + "\n"
                 + "if (typeof String.prototype.replaceAll2 !== \"function\") {\n"
                 + "    String.prototype.replaceAll2 = function (oldValue, newValue) {\n"
                 + "        var result = this;\n"
                 + "        while (result.includes(oldValue.toLowerCase())) {\n"
                 + "            result = result.replace(oldValue.toLowerCase(), newValue);\n"
                 + "        }\n"
                 + "        while (result.includes(oldValue.toUpperCase())) {\n"
                 + "            result = result.replace(oldValue.toUpperCase(), newValue);\n"
                 + "        }\n"
                 + "        return result;\n"
                 + "    };\n"
                 + "}\n"
                 + "\n"
                 + "function closeDialog() {\n"
                 + "document.getElementById(\"modal-input\").style.display = \"none\";\n"
                 + "dialogClosed = true;\n"
                 + "};\n"
                 + "\n"
                 + "function closeConfirm(value) {\n"
                 + "document.getElementById(\"modal-confirm\").style.display = \"none\";\n"
                 + "globDialogValue = value;\n"
                 + "dialogClosed = true;\n"
                 + "};\n"
                 + "\n"
                 + "function handleResize() {"
                 + "windowWidth = window.innerWidth;\n"
                 + "windowHeight = window.innerHeight;\n"
                 + "}\n"
                 + "\n"
                 + "function handleMouseDown(evt) {"
                 + "pointerDown = true;\n"
                 + "}\n"
                 + "\n"
                 + "function handleMouseMove(evt) {"
                 + "lastTouchX = evt.clientX;\n"
                 + "lastTouchY = evt.clientY;\n"
                 + "}\n"
                 + "\n"
                 + "function handleMouseUp(evt) {"
                 + "pointerDown = false;\n"
                 + "}\n"
                 + "\n"
                 + "function handleTouchStart(evt) {"
                 + "pointerDown = true;\n"
                 + "}\n"
                 + "\n"
                 + "function handleTouchMove(evt) {"
                 + "lastTouchX = evt.touches[0].clientX;\n"
                 + "lastTouchY = evt.touches[0].clientY;\n"
                 + "}\n"
                 + "\n"
                 + "function handleTouchEnd(evt) {"
                 + "pointerDown = false;\n"
                 + "}\n"
                 + "\n"
                 + "\n"
                 + "function selectFile (contentType){\n"
                 + "    return new Promise(resolve => {\n"
                 + "        var input = document.getElementById('file-select');\n"
                 + "        //input.accept = contentType;\n"
                 + "\n"
                 + "        input.onchange = _ => {\n"
                 + "            let files = Array.from(input.files);\n"
                 + "            resolve(files[0]);\n"
                 + "        };\n"
                 + "\n"
                 + "        input.click();\n"
                 + "    });\n"
                 + "}\n"
                 + "\n"
                 + "function getImageFromUrl(url) {\n"
                 + "    var img = new Image();\n"
                 + "    img.setAttribute('crossOrigin', 'anonymous');\n"
                 + "    img.onload = function() {\n"
                 + "        var canvas = document.createElement(\"canvas\");\n"
                 + "        canvas.width = this.width;\n"
                 + "        canvas.height = this.height;\n"
                 + "\n"
                 + "        var ctx = canvas.getContext(\"2d\");\n"
                 + "        ctx.drawImage(this, 0, 0);\n"
                 + "\n"
                 + "        var dataURL = canvas.toDataURL(\"image/png\");\n"
                 + "        globImage = { \"width\": this.width, \"height\": this.height, \"zoom\": 100, \"customWidth\": 0, \"customHeight\": 0, \"data\": dataURL };\n"
                 + "    };\n"
                 + "    img.src = url;\n"
                 + "}\n"
                 + "function initCamera() {\n"
                 + "    document.getElementById(\"camera\").style.display = \"\";\n"
                 + "    lastDeviceId = \"\";\n"
                 + "    switchCamera();\n"
                 + "}\n"
                 + "function takePicture() {\n"
                 + "    var video = document.querySelector(\"video\");\n"
                 + "    var canvas = document.querySelector(\"canvas\");\n"
                 + "    var img = document.querySelector(\"img\");\n"
                 + "    canvas.width = video.videoWidth;\n"
                 + "    canvas.height = video.videoHeight;\n"
                 + "    canvas.getContext(\"2d\").drawImage(video, 0, 0);\n"
                 + "    var dataURL = canvas.toDataURL(\"image/png\");\n"
                 + "    globImage = { \"width\": canvas.width, \"height\": canvas.height, \"zoom\": 100, \"customWidth\": 0, \"customHeight\": 0, \"data\": dataURL };\n"
                 + "    closeCamera();\n"
                 + "}\n"
                 + "\n"
                 + "function switchCamera() {\n"
                 + "    navigator.mediaDevices.enumerateDevices()\n"
                 + "        .then(handleEnumerateDevices);\n"
                 + "}\n"
                 + "\n"
                 + "function handleSuccess(stream) {\n"
                 + "    var video = document.querySelector(\"video\");\n"
                 + "    video.srcObject = stream;\n"
                 + "}\n"
                 + "\n"
                 + "function handleError(error) {\n"
                 + "    console.error(\"Error: \", error);\n"
                 + "}\n"
                 + "function closeCamera() {\n"
                 + "    document.getElementById(\"camera\").style.display = \"none\";\n"
                 + "    var video = document.querySelector(\"video\");\n"
                 + "    video.srcObject = null;\n"
                 + "}\n"
                 + "\n"
                 + "function handleEnumerateDevices(devices) {\n"
                 + "    var videoDevices = devices.filter(device => device.kind === 'videoinput');\n"
                 + "    var videoDevice = videoDevices.find((item) => item.deviceId != lastDeviceId);\n"
                 + "    if (videoDevice == null)\n"
                 + "        videoDevice = videoDevices[0];\n"
                 + "    lastDeviceId = videoDevice.deviceId;\n"
                 + "    navigator.mediaDevices.getUserMedia({video: { deviceId: lastDeviceId }})\n"
                 + "        .then(handleSuccess)\n"
                 + "        .catch(handleError);\n"
                 + "}\n"
                 + "function download(data, name) {\n"
                 + "    var element = document.createElement('a');\n"
                 + "    element.setAttribute('href', data);\n"
                 + "    element.setAttribute('download', name);\n"
                 + "    element.style.display = 'none';\n"
                 + "    document.body.appendChild(element);\n"
                 + "    element.click();\n"
                 + "    document.body.removeChild(element);\n"
                 + "}\n"
                 + "function actionLink(data) {\n"
                 + "    var element = document.createElement('a');\n"
                 + "    element.setAttribute('href', data);\n"
                 + "    element.setAttribute('target', '_new');\n"
                 + "    element.style.display = 'none';\n"
                 + "    document.body.appendChild(element);\n"
                 + "    element.click();\n"
                 + "    document.body.removeChild(element);\n"
                 + "}\n"
                 + "function getGeoLocation() {\n"
                 + "    checkGetLocationInProgress = true;\n"
                 + "    if (navigator.geolocation) {\n"
                 + "        navigator.geolocation.getCurrentPosition(showPosition, positionError);\n"
                 + "    }\n"
                 + "    else {\n"
                 + "        checkGetLocationInProgress = false;\n"
                 + "        globPosition = {};\n"
                 + "        document.getElementById(\"content\").innerHTML += \"<div style='color:red;font-weight:800;'>Geolocation is not supported on this device.</div>\";"
                 + "    }\n"
                 + "}\n"
                 + "\n"
                 + "function watchGeoLocation() {\n"
                 + "    checkGetLocationInProgress = true;\n"
                 + "    if (navigator.geolocation) {\n"
                 + "        navigator.geolocation.watchPosition(showPosition);\n"
                 + "    }\n"
                 + "    else {\n"
                 + "        checkGetLocationInProgress = false;\n"
                 + "        globPosition = {};\n"
                 + "        document.getElementById(\"content\").innerHTML += \"<div style='color:red;font-weight:800;'>Geolocation is not supported on this device.</div>\";"
                 + "    }\n"
                 + "}\n"
                 + "\n"
                 + "function showPosition(position) {\n"
                 + "    checkGetLocationInProgress = false;\n"
                 + "    globPosition = position;\n"
                 + "}\n"
                 + "\n"
                 + "function positionError() {\n"
                 + "    checkGetLocationInProgress = false;\n"
                 + "    document.getElementById(\"content\").innerHTML += \"<div style='color:red;font-weight:800;'>Position could not be fetched.</div>\";"
                 + "}\n"
                 + "\n"
                 + "function initialize_map(mapLat, mapLng, mapDefaultZoom, targetDiv) {\n"
                 + "    var map = new ol.Map({\n"
                 + "    target: 'map' + targetDiv,\n"
                 + "    layers: [\n"
                 + "        new ol.layer.Tile({\n"
                 + "            source: new ol.source.OSM({\n"
                 + "                    url: \"https://a.tile.openstreetmap.org/{z}/{x}/{y}.png\"\n"
                 + "            })\n"
                 + "        })\n"
                 + "    ],\n"
                 + "    view: new ol.View({\n"
                 + "        center: ol.proj.fromLonLat([mapLng, mapLat]),\n"
                 + "        zoom: mapDefaultZoom\n"
                 + "        })\n"
                 + "    });\n"
                 + "\n"
                 + "    var mapLayer = add_map_point(map, mapLat, mapLng);\n"
                 + "    maps.push({ \"name\": targetDiv, \"map\": map, \"layer\": mapLayer });\n"
                 + "}\n"
                 + "\n"
                 + "function add_map_point(map, mapLat, mapLng) {\n"
                 + "    var vectorLayer = new ol.layer.Vector({\n"
                 + "    source:new ol.source.Vector({\n"
                 + "        features: [new ol.Feature({\n"
                 + "            geometry: new ol.geom.Point(ol.proj.transform([parseFloat(mapLng), parseFloat(mapLat)], 'EPSG:4326', 'EPSG:3857')),\n"
                 + "        })]\n"
                 + "    }),\n"
                 + "    style: new ol.style.Style({\n"
                 + "        image: new ol.style.Icon({\n"
                 + "        anchor: [0.5, 0.5],\n"
                 + "        anchorXUnits: \"fraction\",\n"
                 + "        anchorYUnits: \"fraction\",\n"
                 + "        src: \"https://upload.wikimedia.org/wikipedia/commons/e/ec/RedDot.svg\"\n"
                 + "        })\n"
                 + "    })\n"
                 + "    });\n"
                 + "\n"
                 + "    map.addLayer(vectorLayer); \n"
                 + "    return vectorLayer; \n"
                 + "}\n"
                 + "\n"
                 + "function recreateFileInput() {\n"
                 + "    var fileInput = document.getElementById(\"file\");\n"
                 + "    if (fileInput != null)\n"
                 + "        fileInput.parentNode.removeChild(fileInput);\n"
                 + "    fileInput = document.createElement(\"input\");\n"
                 + "    fileInput.type = \"file\";\n"
                 + "    fileInput.id = \"file\";\n"
                 + "    fileInput.style.display = \"none\";\n"
                 + "    document.body.appendChild(fileInput);\n"
                 + "    return fileInput;\n"
                 + "}\n"
                 + "\n"
                 + "function setImageFromFileInput() {\n"
                 + "    var input = document.getElementById(\"file\");\n"
                 + "    if (input.files.length > 0) {\n"
                 + "        var reader = new FileReader();\n"
                 + "        reader.addEventListener(\"load\", function() {\n"
                 + "            getImageFromUrl(reader.result);\n"
                 + "        });\n"
                 + "        reader.readAsDataURL(input.files[0]);\n"
                 + "    }\n"
                 + "}\n"
                 + "\n"
                 + "function getDictKeys(dict) {\n"
                 + "    var result = [];\n"
                 + "    for (var i = 0; i < dict.length; i++) {\n"
                 + "        result.push(dict[i].Key);\n"
                 + "    }\n"
                 + "    return result;\n"
                 + "}\n"
                 + "\n"
                  + "function dateTimeReviver(key, value) {\n"
                 + "   var a;\n"
                 + "   if (typeof value === 'string' && value != null && value.length == 24) {\n"
                 + "       var helper = value.split('T');\n"
                 + "       if (helper.length == 2) {\n"
                 + "           var dateHelper = helper[0].split('-');\n"
                 + "           var timeHelper = helper[1].split(':');\n"
                 + "           if (dateHelper.length == 3 && timeHelper.length == 3) {\n"
                 + "               return new Date(value);\n"
                 + "           }\n"
                 + "       }\n"
                 + "   }\n"
                 + "   return value;\n"
                 + "}\n"
                 + "function parseValue(value) {\n"
                 + "    if (value != null) {\n"
                 + "        if (value.toString() == 'true')\n"
                 + "            return \"&check;\";\n"
                 + "        else if (value.toString() == 'false')\n"
                 + "            return \"\";\n"
                 + "        else if (Object.prototype.toString.call(value) === '[object Date]')"
                 + "            return value.toLocaleString();\n"
                 + "        else\n"
                 + "            return value;\n"
                 + "    }\n"
                 + "    else {\n"
                 + "        return \"\";\n"
                 + "    }\n"
                 + "}\n"
                 + "\n"
                 + "function onScanSuccess(decodedText, decodedResult) {"
                 + "    globScanFinished = true;\n"
                 + "    globScannedCode = decodedText;\n"
                 + "}\n"
                 + "\n"
                 + "function cancelCodeScan(decodedText, decodedResult) {"
                 + "    globScanFinished = true;\n"
                 + "    globScannedCode = '';\n"
                 + "}\n"
                 + "\n"
                 + "function getImageScaleStyle(image) {"
                 + "    if (image.zoom == 100)\n"
                 + "        return \"\"\n"
                 + "    else if (image.zoom >= 0)\n"
                 + "        return \"width:\" + image.zoom + \"%\";\n"
                 + "    else\n"
                 + "        return \"width:\" + image.customWidth + \"px;height:\" + image.customHeight + \"px;\";\n"
                 + "}\n"
                 + "\n"
                 + "function selectMenuItem(index) {"
                 + "    appMenuIndex = index;\n"
                 + "    switchAppMenu();\n"
                 + "}\n"
                 + "\n"
                 + "function switchAppMenu() {"
                 + "    appMenuOpen = !appMenuOpen;\n"
                 + "    if (appMenuOpen) {\n"
                 + "        document.getElementById(\"top-bar-menu\").innerHTML = \"\";\n"
                 + "        for (var i = 0; i < appMenuItems.length; i++) {\n"
                 + "            document.getElementById(\"top-bar-menu\").innerHTML += \"<div class='menu-item' onclick='selectMenuItem(\" + i + \")'>\" + appMenuItems[i] + \"</div>\";\n"
                 + "        }\n"
                 + "        document.getElementById(\"top-bar-menu\").style.left = \"0\";\n"
                 + "    }\n"
                 + "    else {\n"
                 + "        document.getElementById(\"top-bar-menu\").style.left = \"-620px\";\n"
                 + "    }\n"
                 + "}\n"
                 + "\n"
                 + "function switchDisplay() {"
                 + "    darkMode = !darkMode;\n"
                 + "    localStorage.setItem(\"" + currentProject.Id + "-darkmode\", darkMode);\n"
                 + "    setDisplay();\n"
                 + "}\n"
                 + "\n"
                 + "function setDisplay() {\n"
                 + "    if (darkMode) {\n"
                 + "        document.body.className = \"dark-general\";\n"
                 + "        document.getElementById(\"modal-input\").className = \"modal dark-general\";\n"
                 + "        document.getElementById(\"top-bar\").className = \"dark-top-bar\";\n"
                 + "        document.getElementById(\"top-bar-menu\").className = \"dark-menu\";\n"
                 + "        document.getElementById(\"top-bar-menu-btn\").style.filter = \"brightness(2)\";\n"
                 + "        document.getElementById(\"top-bar-display-btn\").style.filter = \"brightness(2)\";\n"
                 + "        document.getElementById(\"top-bar-display-btn\").className = \"btn-light-mode\";\n"
                 + "        var inputElements = document.getElementsByTagName(\"input\");\n"
                 + "        if (inputElements != null && inputElements.length > 0) {\n"
                 + "            for (var i = 0; i < inputElements.length; i++) {\n"
                 + "                inputElements[i].className = \"dark-input\";\n"
                 + "            }\n"
                 + "        }\n"
                 + "        var buttonElements = document.getElementsByTagName(\"button\");\n"
                 + "        if (buttonElements != null && buttonElements.length > 0) {\n"
                 + "            for (var i = 0; i < buttonElements.length; i++) {\n"
                 + "                buttonElements[i].className = \"dark-button\";\n"
                 + "            }\n"
                 + "        }\n"
                 + "        var tableHeaders = document.getElementsByTagName(\"thead\");\n"
                 + "        if (tableHeaders != null && tableHeaders.length > 0) {\n"
                 + "            for (var i = 0; i < tableHeaders.length; i++) {\n"
                 + "                tableHeaders[i].style.backgroundColor = \"dimgray\";\n"
                 + "            }\n"
                 + "        }\n"
                 + "    }\n"
                 + "    else {\n"
                 + "        document.body.className = \"\";\n"
                 + "        document.getElementById(\"modal-input\").className = \"modal\";\n"
                 + "        document.getElementById(\"top-bar\").className = \"\";\n"
                 + "        document.getElementById(\"top-bar-menu\").className = \"\";\n"
                 + "        document.getElementById(\"top-bar-menu-btn\").style.filter = \"\";\n"
                 + "        document.getElementById(\"top-bar-display-btn\").style.filter = \"\";\n"
                 + "        document.getElementById(\"top-bar-display-btn\").className = \"btn-dark-mode\";\n"
                 + "        var inputElements = document.getElementsByTagName(\"input\");\n"
                 + "        if (inputElements != null && inputElements.length > 0) {\n"
                 + "            for (var i = 0; i < inputElements.length; i++) {\n"
                 + "                inputElements[i].className = \"\";\n"
                 + "            }\n"
                 + "        }\n"
                 + "        var buttonElements = document.getElementsByTagName(\"button\");\n"
                 + "        if (buttonElements != null && buttonElements.length > 0) {\n"
                 + "            for (var i = 0; i < buttonElements.length; i++) {\n"
                 + "                buttonElements[i].className = \"\";\n"
                 + "            }\n"
                 + "        }\n"
                 + "        var tableHeaders = document.getElementsByTagName(\"thead\");\n"
                 + "        if (tableHeaders != null && tableHeaders.length > 0) {\n"
                 + "            for (var i = 0; i < tableHeaders.length; i++) {\n"
                 + "                tableHeaders[i].style.backgroundColor = \"\";\n"
                 + "            }\n"
                 + "        }\n"
                 + "    }\n"
                 + "}\n"
                 + "\n"
                 + "function getLanguage() {"
                 + "    var lang = navigator.language || navigator.userLanguage;\n"
                 + "    if (lang.indexOf(\"-\") > -1) {\n"
                 + "        lang = lang.substring(0, 2);\n"
                 + "    }\n"
                 + "    return lang.toLowerCase();\n"
                 + "}\n"
                 + "\n"
                 + "\n";

    for (var func = 0; func < functions.length; func++)
    {
        if (functions[func].Type == "function") {
            var funcCommands = functions[func].Commands;
            jsString += "async function " + functions[func].Name.replace(/ /g, "_") + "(";
            for (var fpar = 0; fpar < functions[func].Parameters.length; fpar++) {
                jsString += functions[func].Parameters[fpar].Name.replace(/ /g, "_");
                if (fpar < functions[func].Parameters.length - 1)
                    jsString += ", ";
            }
            jsString += ") {\n";
            jsString += "try {\n";
            if (functions[func].Name == "main") {
                jsString += "if (localStorage.getItem(\"" + currentProject.Id + "-darkmode\") != null) {\n";
                jsString += "    var darkModeResult = localStorage.getItem(\"" + currentProject.Id + "-darkmode\");\n";
                jsString += "    darkMode = darkModeResult.toLowerCase() == 'true';\n";
                jsString += "    setDisplay();\n";
                jsString += "}\n";
            }
            for (var cmd = 0; cmd < funcCommands.length; cmd++) {
                if (!funcCommands[cmd].Disabled) {
                    currentClass = "";
                    currentClassVariableName = "";
                    if (funcCommands[cmd].Type == "command") {
                        if (funcCommands[cmd].ClassName != null && funcCommands[cmd].ClassName.length > 0) {
                            currentClass = funcCommands[cmd].ClassName;
                            currentClassVariableName = funcCommands[cmd].ClassVariable;
                        }
                        jsString += parseExpressionsForJs(functions[func], funcCommands[cmd].Expressions, true)
                        if (!jsString.endsWith(";"))
                            jsString += ";";
                        jsString += "\n";
                    }
                    if (funcCommands[cmd].Type == "if") {
                        jsString += "if (" + parseExpressionsForJs(functions[func], funcCommands[cmd].Expressions, true) + ") {\n";
                    }
                    else if (funcCommands[cmd].Type == "else") {
                        jsString += "else {\n";
                    }
                    else if (funcCommands[cmd].Type == "else if") {
                        jsString += "else if (" + parseExpressionsForJs(functions[func], funcCommands[cmd].Expressions, true) + ") {\n";
                    }
                    else if (funcCommands[cmd].Type == "while") {
                        jsString += "while (" + parseExpressionsForJs(functions[func], funcCommands[cmd].Expressions, true) + ") {\n";
                    }
                    else if (funcCommands[cmd].Type == "loop") {
                        jsString += "while (true) {\n";
                        jsString += "await sleep(" + parseExpressionsForJs(functions[func], funcCommands[cmd].Expressions, false) + ");"
                    }
                    else if (funcCommands[cmd].Type == "for") {
                        var variableName = funcCommands[cmd].Expressions[0].Name.replace(/ /g, "_");
                        var expressions1 = parseExpressionsForJs(functions[func], funcCommands[cmd].Expressions[0].Expressions, false);
                        var expressions2 = parseExpressionsForJs(functions[func], funcCommands[cmd].Expressions[1].Expressions, false);
                        jsString += "for (var " + variableName + " = " + expressions1 + "; " + variableName + " <= " + expressions2 + "; " + variableName + "++) {\n";
                    }
                    else if (funcCommands[cmd].Type == "foreach") {
                        var variableName = funcCommands[cmd].Expressions[0].Name.replace(/ /g, "_");
                        var collectionName = funcCommands[cmd].Expressions[1].Name.replace(/ /g, "_");
                        if (funcCommands[cmd].Expressions[1].ClassVariable != null && funcCommands[cmd].Expressions[1].ClassVariable.length > 0) {
                            var classVariable = funcCommands[cmd].Expressions[1].ClassVariable.replace(/ /g, "_");
                            collectionName = classVariable + "." + funcCommands[cmd].Expressions[1].Name.replace(/ /g, "_");
                        }
                        jsString += "for (var " + variableName + "Counter = 0; " + variableName + "Counter < " + collectionName + ".length; " + variableName + "Counter++) {\n";
                        jsString += "var " + variableName + " = " + collectionName + "[" + variableName + "Counter];\n";
                    }
                    else if (funcCommands[cmd].Type == "end") {
                        jsString += "}\n";
                    }
                }
            }
            jsString += "}\n";
            jsString += "catch(ex) {\n";
            jsString += "document.getElementById(\"content\").innerHTML += \"<div style='color:red;font-weight:800;'>\" + ex + \"</div>\";";
            jsString += "console.log(ex);\n";
            jsString += "}\n";
            jsString += "}\n\n";
        }
    }
    return jsString;
}

function parseExpressionsForJs(func, expressions, replaceEquals) {
    var output = "";
    if (expressions != null) {
        for (var i = 0; i < expressions.length; i++) {
            if (expressions[i].Type == "namespace") {
                currentNamespace = expressions[i].Name;
            }
            else if (expressions[i].Type == "method" || expressions[i].Type == "function") {
                var command = commandsReference.find((item) => (item.BelongsToNamespace == currentNamespace || item.BelongsToNamespace == expressions[i].Namespace) && item.Name == expressions[i].Name);
                var classObject = functions.find((item) => item.Name == currentNamespace);
                var classListObject = functions.find((item) => item.Name == currentNamespace.replace(" list", ""));
                if (command != null) {
                    if (command.BelongsToNamespace == "page") {
                        if (command.Name == "print") {
                            output += "document.getElementById(\"content\").innerHTML += \"<div>\" + (" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ") + \"</div>\";\n";
                            output += "setDisplay();"
                        }    
                        else if (command.Name == "get string") {
                            output += "null;\n"
                            output += "dialogClosed = false;\n"
                            output += "document.getElementById(\"modal-input-prompt\").innerText = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";\n"
                            output += "document.getElementById(\"modal-input\").style.display = \"\";\n"
                            output += "document.getElementById(\"modal-input-value\").type = \"text\";\n"
                            output += "document.getElementById(\"modal-input-value\").value = \"\";\n"
                            output += "document.getElementById(\"modal-input-value\").focus();\n"
                            output += "document.getElementById(\"modal-input-value\").setSelectionRange(0, document.getElementById(\"modal-input-value\").value.length);\n"
                            output += "while (!dialogClosed) { await sleep(200); }\n";
                            output += currentVariableName + " = document.getElementById(\"modal-input-value\").value;";
                        }    
                        else if (command.Name == "get number") {
                            output += "null;\n"
                            output += "dialogClosed = false;\n"
                            output += "document.getElementById(\"modal-input-prompt\").innerText = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";\n"
                            output += "document.getElementById(\"modal-input\").style.display = \"\";\n"
                            output += "document.getElementById(\"modal-input-value\").type = \"number\";\n"
                            output += "document.getElementById(\"modal-input-value\").value = \"\";\n"
                            output += "document.getElementById(\"modal-input-value\").focus();\n"
                            output += "while (!dialogClosed) { await sleep(200); }\n";
                            output += currentVariableName + " = (document.getElementById(\"modal-input-value\").value * 1);";
                        }    
                        else if (command.Name == "ask") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += "null;\n"
                            output += "dialogClosed = false;\n"
                            output += "document.getElementById(\"modal-confirm-prompt\").innerText = " + parameterList[0] + ";\n"
                            output += "document.getElementById(\"modal-confirm\").style.display = \"\";\n"
                            for (var btn = 1; btn <= 4; btn++) {
                                output += "document.getElementById(\"modal-confirm-button-" + btn + "\").innerText = " + parameterList[btn] + ";\n"
                                output += "if (document.getElementById(\"modal-confirm-button-" + btn + "\").innerText == \"\")\n"
                                output += "    document.getElementById(\"modal-confirm-button-" + btn + "\").style.display = \"none\";\n";
                                output += "else\n"
                                output += "    document.getElementById(\"modal-confirm-button-" + btn + "\").style.display = \"\";\n";
                            }
                            output += "while (!dialogClosed) { await sleep(200); }\n";
                            output += currentVariableName + " = globDialogValue;";
                        }    
                        else if (command.Name == "clear") {
                            output += "document.getElementById(\"content\").innerHTML = \"\";\n";
                            output += "setDisplay();";
                        }
                        else if (command.Name == "set title") {
                            output += "document.getElementById(\"content\").style.top = \"52px\";\n";
                            output += "document.getElementById(\"top-bar\").style.display = \"flex\";\n";
                            output += "document.getElementById(\"top-bar-title\").innerText = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";";
                        }
                        else if (command.Name == "hide title") {
                            output += "document.getElementById(\"content\").style.top = \"0\";\n";
                            output += "document.getElementById(\"top-bar\").style.display = \"none\";";
                        }
                        else if (command.Name == "get pointer x") {
                            output += "lastTouchX;";
                        }
                        else if (command.Name == "get pointer y") {
                            output += "lastTouchY;";
                        }
                        else if (command.Name == "is pointer down") {
                            output += "pointerDown;";
                        }
                        else if (command.Name == "width") {
                            output += "parent.innerWidth;";
                        }
                        else if (command.Name == "height") {
                            output += "parent.innerHeight;";
                        }
                        else if (command.Name == "add menu item") {
                            output += "appMenuItems.push(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ");\n";
                            output += "document.getElementById(\"top-bar-menu-btn\").style.display = '';";
                        }
                        else if (command.Name == "menu item index") {
                            output += "appMenuIndex;\n";
                            output += "appMenuIndex = -1;";
                        }
                        else if (command.Name == "menu item name") {
                            output += "appMenuIndex > -1 ? appMenuItems[appMenuIndex] : \"\";\n";
                            output += "appMenuIndex = -1;";
                        }
                    }
                    else if (command.BelongsToNamespace == "collection") {
                        if (command.Name == "create") {
                            output += "[];";
                        }
                        else if (command.Name == "clear") {
                            output += currentVariableName + " = [];";
                        }
                        else if (command.Name == "add") {
                            output += currentVariableName + ".push(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ");";
                        }
                        else if (command.Name == "insert") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += currentVariableName + ".splice(" + parameterList[0] + ", 0, " + parameterList[1] + ");";
                        }
                        else if (command.Name == "remove") {
                            output += currentVariableName + ".splice(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ", 1);";
                        }
                        else if (command.Name == "get by index") {
                            output += currentVariableName + "[" + parseExpressionsForJs(func, expressions[i].Expressions, false) + "];";
                        }
                        else if (command.Name == "get length") {
                            output += currentVariableName + ".length;";
                        }
                        else if (command.Name == "get first") {
                            output += "(" + currentVariableName + ".length > 0 ? " + currentVariableName + "[0] : '');";
                        }
                        else if (command.Name == "get last") {
                            output += "(" + currentVariableName + ".length > 0 ? " + currentVariableName + "[" + currentVariableName + ".length - 1] : '');";
                        }
                    }
                    else if (command.BelongsToNamespace == "dictionary") {
                        if (command.Name == "create") {
                            output += "[];";
                        }
                        else if (command.Name == "clear") {
                            output += currentVariableName + " = [];";
                        }
                        else if (command.Name == "add") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += currentVariableName + ".push({ \"Key\" : " + parameterList[0] + ", \"Value\" : " + parameterList[1] + " });";
                        }
                        else if (command.Name == "add object") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += currentVariableName + ".push({ \"Key\" : " + parameterList[0] + ", \"Value\" : JSON.stringify(" + parameterList[1] + ") });";
                        }
                        else if (command.Name == "remove") {
                            output += "var " + currentVariableName + "Index = " + currentVariableName + ".findIndex((" + currentVariableName + "item) => " + currentVariableName + "item.Key == " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ");\n";
                            output += "if (" + currentVariableName + "Index > -1)\n"
                            output += currentVariableName + ".splice(" + currentVariableName + "Index, 1);";
                        }
                        else if (command.Name == "get by key") {
                            output += currentVariableName + ".find((" + currentVariableName + "item) => " + currentVariableName + "item.Key == " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ") != null ? " + currentVariableName + ".find((" + currentVariableName + "item) => " + currentVariableName + "item.Key == " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ").Value : \"\"";
                        }
                        else if (command.Name == "get object by key") {
                            output += currentVariableName + ".find((" + currentVariableName + "item) => " + currentVariableName + "item.Key == " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ") != null ? JSON.parse(" + currentVariableName + ".find((" + currentVariableName + "item) => " + currentVariableName + "item.Key == " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ").Value) : \"\"";
                        }
                        else if (command.Name == "get length") {
                            output += currentVariableName + ".length;";
                        }
                        else if (command.Name == "get keys") {
                            output += "getDictKeys(" + currentVariableName + ");";
                        }
                    }
                    else if (command.BelongsToNamespace == "datetime") {
                        if (command.Name == "create") {
                            output += "new Date()";
                        }
                        else if (command.Name == "set") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            var year = parameterList[0];
                            var month = parameterList[1];
                            var day = parameterList[2];
                            var hours = parameterList[3];
                            var minutes = parameterList[4];
                            var seconds = parameterList[5];
                            var milliseconds = parameterList[6];
                            output += currentVariableName + ".setFullYear(" + year + ");\n";
                            output += currentVariableName + ".setMonth(" + (month - 1) + ");\n";
                            output += currentVariableName + ".setDate(" + day + ");\n";
                            output += currentVariableName + ".setHours(" + hours + ");\n";
                            output += currentVariableName + ".setMinutes(" + minutes + ");\n";
                            output += currentVariableName + ".setSeconds(" + seconds + ");\n";
                            output += currentVariableName + ".setMilliseconds(" + milliseconds + ");";
                        }
                        else if (command.Name == "get year") {
                            output += currentVariableName + ".getFullYear();";
                        }
                        else if (command.Name == "get month") {
                            output += currentVariableName + ".getMonth();";
                        }
                        else if (command.Name == "get day") {
                            output += currentVariableName + ".getDate();";
                        }
                        else if (command.Name == "get hours") {
                            output += currentVariableName + ".getHours();";
                        }
                        else if (command.Name == "get minutes") {
                            output += currentVariableName + ".getMinutes();";
                        }
                        else if (command.Name == "get seconds") {
                            output += currentVariableName + ".getSeconds();";
                        }
                        else if (command.Name == "get milliseconds") {
                            output += currentVariableName + ".getMilliseconds();";
                        }
                        else if (command.Name == "get date string") {
                            output += currentVariableName + ".toLocaleDateString();";
                        }
                        else if (command.Name == "get time string") {
                            output += currentVariableName + ".toLocaleTimeString();";
                        }

                    }
                    else if (command.BelongsToNamespace == "web request") {
                        if (command.Name == "create") {
                            output += "{ \"url\": \"\", \"method\": \"GET\", \"status\": 0, \"contentType\":\"\", \"body\": \"\", \"headers\": [] }";
                        }
                        else if (command.Name == "set url") {
                            output += currentVariableName + ".url = " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "set method") {
                            output += currentVariableName + ".method = " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "set content type") {
                            output += currentVariableName + ".contentType = " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "set body") {
                            output += currentVariableName + ".body = " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "add header") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += currentVariableName + ".headers.push({ \"name\" : " + parameterList[0] + ", \"value\" : " + parameterList[1] + "})";
                        }
                        else if (command.Name == "get response") {
                            var xhttp = "xhttp" + currentVariableName;
                            output += currentVariableName + ";\n";
                            output += currentVariableName + ".status = 0;\n";
                            output += "var " + xhttp + " = new XMLHttpRequest();\n";
                            output += xhttp + ".onreadystatechange = function() {\n";
                            output += "if (this.readyState == 4) {\n";
                            output += currentVariableName + ".status = " + xhttp + ".status;\n";
                            output += currentVariableName + ".statusText = " + xhttp + ".statusText;\n";
                            output += currentVariableName + ".responseText = " + xhttp + ".responseText;\n";
                            output += "}\n";
                            output += "}\n";
                            output += xhttp + ".onerror = function(e) {\n";
                            output += "console.log('ERROR');\n";
                            output += "}\n";
                            output += xhttp + ".onabort = function(e) {\n";
                            output += "console.log('ABORT');\n";
                            output += "}\n";
                            output += xhttp + ".open(" + currentVariableName + ".method, " + currentVariableName + ".url, true);\n";
                            output += "if (" + currentVariableName + ".contentType != \"\")\n";
                            output += xhttp + ".setRequestHeader('content-type', " + currentVariableName + ".contentType);\n";
                            output += "for (var i = 0; i < " + currentVariableName + ".headers; i++) {\n";
                            output += xhttp + ".setRequestHeader(" + currentVariableName + ".headers[i].name, " + currentVariableName + ".headers[i].value);\n";
                            output += "}\n";
                            output += xhttp + ".send(" + currentVariableName + ".body);\n";
                            output += "while (" + currentVariableName + ".status == 0) { await sleep(200); }";
                        }
                    }
                    else if (command.BelongsToNamespace == "web response") {
                        if (command.Name == "status") {
                            output += currentVariableName + ".status;";
                        }
                        else if (command.Name == "status text") {
                            output += currentVariableName + ".statusText;";
                        }
                        else if (command.Name == "response text") {
                            output += currentVariableName + ".responseText;";
                        }
                    }
                    else if (command.BelongsToNamespace == "device") {
                        if (command.Name == "get orientation") {
                            output += "(windowWidth > windowHeight ? \"landscape\" : \"portrait\")";
                        }
                        else if (command.Name == "send email") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += "var emailAddress = " + parameterList[0] + ";\n";
                            output += "var subject = encodeURI(" + parameterList[1] + ");\n";
                            output += "var body = encodeURI(" + parameterList[2] + ");\n";
                            output += "var emailLink = \"mailto:\" + emailAddress + \"?subject=\" + subject + \"&body=\" + body;\n";
                            output += "actionLink(emailLink);\n";
                        }
                        else if (command.Name == "call") {
                            output += "var telNumber = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";\n";
                            output += "var telLink = \"tel:\" + telNumber;\n";
                            output += "actionLink(telLink);\n";
                        }
                        else if (command.Name == "scan code") {
                            output += currentVariableName + ";\n";
                            output += "globScanFinished = false;\n"
                            output += "globScanedCode = \"\";\n"
                            output += "document.getElementById(\"codescanner\").style.display = \"\";\n"
                            output += "var html5QrCode = new Html5Qrcode(\"codereader\");\n"
                            output += "var html5CodeConfig = { fps: 30 };\n"
                            output += "html5QrCode.start({ facingMode: \"environment\" }, html5CodeConfig, onScanSuccess);\n";
                            output += "while (!globScanFinished) { await sleep(200); };\n";
                            output += "await html5QrCode.stop();"
                            output += "document.getElementById(\"codescanner\").style.display = \"none\";\n"
                            output += currentVariableName + " = globScannedCode";
                        }
                        else if (command.Name == "open url") {
                            output += "var url = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";\n";
                            output += "actionLink(url);\n";
                        }
                        else if (command.Name == "get language") {
                            output += "getLanguage()";
                        }
                        else if (command.Name == "get geolocation") {
                            output += "{};\n";
                            output += "getGeoLocation();\n";
                            output += "while (checkGetLocationInProgress) { await sleep(200); }\n";
                            output += currentVariableName + " = globPosition;";
                        }
                    }
                    else if (command.BelongsToNamespace == "geolocation") {
                        if (command.Name == "activate tracking") {
                            output += "{};\n";
                            output += "watchGeoLocation();\n";
                            output += "while (checkGetLocationInProgress) { await sleep(200); }";
                        }
                        else if (command.Name == "update geolocation") {
                            output += currentVariableName + " = globPosition;";
                        }
                        else if (command.Name == "latitude") {
                            output += currentVariableName + ".coords.latitude;";
                        }
                        else if (command.Name == "longitude") {
                            output += currentVariableName + ".coords.longitude;";
                        }
                        else if (command.Name == "accuracy") {
                            output += currentVariableName + ".coords.accuracy;";
                        }
                        else if (command.Name == "altitude") {
                            output += currentVariableName + ".coords.altitude;";
                        }
                        else if (command.Name == "altitude accuracy") {
                            output += currentVariableName + ".coords.altitudeAccuracy;";
                        }
                        else if (command.Name == "heading") {
                            output += currentVariableName + ".coords.heading;";
                        }
                        else if (command.Name == "speed") {
                            output += currentVariableName + ".coords.speed;";
                        }
                        else if (command.Name == "show map") {
                            output += "document.getElementById(\"content\").innerHTML += \"<div id='map" + currentVariableName + "'></div>\";\n";
                            output += "setTimeout(function() { initialize_map(" + currentVariableName + ".coords.latitude, " + currentVariableName + ".coords.longitude, 16, '" + currentVariableName + "'); }, 100);";
                        }
                        else if (command.Name == "update map") {
                            output += "var mapItem = maps.find((item) => item.name == \"" + currentVariableName + "\");\n";
                            output += "if (mapItem != null) {\n";
                            output += "var view = new ol.View({\n";
                            output += "    center: ol.proj.fromLonLat([" + currentVariableName + ".coords.longitude, " + currentVariableName + ".coords.latitude]),\n";
                            output += "    zoom: 16\n";
                            output += "});\n";
                            output += "mapItem.map.setView(view);\n";
                            output += "mapItem.map.removeLayer(mapItem.layer);\n";
                            output += "mapItem.layer = add_map_point(mapItem.map, " + currentVariableName + ".coords.latitude, " + currentVariableName + ".coords.longitude);\n";
                            output += "}";
                        }
                    }
                    else if (command.BelongsToNamespace == "math") {
                        output += "Math." + command.Name + "(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ")";
                    }
                    else if (command.BelongsToNamespace == "image") {
                        if (command.Name == "create") {
                            output += "{ \"width\": 0, \"height\": 0, \"zoom\": 100, \"customWidth\": 0, \"customHeight\": 0, \"data\": \"\" }";
                        }
                        else if (command.Name == "from url") {
                            output += "globImage = { \"width\": 0, \"height\": 0, \"zoom\": 100, \"customWidth\": 0, \"customHeight\": 0, \"data\": \"\" };\n";
                            output += "getImageFromUrl(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ");\n";
                            output += "while (globImage.data == \"\") { await sleep(200); };\n";
                            output += currentVariableName + " = globImage";
                        }
                        else if (command.Name == "from camera") {
                            output += "globImage = { \"width\": 0, \"height\": 0, \"zoom\": 100, \"customWidth\": 0, \"customHeight\": 0, \"data\": \"\" };\n";
                            output += "initCamera();"
                            output += "while (globImage.data == \"\") { await sleep(200); };\n";
                            output += currentVariableName + " = globImage";
                        }
                        else if (command.Name == "from file") {
                            output += "globImage = { \"width\": 0, \"height\": 0, \"zoom\": 100, \"customWidth\": 0, \"customHeight\": 0, \"data\": \"\" };\n";
                            output += "var " + currentVariableName + "fileInput = recreateFileInput();\n";
                            output += currentVariableName + "fileInput.addEventListener(\"change\", function() { setImageFromFileInput(); });\n";
                            output += currentVariableName + "fileInput.addEventListener(\"cancel\", function() { globImage.data = \"CANCEL\"; });\n";
                            output += currentVariableName + "fileInput.click();\n";
                            output += "document.getElementById(\"content\").innerHTML += \"<button id=\\\"cancel-button\\\" onclick=\\\"globImage.data = 'CANCEL';\\\">Cancel</button>\";";
                            output += "while (globImage.data == \"\") { await sleep(200); };\n";
                            output += "if (globImage.data == \"CANCEL\") { globImage.data = \"\"; };\n";
                            output += "var cancelButton = document.getElementById(\"cancel-button\");\n";
                            output += "if (cancelButton != null) { document.getElementById(\"content\").removeChild(cancelButton); };\n";
                            output += currentVariableName + " = globImage";
                        }
                        else if (command.Name == "qrcode") {
                            output += "qrcode.clear();\n";
                            output += "qrcode.makeCode(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ");\n";
                            output += "await sleep(100);"
                            output += currentVariableName + ".data = document.getElementById(\"qrcode\").getElementsByTagName(\"img\")[0].src;";
                        }
                        else if (command.Name == "show") {
                            output += "document.getElementById(\"content\").innerHTML += \"<div><img id=\\\"" + currentVariableName + "\\\" draggable=\\\"false\\\" src=\" + " + currentVariableName + ".data + \" style=\" + getImageScaleStyle(" + currentVariableName + ") + \" data-clicked='false' onmousedown='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clicked = true;' ontouchstart='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clicked = true;' onmouseup='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clicked = false;' ontouchend='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clicked = false;'></div>\";\n";
                            output += "setDisplay();";
                        }
                        else if (command.Name == "show at") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += "var " + currentVariableName + "posx = " + parameterList[0].replaceAll2(" ", "") + ";\n";
                            output += "var " + currentVariableName + "posy = " + parameterList[1].replaceAll2(" ", "") + ";\n";
                            output += "document.getElementById(\"content\").innerHTML += \"<div style='position:absolute;left:\" + " + currentVariableName + "posx + \"px;top:\" + " + currentVariableName + "posy + \"px;'><img id=\\\"" + currentVariableName + "\\\" draggable=\\\"false\\\" src=\" + " + currentVariableName + ".data + \" style=\" + getImageScaleStyle(" + currentVariableName + ") + \" data-clicked='false' onmousedown='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clicked = true;' ontouchstart='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clicked = true;' onmouseup='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clicked = false;' ontouchend='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clicked = false;'></div>\";\n";
                            output += "setDisplay();";
                        }
                        else if (command.Name == "move to") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += "var " + currentVariableName + "posx = " + parameterList[0].replaceAll2(" ", "") + ";\n";
                            output += "var " + currentVariableName + "posy = " + parameterList[1].replaceAll2(" ", "") + ";\n";
                            output += "var " + currentVariableName + "Image = document.getElementById(\"" + currentVariableName + "\").parentElement;\n";
                            output += "if (" + currentVariableName + "Image != null) {\n";
                            output += currentVariableName + "Image.style.left = " + currentVariableName + "posx + 'px';\n";
                            output += currentVariableName + "Image.style.top = " + currentVariableName + "posy + 'px';\n";
                            output += "}";
                        }
                        else if (command.Name == "download") {
                            output += "download(" + currentVariableName + ".data, " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ");";
                        }
                        else if (command.Name == "left") {
                            output += "document.getElementById(\"" + currentVariableName + "\").parentElement.getBoundingClientRect().left;";
                        }
                        else if (command.Name == "top") {
                            output += "document.getElementById(\"" + currentVariableName + "\").parentElement.getBoundingClientRect().top;";
                        }
                        else if (command.Name == "width") {
                            output += currentVariableName + ".width;";
                        }
                        else if (command.Name == "height") {
                            output += currentVariableName + ".height;";
                        }
                        else if (command.Name == "zoom") {
                            output += currentVariableName + ".zoom = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";\n";
                            output += currentVariableName + ".customWidth = 0;\n";
                            output += currentVariableName + ".customHeight = 0;\n";
                            output += "var " + currentVariableName + "Elem = document.getElementById(\"" + currentVariableName + "\");\n"
                            output += "if (" + currentVariableName + "Elem != null) " + currentVariableName + "Elem.style.width = '" + parseExpressionsForJs(func, expressions[i].Expressions, false).replaceAll2(" ", "") + "%'"
                        }
                        else if (command.Name == "resize") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += currentVariableName + ".zoom = -1;\n";
                            output += currentVariableName + ".customWidth = " + parameterList[0].replaceAll2(" ", "") + ";\n";
                            output += currentVariableName + ".customHeight = " + parameterList[1].replaceAll2(" ", "") + ";\n";
                            output += "var " + currentVariableName + "Elem = document.getElementById(\"" + currentVariableName + "\");\n"
                            output += "if (" + currentVariableName + "Elem != null) {\n"
                            output += currentVariableName + "Elem.style.width = '" + parameterList[0].replaceAll2(" ", "").replaceAll2(" ", "") + "px'\n"
                            output += currentVariableName + "Elem.style.height = '" + parameterList[1].replaceAll2(" ", "").replaceAll2(" ", "") + "px'\n"
                            output += "}"
                        }
                        else if (command.Name == "is pointer down") {
                            output += "document.getElementById(\"" + currentVariableName.replaceAll2(" ", "") + "\") != null ? document.getElementById(\"" + currentVariableName.replaceAll2(" ", "") + "\").dataset.clicked == 'true' : false;\n";
                        }
                    }
                    else if (command.BelongsToNamespace == "storage") {
                        if (command.Name == "put") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            var storageName = "\"" + currentProject.Id + "-\" + " + parameterList[0].replaceAll2(" ", "");
                            output += "localStorage.setItem(" + storageName + ", JSON.stringify(" + parameterList[1] + "));";
                        }
                        else if (command.Name == "get") {
                            output += currentVariableName + ";\n";
                            var storageName = "\"" + currentProject.Id + "-\" + " + parseExpressionsForJs(func, expressions[i].Expressions, false).replaceAll2(" ", "");
                            output += "var jsonString = localStorage.getItem(" + storageName + ");\n";
                            output += "if (jsonString != null) " + currentVariableName + " = JSON.parse(jsonString,dateTimeReviver);"
                        }
                        else if (command.Name == "remove") {
                            var storageName = "\"" + currentProject.Id + "-\" + " + parseExpressionsForJs(func, expressions[i].Expressions, false).replaceAll2(" ", "");
                            output += "localStorage.removeItem(" + storageName + ");";
                        }
                        else if (command.Name == "upload") {
                            if (syncenabled && currentProject.Sync) {
                                var username = localStorage.getItem("username");
                                var appid = localStorage.getItem("appid");
                        
                                output += "var storageList = [];\n";
                                output += "for (var i = 0; i < localStorage.length; i++) {\n";
                                output += "if (localStorage.key(i).startsWith(\"" + currentProject.Id + "-\")) {\n"
                                output += "storageList.push({\"Key\": localStorage.key(i), \"Value\": btoa(localStorage.getItem(localStorage.key(i))) });\n";
                                output += "}\n";
                                output += "}\n";
                                output += "var storageObject = { \"Id\": \"" + currentProject.Id + "\", \"Name\": \"" + currentProject.Name + "\", \"Storages\": storageList };";

                                output += "var syncUpRequest = new XMLHttpRequest();\n";
                                output += "var syncUpStatus = { \"status\": 0, \"statusText\": \"\", \"responstText\": \"\"};\n";
                                output += "syncUpRequest.onreadystatechange = function() {\n";
                                output += "if (this.readyState == 4) {\n";
                                output += "syncUpStatus.status = syncUpRequest.status;\n";
                                output += "syncUpStatus.statusText = syncUpRequest.statusText;\n";
                                output += "syncUpStatus.responseText = syncUpRequest.responseText;\n";
                                output += "}\n";
                                output += "}\n";
                                output += "syncUpRequest.onerror = function(e) {\n";
                                output += "console.log('SYNC ERROR');\n";
                                output += "}\n";
                                output += "syncUpRequest.onabort = function(e) {\n";
                                output += "console.log('SYNC ABORTED');\n";
                                output += "}\n";
                                output += "var syncUpUrl = 'https://touchide.net?u=" + encodeURIComponent(username) + "&a=" + encodeURIComponent(appid) + "&storage=1';\n";
                                output += "if (storageMode == 'S') syncUpUrl += '&sessionid=" + encodeURIComponent(sessionId) + "';\n";
                                output += "syncUpRequest.open('POST', syncUpUrl, true);\n";
                                output += "syncUpRequest.setRequestHeader('content-type', 'application/json');\n";
                                output += "syncUpRequest.send(JSON.stringify(storageObject));\n";
                                output += "while (syncUpStatus.status == 0) { await sleep(200); }\n";
                            }
                            else {
                                output += "document.getElementById(\"content\").innerHTML += \"<div style='color:red;font-weight:800;'>To use storage.upload, valid cloud credentials must be present, and sync has to be enabled for this script.</div>\";\n"
                                output += "setDisplay();";
                            }
                        }
                        else if (command.Name == "download") {
                            if (syncenabled && currentProject.Sync) {
                                var username = localStorage.getItem("username");
                                var appid = localStorage.getItem("appid");

                                output += "var syncDownRequest = new XMLHttpRequest();\n";
                                output += "var syncDownStatus = { \"status\": 0, \"statusText\": \"\", \"responstText\": \"\"};\n";
                                output += "syncDownRequest.onreadystatechange = function() {\n";
                                output += "if (this.readyState == 4) {\n";
                                output += "syncDownStatus.status = syncDownRequest.status;\n";
                                output += "syncDownStatus.statusText = syncDownRequest.statusText;\n";
                                output += "syncDownStatus.responseText = syncDownRequest.responseText;\n";
                                output += "}\n";
                                output += "}\n";
                                output += "syncDownRequest.onerror = function(e) {\n";
                                output += "console.log('SYNC ERROR');\n";
                                output += "}\n";
                                output += "syncDownRequest.onabort = function(e) {\n";
                                output += "console.log('SYNC ABORTED');\n";
                                output += "}\n";
                                output += "var syncDownUrl = 'https://touchide.net?u=" + encodeURIComponent(username) + "&a=" + encodeURIComponent(appid) + "&s=" + encodeURIComponent(currentProject.Id) + "&storage=1';\n";
                                output += "if (storageMode == 'S') syncDownUrl += '&sessionid=" + encodeURIComponent(sessionId) + "';\n";
                                output += "syncDownRequest.open('POST', syncDownUrl, true);\n";
                                output += "syncDownRequest.send();\n";
                                output += "while (syncDownStatus.status == 0) { await sleep(200); }\n";

                                output += "var syncDownData = JSON.parse(syncDownStatus.responseText);\n";
                                output += "for (var i = 0; i < syncDownData.Storages.length; i++) {\n";
                                output += "localStorage.setItem(syncDownData.Storages[i].Key, atob(syncDownData.Storages[i].Value));\n";
                                output += "}\n";
                            }
                            else {
                                output += "document.getElementById(\"content\").innerHTML += \"<div style='color:red;font-weight:800;'>To use storage.download, valid cloud credentials must be present, and sync has to be enabled for this script.</div>\";\n"
                                output += "setDisplay();";
                            }
                        }
                        else if (command.Name == "set global mode") {
                            output += "storageMode = 'G';\n";
                        }
                        else if (command.Name == "set session mode") {
                            output += "storageMode = 'S';\n";
                        }
                    }
                    else if (command.BelongsToNamespace == "json") {
                        if (command.Name == "parse") {
                            var valueParam = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            output += "JSON.parse(" + valueParam + ");";
                        }
                        else if (command.Name == "to string") {
                            var valueParam = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            output += "JSON.stringify(" + valueParam + ");";
                        }
                    }
                    else if (command.BelongsToNamespace == "button") {
                        if (command.Name == "create") {
                            output += "{ \"Text\": \"Click me\" }";
                        }
                        else if (command.Name == "set text") {
                            output += "if (document.getElementById(\"btn-" + currentVariableName.replaceAll2(" ", "") + "\") != null) { document.getElementById(\"btn-" + currentVariableName.replaceAll2(" ", "") + "\").innerText = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + "; } else { " + currentVariableName + ".Text = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + " }";
                        }
                        else if (command.Name == "show") {
                            output += "document.getElementById(\"content\").innerHTML += \"<button id='btn-" + currentVariableName.replaceAll2(" ", "") + "' style='margin:8px;' data-clicked='false' onclick='document.getElementById(\\\"btn-" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clicked = true;'>\" + (" + currentVariableName + ".Text) + \"</button>\";\n";
                            output += "setDisplay();";
                        }
                        else if (command.Name == "is clicked") {
                            output += "document.getElementById(\"btn-" + currentVariableName.replaceAll2(" ", "") + "\") != null ? document.getElementById(\"btn-" + currentVariableName.replaceAll2(" ", "") + "\").dataset.clicked == 'true' : false;\n";
                            output += "if (document.getElementById(\"btn-" + currentVariableName.replaceAll2(" ", "") + "\") != null) { document.getElementById(\"btn-" + currentVariableName.replaceAll2(" ", "") + "\").dataset.clicked = false; }\n";
                        }
                    }
                    else if (command.BelongsToNamespace == "text input") {
                        if (command.Name == "create") {
                            output += "{ \"Placeholder\": \"\", \"Value\": \"\" };";
                        }
                        else if (command.Name == "set placeholder") {
                            output += "if (document.getElementById(\"txt-" + currentVariableName.replaceAll2(" ", "") + "\") != null) { document.getElementById(\"txt-" + currentVariableName.replaceAll2(" ", "") + "\").placeholder = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + "; } else { " + currentVariableName + ".Placeholder = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + " }";
                        }
                        else if (command.Name == "set value") {
                            output += currentVariableName + ".Value = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";"
                        }
                        else if (command.Name == "get value") {
                            output += "document.getElementById(\"txt-" + currentVariableName + "\").value;";
                        }
                        else if (command.Name == "show") {
                            output += "document.getElementById(\"content\").innerHTML += \"<input id='txt-" + currentVariableName.replaceAll2(" ", "") + "' type='text' style='margin:8px;' placeholder='\" + " + currentVariableName + ".Placeholder + \"'>\";\n";
                            output += "setDisplay();\n";
                            output += "setTimeout(function() { document.getElementById(\"txt-" + currentVariableName + "\").value = " + currentVariableName + ".Value }, 100);";
                        }
                    }
                    else if (command.BelongsToNamespace == "number input") {
                        if (command.Name == "create") {
                            output += "{ \"Placeholder\": \"\", \"Value\": \"\" };";
                        }
                        else if (command.Name == "set placeholder") {
                            output += "if (document.getElementById(\"txt-" + currentVariableName.replaceAll2(" ", "") + "\") != null) { document.getElementById(\"txt-" + currentVariableName.replaceAll2(" ", "") + "\").placeholder = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + "; } else { " + currentVariableName + ".Placeholder = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + " }";
                        }
                        else if (command.Name == "set value") {
                            output += currentVariableName + ".Value = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";"
                        }
                        else if (command.Name == "get value") {
                            output += "(document.getElementById(\"txt-" + currentVariableName + "\").value * 1);";
                        }
                        else if (command.Name == "show") {
                            output += "document.getElementById(\"content\").innerHTML += \"<input id='txt-" + currentVariableName.replaceAll2(" ", "") + "' type='number' style='margin:8px;' placeholder='\" + " + currentVariableName + ".Placeholder + \"'>\";\n";
                            output += "setDisplay();\n";
                            output += "setTimeout(function() { document.getElementById(\"txt-" + currentVariableName + "\").value = " + currentVariableName + ".Value }, 100);";
                        }
                    }
                    else if (command.BelongsToNamespace == "text box") {
                        if (command.Name == "create") {
                            output += "{ \"Text\": \"Hello world\" };";
                        }
                        else if (command.Name == "set text") {
                            output += "if (document.getElementById(\"txt-" + currentVariableName.replaceAll2(" ", "") + "\") != null) { document.getElementById(\"txt-" + currentVariableName.replaceAll2(" ", "") + "\").innerHTML = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + "; } else { " + currentVariableName + ".Text = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + " }";
                        }
                        else if (command.Name == "show") {
                            output += "document.getElementById(\"content\").innerHTML += \"<div id='txt-" + currentVariableName.replaceAll2(" ", "") + "' style='margin:8px;'>\" + " + currentVariableName + ".Text + \"</div>\";\n";
                            output += "setDisplay();";
                        }
                    }
                    else if (command.BelongsToNamespace == "check box") {
                        if (command.Name == "create") {
                            output += "{ \"Text\": \"Hello world\" };";
                        }
                        else if (command.Name == "set text") {
                            output += "if (document.getElementById(\"chklbl-" + currentVariableName.replaceAll2(" ", "") + "\") != null) { document.getElementById(\"chklbl-" + currentVariableName.replaceAll2(" ", "") + "\").innerHTML = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + "; } else { " + currentVariableName + ".Text = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + " }";
                        }
                        else if (command.Name == "set value") {
                            output += currentVariableName + ".Value = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";"
                        }
                        else if (command.Name == "get value") {
                            output += "document.getElementById(\"chk-" + currentVariableName + "\").checked;";
                        }
                        else if (command.Name == "show") {
                            output += "document.getElementById(\"content\").innerHTML += \"<div style='margin:8px;display:flex;flex-direction:row;align-items:center;'><input id='chk-" + currentVariableName.replaceAll2(" ", "") + "' type='checkbox' style='min-width:28px;min-height:28px;margin-right:12px;' checked=''><label id='chklbl-" + currentVariableName.replaceAll2(" ", "") + "' for='chk-" + currentVariableName.replaceAll2(" ", "") + "'>\" + " + currentVariableName + ".Text + \"</label></div>\";\n";
                            output += "setDisplay();\n";
                            output += "setTimeout(function() { document.getElementById(\"chk-" + currentVariableName + "\").checked = " + currentVariableName + ".Value }, 100);";
                        }
                    }
                    else if (command.BelongsToNamespace == "buttonbar") {
                        if (command.Name == "create") {
                            output += "[];";
                        }
                        else if (command.Name == "add") {
                            output += currentVariableName + ".push(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ");\n";
                            output += "if (document.getElementById(\"" + currentVariableName + "\") != null) {\n";
                            output += "    document.getElementById(\"" + currentVariableName + "\").innerHTML = \"\";";
                            output += "    for (var i = 0; i < " + currentVariableName + ".length; i++) {";
                            output += "        document.getElementById(\"" + currentVariableName + "\").innerHTML += \"<button id='btn-" + currentVariableName.replaceAll2(" ", "") + "\" + i + \"' style='margin:8px;' data-clickedIndex=-1 onclick='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clickedIndex = \" + i + \";'>\" + " + currentVariableName + "[i] + \"</button>\";";
                            output += "    }\n";
                            output += "    setDisplay();\n";
                            output += "}";
                        }
                        else if (command.Name == "show") {
                            output += "document.getElementById(\"content\").innerHTML += \"<div id='" + currentVariableName + "' style='display:flex;flex-direction:row;flex-wrap:wrap;' data-clicked-index=-1></div>\";";
                            output += "for (var i = 0; i < " + currentVariableName + ".length; i++) {";
                            output += "    document.getElementById(\"" + currentVariableName + "\").innerHTML += \"<button id='btn-" + currentVariableName.replaceAll2(" ", "") + "\" + i + \"' style='margin:8px;' data-clickedIndex=-1 onclick='document.getElementById(\\\"" + currentVariableName.replaceAll2(" ", "") + "\\\").dataset.clickedIndex = \" + i + \";'>\" + " + currentVariableName + "[i] + \"</button>\";";
                            output += "}\n";
                            output += "setDisplay();";
                        }
                        else if (command.Name == "clicked index") {
                            output += "document.getElementById(\"" + currentVariableName + "\").dataset.clickedIndex;\n";
                            output += "document.getElementById(\"" + currentVariableName + "\").dataset.clickedIndex = -1;\n";
                        }
                        else if (command.Name == "clicked name") {
                            output += "document.getElementById(\"" + currentVariableName + "\").dataset.clickedIndex > -1 ? " + currentVariableName + "[document.getElementById(\"" + currentVariableName + "\").dataset.clickedIndex] : \"\";\n";
                            output += "document.getElementById(\"" + currentVariableName + "\").dataset.clickedIndex = -1;\n";
                        }
                    }
                    else if (command.BelongsToNamespace == "data grid") {
                        if (command.Name == "create") {
                            output += "{ \"ColumnCount\": 0 };";
                        }
                        else if (command.Name == "set column count") {
                            output += currentVariableName + ".ColumnCount = " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ";"
                        }
                        else if (command.Name == "selected row") {
                            output += "(document.getElementById(\"dg" + currentVariableName + "\") != null ? document.getElementById(\"dg" + currentVariableName + "\").dataset.row : -1);";
                        }
                        else if (command.Name == "selected cell") {
                            output += "(document.getElementById(\"dg" + currentVariableName + "\") != null ? document.getElementById(\"dg" + currentVariableName + "\").dataset.cell : -1);";
                        }
                        else if (command.Name == "show") {
                            var variableName = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var variableCommand = getVariableCommand(func, variableName);
                            if (variableCommand == null)
                                variableCommand = getVariableCommand(func, variableName.replace(/_/g, " "));
                            if (variableCommand != null) {
                                var classType = variableCommand.VariableType;
                                if (classType.endsWith(" list"))
                                    classType = classType.substr(0, classType.length - 5);

                                var classFunction = getClass(classType);
                                if (classFunction != null) {
                                    output += currentVariableName + "Properties = [";
                                    for (var i = 0; i < classFunction.Commands.length; i++) {
                                        output += "\"" + classFunction.Commands[i].Expressions[0].Name + "\"";
                                        if (i < classFunction.Commands.length - 1)
                                            output += ",";
                                    }
                                    output += "];\n"
                                    output += currentVariableName + "Html = \"<table id=\\\"dg" + currentVariableName + "\\\" data-row=\\\"-1\\\" data-cell=\\\"-1\\\"><thead><tr>\";\n";
                                    output += "for (var i = 0; i < " + currentVariableName + "Properties.length; i++) {\n";
                                    output += currentVariableName + "Html += \"<th>\" + " + currentVariableName + "Properties[i] + \"</th>\";\n";
                                    output += "if (" + currentVariableName + ".ColumnCount > 0 && " + currentVariableName + ".ColumnCount - 1 == i) break;"
                                    output += "}\n"
                                    output += currentVariableName + "Html += \"</tr></thead>\";\n";
                                    output += currentVariableName + "Html += \"<tbody>\";\n";
                                    output += "for (var r = 0; r < " + variableName + ".length; r++) {\n";
                                    output += currentVariableName + "Html += \"<tr>\";\n";
                                    output += "for (var i = 0; i < " + currentVariableName + "Properties.length; i++) {\n";
                                    output += currentVariableName + "Html += \"<td onclick=\\\"document.getElementById('dg" + currentVariableName + "').dataset.row = \" + r + \"; document.getElementById('dg" + currentVariableName + "').dataset.cell = \" + i + \";\\\">\" + parseValue(" + variableName + "[r][" + currentVariableName + "Properties[i].replace(\" \",\"_\")]) + \"</td>\";\n";
                                    output += "if (" + currentVariableName + ".ColumnCount > 0 && " + currentVariableName + ".ColumnCount - 1 == i) break;"
                                    output += "}\n"
                                    output += currentVariableName + "Html += \"</tr>\";\n";
                                    output += "}\n"
                                    output += currentVariableName + "Html += \"</tbody></table>\";\n";
                                    output += "document.getElementById(\"content\").innerHTML += " + currentVariableName + "Html;\n";
                                    output += "setDisplay();"
                                }
                            }
                        }
                    }
                    else if (command.BelongsToNamespace == "app") {
                        if (command.Name == "restart") {
                            output += "appMenuItems = [];\n"
                            output += "document.getElementById(\"content\").innerHTML = \"\";\n";
                            output += "main();";
                        }
                        else if (command.Name == "duration") {
                            output += "(new Date() - startDate) / 1000;";
                        }
                    }
                    else if (command.BelongsToNamespace == "string") {
                        if (command.Name == "to lowercase") {
                            output += currentVariableName + ".toLowerCase()";
                        }
                        else if (command.Name == "to uppercase") {
                            output += currentVariableName + ".toUpperCase()";
                        }
                        else if (command.Name == "substring") {
                            output += currentVariableName + ".substr(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ")";
                        }
                        else if (command.Name == "replace") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += currentVariableName + ".replaceAll2(" + parameterList[0] + ", " + parameterList[1] + ")";
                        }
                        else if (command.Name == "append") {
                            output += currentVariableName + " += " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "set") {
                            output += currentVariableName + " = " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "clear") {
                            output += currentVariableName + " = \"\"";
                        }
                        else if (command.Name == "get length") {
                            output += currentVariableName + ".length";
                        }
                        else if (command.Name == "split") {
                            output += currentVariableName + ".split(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ")";
                        }
                        else if (command.Name == "contains") {
                            output += currentVariableName + ".includes(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ")";
                        }
                        else if (command.Name == "starts with") {
                            output += currentVariableName + ".startsWith(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ")";
                        }
                        else if (command.Name == "ends with") {
                            output += currentVariableName + ".endsWith(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ")";
                        }
                        else if (command.Name == "to object") {
                            output += "JSON.parse(" + currentVariableName + ")";
                        }
                        else if (command.Name == "download") {
                            output += "download('data:text/plain;,' + " + currentVariableName + ", " + parseExpressionsForJs(func, expressions[i].Expressions, false) + ");";
                        }
                    }
                    else if (command.BelongsToNamespace == "number") {
                        if (command.Name == "add") {
                            output += currentVariableName + " += " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "subtract") {
                            output += currentVariableName + " -= " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "multiply") {
                            output += currentVariableName + " *= " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "divide") {
                            output += currentVariableName + " /= " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                        else if (command.Name == "set") {
                            output += currentVariableName + " = " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                    }
                    else if (command.BelongsToNamespace == "boolean") {
                        if (command.Name == "switch") {
                            output += currentVariableName + " = !" + currentVariableName;
                        }
                        else if (command.Name == "set true") {
                            output += currentVariableName + " = true";
                        }
                        else if (command.Name == "set false") {
                            output += currentVariableName + " = false";
                        }
                        else if (command.Name == "set") {
                            output += currentVariableName + " = " + parseExpressionsForJs(func, expressions[i].Expressions, false);
                        }
                    }
                }
                else if (classObject != null) {
                    output += "{";
                    for (var cc = 0; cc < classObject.Commands.length; cc++) {
                        if (classObject.Commands[cc].Expressions != null && classObject.Commands[cc].Expressions[0] != null) {
                            var propValue = parseExpressionsForJs(func, classObject.Commands[cc].Expressions[0].Expressions, false)
                            output += "\"" + classObject.Commands[cc].Expressions[0].Name.replace(/ /g, "_") + "\": " + propValue + ",";
                        }
                    }
                    if (output.endsWith(",")) {
                        output = output.substr(0, output.length - 1);
                    }
                    if (output.endsWith(";")) {
                        output = output.substr(0, output.length - 1);
                    }
                    output += "}";
                }
                else if (classListObject != null) {
                    var command = commandsReference.find((item) => (item.BelongsToNamespace == "list" || item.BelongsToNamespace == expressions[i].Namespace) && item.Name == expressions[i].Name);
                    if (command != null) {
                        if (command.Name == "create") {
                            output += "[];";
                        }
                        else if (command.Name == "clear") {
                            output += currentVariableName + " = [];";
                        }
                        else if (command.Name == "add") {
                            output += currentVariableName + ".push(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ");";
                        }
                        else if (command.Name == "insert") {
                            var parameter = parseExpressionsForJs(func, expressions[i].Expressions, false);
                            var parameterList = parameter.split(",");
                            output += currentVariableName + ".splice(" + parameterList[0] + ", 0, " + parameterList[1] + ");";
                        }
                        else if (command.Name == "remove") {
                            output += currentVariableName + ".splice(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ", 1);";
                        }
                        else if (command.Name == "get by index") {
                            output += currentVariableName + "[" + parseExpressionsForJs(func, expressions[i].Expressions, false) + "];";
                        }
                        else if (command.Name == "get length") {
                            output += currentVariableName + ".length;";
                        }
                        else if (command.Name == "get first") {
                            output += "(" + currentVariableName + ".length > 0 ? " + currentVariableName + "[0] : '');";
                        }
                        else if (command.Name == "get last") {
                            output += "(" + currentVariableName + ".length > 0 ? " + currentVariableName + "[" + currentVariableName + ".length - 1] : '');";
                        }
                        else if (command.Name == "to string") {
                            output += "JSON.stringify(" + currentVariableName + ");";
                        }
                    }
                    else {
                        output += "await " + expressions[i].Name.replace(/ /g, "_") + "(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ")";
                    }
                }
                else {
                    output += "await " + expressions[i].Name.replace(/ /g, "_") + "(" + parseExpressionsForJs(func, expressions[i].Expressions, false) + ")";
                }
            }
            else if (expressions[i].Type == "value") {
                if (expressions[i].DataType == "string") {
                    output += "\"" + expressions[i].Name.replace(/\"/g, "\\\"") + "\" ";
                }
                else if (expressions[i].DataType == "number" || expressions[i].DataType == "boolean") {
                    output += " " + expressions[i].Name + " ";
                }
            }
            else if (expressions[i].Type == "operator") {
                if (expressions[i].Name == "=" && replaceEquals)
                    output += " == ";
                else if (expressions[i].Name == "or")
                    output += " || ";
                else if (expressions[i].Name == "and")
                    output += " && ";
                else if (expressions[i].Name == "not")
                    output += " ! ";
                else if (expressions[i].Name == "≠")
                    output += " != ";
                else if (expressions[i].Name == "mod")
                    output += " % ";
                else if (expressions[i].Name != "&rarr;")
                    output += " " + expressions[i].Name + " ";
            }
            else if (expressions[i].Type == "variable") {
                var variableName = "";
                if (expressions[i].ClassVariable != null && expressions[i].ClassVariable.length > 0) {
                    variableName += expressions[i].ClassVariable.replace(/ /g, "_") + ".";
                }
                variableName += expressions[i].Name.replace(/ /g, "_");
                output += variableName;
            }
            else if (expressions[i].Type == "variable-declaration") {
                currentVariableName = expressions[i].Name.replace(/ /g, "_");
                output += "var " + currentVariableName + " = " + parseExpressionsForJs(func, expressions[i].Expressions, false);
            }
            else if (expressions[i].Type == "variable-assign") {
                currentVariableName = expressions[i].Name.replace(/ /g, "_");
                output += expressions[i].Name.replace(/ /g, "_") + " = " + parseExpressionsForJs(func, expressions[i].Expressions, false);
            }
            else if (expressions[i].Type == "variable-assign-namespace") {
                if (currentClass.length > 0) {
                    currentVariableName = currentClassVariableName.replace(/ /g, "_") + "." + expressions[i].Name.replace(/ /g, "_");
                    currentNamespace = getVariableTypeByClass(currentClass, expressions[i].Name.replace(/ /g, "_"));
                }
                else {
                    currentVariableName = expressions[i].Name.replace(/ /g, "_");
                    var varCommand = getVariableCommand(func, expressions[i].Name);
                    if (varCommand != null) {
                        currentNamespace = varCommand.VariableType;
                    }
                    output += parseExpressionsForJs(func, expressions[i].Expressions, false);
                }
            }
            else if (expressions[i].Type == "reserved-word") {
                output += expressions[i].Name + " ";
            }
        }
    }
    return output;
}

function generateOutputHTML() {
    var jsString = exportJs();

    var hasGeoLocation = false;
    var hasQrCode = false;
    var hasCodeScanner = false;
    for (var i = 0; i < functions.length; i++) {
        var geoCommand = functions[i].Commands.find((item) => item.VariableType == "geolocation");
        if (geoCommand != null)
            hasGeoLocation = true;
        var qrCommand = functions[i].Commands.find((item) => item.Expressions != null && item.Expressions.length > 1 && item.Expressions[1] != null && item.Expressions[1].Name == "qrcode");
        if (qrCommand != null)
            hasQrCode = true;
        var codeScannerCommand = functions[i].Commands.find((item) => item.Expressions != null && item.Expressions.length > 0 && item.Expressions[0] != null && item.Expressions[0].Expressions != null && item.Expressions[0].Expressions.length > 1 && item.Expressions[0].Expressions[1].Name == "scan code");
        if (codeScannerCommand != null)
            hasCodeScanner = true;
    }

    var ubuntuTheme = currentProject.Options != null && currentProject.Options.UbuntuTheme;
    var htmlString = "<!DOCTYPE html>\n";
    htmlString += "<html>\n";
    htmlString += "<head>\n";
    htmlString += "<title>" + currentProject.Name + "</title>\n";
    htmlString += "<meta charset=\"utf-8\" />";
    htmlString += "<meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0\" />\n";
    if (hasGeoLocation)
        htmlString += "<link rel=\"stylesheet\" href=\"https://openlayers.org/en/v4.6.5/css/ol.css\">\n";
    htmlString += "<style>\n";
    if (ubuntuTheme) {
        htmlString += "body {\n";
        htmlString += "    font-family: 'Ubuntu', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;\n";
        htmlString += "    font-weight: 300;\n";
        htmlString += "}\n\n";
        htmlString += ".modal {\n";
        htmlString += "    position: fixed;\n";
        htmlString += "    left: 0;\n";
        htmlString += "    top: 0;\n";
        htmlString += "    right: 0;\n";
        htmlString += "    background-color: white;\n";
        htmlString += "    box-shadow: 0 0 20px black;\n"
        htmlString += "    border-top-left-radius: 10px;\n"
        htmlString += "    border-top-right-radius: 10px;\n"
        htmlString += "    padding: 12px;\n";
        htmlString += "    margin: 8px;\n";
        htmlString += "}\n\n";
        htmlString += "button {\n";
        htmlString += "    color: black !important;\n";
        htmlString += "    font-weight: 300;\n";
        htmlString += "    padding: 8px 22px 8px 22px;";
        htmlString += "    border-radius: 6px;\n";
        htmlString += "    border: 1px solid black;\n";
        htmlString += "    outline: dodgerblue;\n";
        htmlString += "}\n\n";
        htmlString += "button:active {\n";
        htmlString += "    background-color: #666666;\n";
        htmlString += "}\n\n";
        htmlString += "input {\n";
        htmlString += "    font-weight: 300;\n";
        htmlString += "    padding: 8px;\n";
        htmlString += "    border-radius: 6px;\n";
        htmlString += "    border: 1px solid black;\n";
        htmlString += "    outline:dodgerblue;\n";
        htmlString += "}\n\n";
        htmlString += "table {\n";
        htmlString += "    border-collapse: collapse;\n";
        htmlString += "}\n\n";
        htmlString += "table thead {\n";
        htmlString += "    background-color: whitesmoke;\n";
        htmlString += "}\n\n";
        htmlString += "table th, table td {\n";
        htmlString += "    padding: 8px;\n";
        htmlString += "}\n\n";
        htmlString += "table th {\n";
        htmlString += "    text-align: left;\n";
        htmlString += "}\n\n";
        htmlString += "table td {\n";
        htmlString += "    border-top: 1px solid lavender;\n";
        htmlString += "}\n\n";
        htmlString += "#top-bar {\n";
        htmlString += "    position: fixed;\n";
        htmlString += "    left: 0;\n";
        htmlString += "    top: 0;\n";
        htmlString += "    right: 0;\n";
        htmlString += "    max-height: 50px;\n";
        htmlString += "    padding: 0;\n";
        htmlString += "    display: flex;\n";
        htmlString += "    flex-direction: row;\n";
        htmlString += "    align-items: center;\n";
        htmlString += "    justify-content: space-between;\n";
        htmlString += "    background-color: #19B6EE;\n";
        htmlString += "}\n\n";
        htmlString += "#top-bar-menu-btn {\n";
        htmlString += "    background-image:url(/usr/share/icons/suru/actions/scalable/navigation-menu.svg);\n";
        htmlString += "    background-position:center;\n";
        htmlString += "    background-size:70%;\n";
        htmlString += "    background-repeat:no-repeat;\n";
        htmlString += "    width:32px;\n";
        htmlString += "    height:32px;\n";
        htmlString += "    margin-right:8px;\n";
        htmlString += "    filter:brightness(0);\n";
        htmlString += "}\n\n";
        htmlString += "#top-bar-menu-btn:hover, #top-bar-display-btn:hover {\n";
        htmlString += "    filter:brightness(1);\n";
        htmlString += "    background-color: #5D5D5D;\n";
        htmlString += "}\n\n";
        htmlString += "#top-bar-display-btn {\n";
        htmlString += "    background-position:center;\n";
        htmlString += "    background-size:70%;\n";
        htmlString += "    background-repeat:no-repeat;\n";
        htmlString += "    width:32px;\n";
        htmlString += "    height:32px;\n";
        htmlString += "    margin-right:8px;\n";
        htmlString += "    filter:brightness(0);\n";
        htmlString += "}\n\n";
        htmlString += "#top-bar-menu {\n";
        htmlString += "    position: fixed;\n";
        htmlString += "    top: 40px;\n";
        htmlString += "    right: 0;\n";
        htmlString += "    bottom: 0;\n";
        htmlString += "    max-width: 600px;\n";
        htmlString += "    padding: 8px;\n";
        htmlString += "    display: flex;\n";
        htmlString += "    flex-direction: column;\n";
        htmlString += "    overflow: auto;\n";
        htmlString += "    z-index: 10;\n";
        htmlString += "    background-color: #CDCDCD;\n";
        htmlString += "    transition: 0.2s;\n";
        htmlString += "}\n\n";
        htmlString += ".menu-item {\n";
        htmlString += "    padding: 8px;\n";
        htmlString += "    cursor: pointer;\n";
        htmlString += "}\n\n";
        htmlString += ".menu-item:hover {\n";
        htmlString += "    background-color: #19B6EE;\n";
        htmlString += "}\n\n";
        htmlString += ".dark-general {\n";
        htmlString += "    background-color: #3B3B3B;\n";
        htmlString += "    color: white;\n";
        htmlString += "}\n\n";
        htmlString += ".dark-input {\n";
        htmlString += "    background-color: #3B3B3B;\n";
        htmlString += "    color: white;\n";
        htmlString += "    border: 1px solid white;\n";
        htmlString += "}\n\n";
        htmlString += ".dark-button {\n";
        htmlString += "    background-color: #3EB34F;\n";
        htmlString += "    color: #F7F7F7;\n";
        htmlString += "}\n\n";
        htmlString += ".dark-menu {\n";
        htmlString += "    background-color: #111111 !important;\n";
        htmlString += "}\n\n";
        htmlString += ".dark-top-bar {\n";
        htmlString += "    background-color: #3EB34F !important;\n";
        htmlString += "}\n\n";
        htmlString += ".btn-dark-mode {\n";
        htmlString += "    background-image: url(/usr/share/icons/suru/actions/scalable/night-mode.svg);\n";
        htmlString += "}\n";
        htmlString += "\n";
        htmlString += ".btn-light-mode {\n";
        htmlString += "    background-image: url(/usr/share/icons/suru/status/scalable/display-brightness-min.svg);\n";
        htmlString += "}\n";
    }
    else {
        htmlString += "body {\n";
        htmlString += "    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;\n";
        htmlString += "    font-size: 1.2rem;\n";
        htmlString += "}\n\n";
        htmlString += ".modal {\n";
        htmlString += "    position: fixed;\n";
        htmlString += "    left: 0;\n";
        htmlString += "    top: 0;\n";
        htmlString += "    right: 0;\n";
        htmlString += "    background-color: white;\n";
        htmlString += "    box-shadow: 0 0 20px black;\n"
        htmlString += "    border-top-left-radius: 10px;\n"
        htmlString += "    border-top-right-radius: 10px;\n"
        htmlString += "    padding: 12px;\n";
        htmlString += "    margin: 8px;\n";
        htmlString += "}\n\n";
        htmlString += "button {\n";
        htmlString += "    font-size: 1.2rem;";
        htmlString += "    padding: 8px 22px 8px 22px;";
        htmlString += "}\n\n";
        htmlString += "input {\n";
        htmlString += "    font-size: 1.2rem;";
        htmlString += "    padding: 8px;";
        htmlString += "}\n\n";
        htmlString += "table {\n";
        htmlString += "    border-collapse: collapse;\n";
        htmlString += "}\n\n";
        htmlString += "table thead {\n";
        htmlString += "    background-color: whitesmoke;\n";
        htmlString += "}\n\n";
        htmlString += "table th, table td {\n";
        htmlString += "    padding: 8px;\n";
        htmlString += "}\n\n";
        htmlString += "table th {\n";
        htmlString += "    text-align: left;\n";
        htmlString += "}\n\n";
        htmlString += "table td {\n";
        htmlString += "    border-top: 1px solid lavender;\n";
        htmlString += "}\n\n";
        htmlString += "#top-bar {\n";
        htmlString += "    position: fixed;\n";
        htmlString += "    left: 0;\n";
        htmlString += "    top: 0;\n";
        htmlString += "    right: 0;\n";
        htmlString += "    max-height: 50px;\n";
        htmlString += "    padding: 0;\n";
        htmlString += "    display: flex;\n";
        htmlString += "    flex-direction: row;\n";
        htmlString += "    align-items: center;\n";
        htmlString += "    justify-content: space-between;\n";
        htmlString += "    border-bottom: 1px solid dimgray;\n";
        htmlString += "}\n\n";
        htmlString += "#top-bar-menu-btn {\n";
        htmlString += "    background-image:url(/usr/share/icons/suru/actions/scalable/navigation-menu.svg);\n";
        htmlString += "    background-position:center;\n";
        htmlString += "    background-size:70%;\n";
        htmlString += "    background-repeat:no-repeat;\n";
        htmlString += "    width:32px;\n";
        htmlString += "    height:32px;\n";
        htmlString += "    margin-right:8px;\n";
        htmlString += "}\n\n";
        htmlString += "#top-bar-menu-btn:hover {\n";
        htmlString += "    background-color: silver;\n";
        htmlString += "}\n\n";
        htmlString += "#top-bar-menu {\n";
        htmlString += "    position: fixed;\n";
        htmlString += "    top: 40px;\n";
        htmlString += "    right: 0;\n";
        htmlString += "    bottom: 0;\n";
        htmlString += "    max-width: 600px;\n";
        htmlString += "    padding: 8px;\n";
        htmlString += "    display: flex;\n";
        htmlString += "    flex-direction: column;\n";
        htmlString += "    overflow: auto;\n";
        htmlString += "    z-index: 10;\n";
        htmlString += "    background-color: lightgreen;\n";
        htmlString += "    transition: 0.2s;\n";
        htmlString += "}\n\n";
        htmlString += ".menu-item {\n";
        htmlString += "    padding: 8px;\n";
        htmlString += "    cursor: pointer;\n";
        htmlString += "}\n\n";
        htmlString += ".menu-item:hover {\n";
        htmlString += "    background-color: dodgerblue;\n";
        htmlString += "}\n\n";
    }
    htmlString += "</style>\n";
    htmlString += "</head>\n";
    htmlString += "<body onload=\"main()\">\n";
    htmlString += "<div id=\"top-bar\">\n";
    htmlString += "    <div id=\"top-bar-menu-btn\" style=\"display:none;\" onclick=\"switchAppMenu()\"></div>\n";
    htmlString += "    <div id=\"top-bar-title\" style=\"padding:8px;width:100%;\">" + currentProject.Name + "</div>\n";
    htmlString += "    <div id=\"top-bar-display-btn\" class=\"btn-dark-mode\" onclick=\"switchDisplay()\"></div>\n";
    htmlString += "</div>\n";

    htmlString += "<div id=\"top-bar-menu\" style=\"left:-620px;\"></div>\n";
    htmlString += "<div id=\"content\" style=\"position:fixed;left:0;top:52px;right:0;bottom:0;padding:8px;display:flex;flex-direction:column;overflow:auto;\"></div>\n";
    htmlString += "\n";
    htmlString += "<div id=\"modal-input\" class=\"modal\" style=\"display:none;\">\n";
    htmlString += "<div id=\"modal-input-prompt\" style=\"margin-bottom:12px;\"></div>\n";
    htmlString += "<div id=\"modal-input-line\" style=\"margin-bottom:12px;\"><input id=\"modal-input-value\" type=\"text\" style=\"width:calc(100% - 20px);\"></div>\n";
    htmlString += "<div id=\"modal-input-footer\" style=\"text-align:center;border-top:1px solid dimgray;padding-top:12px;\"><button onclick=\"closeDialog()\">OK</button></div>\n";
    htmlString += "</div>\n";
    htmlString += "\n";
    htmlString += "<div id=\"modal-confirm\" class=\"modal\" style=\"display:none;\">\n";
    htmlString += "<div id=\"modal-confirm-prompt\" style=\"margin-bottom:12px;\"></div>\n";
    htmlString += "<div id=\"modal-confirm-footer\" style=\"text-align:center;padding-top:12px;\">\n";
    htmlString += "<button id=\"modal-confirm-button-1\" onclick=\"closeConfirm(this.innerText)\">Button 1</button>\n";
    htmlString += "<button id=\"modal-confirm-button-2\" onclick=\"closeConfirm(this.innerText)\">Button 2</button>\n";
    htmlString += "<button id=\"modal-confirm-button-3\" onclick=\"closeConfirm(this.innerText)\">Button 3</button>\n";
    htmlString += "<button id=\"modal-confirm-button-4\" onclick=\"closeConfirm(this.innerText)\">Button 4</button>\n";
    htmlString += "</div>\n";
    htmlString += "</div>\n";
    htmlString += "\n";
    htmlString += "<div id=\"camera\" style=\"display:none;position:fixed;left:0;top:0;right:0;bottom:0;background-color:black;\">\n";
    htmlString += "    <video autoplay style=\"width:100%;height:100%;\"></video>\n";
    htmlString += "    <div style=\"position:fixed;left:0;bottom:0;right:0;height:52px;display:flex;flex-direction:row;align-items:center;justify-content:center;\">\n";
    htmlString += "        <button style=\"background-image:url(/usr/share/icons/suru/devices/scalable/camera-photo-symbolic.svg);background-size:60%;background-position:center;background-repeat:no-repeat;width:68px;height:68px;background-color:black;border:2px solid white;\" onclick=\"takePicture()\"></button>\n";
    htmlString += "        <button style=\"background-image:url(/usr/share/icons/suru/actions/scalable/camera-flip.svg);background-size:60%;background-position:center;background-repeat:no-repeat;width:68px;height:68px;background-color:black;border:2px solid white;\" onclick=\"switchCamera()\"></button>\n";
    htmlString += "    </div>\n";
    htmlString += "    <img src=\"\">\n";
    htmlString += "    <canvas style=\"display:none;\"></canvas>\n";
    htmlString += "</div>\n";
    htmlString += "<div id=\"codescanner\" style=\"display:none;position:fixed;left:0;top:0;right:0;bottom:0;background-color:black;\">\n";
    htmlString += "    <div id=\"codereader\"></div>\n";
    htmlString += "    <div style=\"position:fixed;left:0;bottom:0;right:0;height:52px;display:flex;flex-direction:row;align-items:center;justify-content:center;\">\n";
    htmlString += "        <button style=\"background-image:url(/usr/share/icons/suru/actions/scalable/cancel.svg);background-size:60%;background-position:center;background-repeat:no-repeat;width:68px;height:68px;background-color:black;border:2px solid white;\" onclick=\"cancelCodeScan()\"></button>\n";
    htmlString += "    </div>\n";
    htmlString += "</div>\n";
    if (hasQrCode)
        htmlString += "<div id=\"qrcode\" style=\"display:none;\"></div>"
    if (hasGeoLocation)
        htmlString += "<script src=\"https://openlayers.org/en/v4.6.5/build/ol.js\"></script>\n";
    htmlString += "<script>\n";
    if (hasQrCode)
        htmlString += getQRCodeLib() + "\n"
                    + "var qrcode = new QRCode(document.getElementById(\"qrcode\"), \"\");\n";
    if (hasCodeScanner)
        htmlString += getCodeScannerLib() + "\n";
    htmlString += jsString + "\n";
    htmlString += "</script>\n";
    htmlString += "</body>\n</html>";
    return htmlString;
}

String.prototype.replaceAll2 = function (oldValue, newValue) {
    var result = this;
    while (result.includes(oldValue.toLowerCase())) {
        result = result.replace(oldValue.toLowerCase(), newValue);
    }
    while (result.includes(oldValue.toUpperCase())) {
        result = result.replace(oldValue.toUpperCase(), newValue);
    }
    return result;
};
