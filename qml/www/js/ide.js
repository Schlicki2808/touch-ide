var projects = [];
var functions = [];
var commands = [];
var selectedCommands = [];
var copiedCommands = [];
var currentProject = null;
var currentFunction = null;
var currentCommand = null;
var currentParentExpression = null;
var currentExpression = null;
var functionSelected = false;
var currentFunctionParameter = null;
var selectionMode = false;
var desktopMode = false;
var liveexecution = true;
var syncenabled = false;
var options = null;
var sessionId = '';
var touchEventActive = false;
var touchedCommandId = '';
var touchedCommandStart = -99;
var touchedCommandEnd = -99;
var touchedScrollTop = 0;
var touchedScrollLeft = 0;
var scrollTopDiff = 0;

function generateGuid() { 
    var d = new Date().getTime();//Timestamp
    var d2 = (performance && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

function showCommandPanel(which, event) {
    var elementName = "command-panel-" + which;
    var panelElement = document.getElementById(elementName);
    if (panelElement != null) {
        panelElement.style.height = "220px";
        document.getElementById("code-container").style.bottom = "220px";
    }
    event.stopImmediatePropagation();
}

function hideCommandPanel(event) {
    var panels = document.getElementsByClassName("command-panel");
    for (var i = 0; i < panels.length; i++) {
        panels[i].style.height = "0";
    }
    document.getElementById("code-container").style.bottom = "0";
    event.stopImmediatePropagation();
}

function commandTouchStart(commandId, event) {
    if (document.getElementById("code-container").scrollLeft == 0) {
        touchedCommandId = commandId;
        touchedCommandStart = event.touches[0].pageX;
        touchedCommandEnd = event.touches[0].pageX;
        var command = commands.find((item) => item.Id == commandId);
        if (command != null && command.Disabled && document.getElementById("code-container").scrollLeft == 0)
            document.getElementById("code-container").style.overflowX = "hidden";
        touchedScrollTop = document.getElementById("code-container").scrollTop;
        touchedScrollLeft = document.getElementById("code-container").scrollLeft;
        document.getElementById("touch-marker").style.top = (event.touches[0].pageY - 16) + "px";
        document.getElementById("touch-marker").style.backgroundColor = "black";
        touchEventActive = true;
    }
}

function commandTouchMove(commandId, event) {
    if (touchEventActive) {
        touchedCommandEnd = event.touches[0].pageX;
        var newScrollTop = document.getElementById("code-container").scrollTop;
        scrollTopDiff = Math.abs(newScrollTop - touchedScrollTop);
        var diff = touchedCommandEnd - touchedCommandStart;
        if (Math.abs(diff) > 10 && scrollTopDiff <= 5)
            document.getElementById("touch-marker").style.display = "";
        else
            document.getElementById("touch-marker").style.display = "none";
        if (Math.abs(diff) > 50)
            document.getElementById("touch-marker").style.backgroundColor = "green";
        else
            document.getElementById("touch-marker").style.backgroundColor = "black";
        document.getElementById("touch-marker").style.width = event.touches[0].pageX + "px";
    }
}

function commandTouchEnd(commandId, event) {
    touchedCommandId = '';
    document.getElementById("touch-marker").style.display = "none";
    if (touchEventActive && scrollTopDiff <= 5 && touchedScrollLeft < 5) {
        var diff = touchedCommandEnd - touchedCommandStart;
        if (diff > 50) {
            // auskommentieren
            commentOut(commandId, event);
        }
        else if (diff < -50) {
            // einkommentieren
            commentIn(commandId, event);
        }
    }
    touchEventActive = false;
    touchedCommandStart = -99;
    touchedScrollLeft = 0;
    document.getElementById("code-container").style.overflowX = "auto";
}

function commandMouseDown(commandId, event) {
    touchedCommandId = commandId;
    touchedCommandStart = event.clientX;
}

function commandMouseUp(commandId, event) {
    touchedCommandId = '';
    var diff = event.clientX - touchedCommandStart;
    if (diff > 50) {
        // auskommentieren
        commentOut(commandId, event);
    }
    else if (diff < -50) {
        // einkommentieren
        commentIn(commandId, event);
    }
    touchedCommandStart = -99;
}

function commentOut(commandId, event) {
    var startIndex = commands.findIndex((item) => item.Id == commandId);
    var endIndex = commands.findIndex((item) => item.SourceId == commandId);
    if (endIndex >= 0) {
        for (var i = endIndex; i >= startIndex; i--) {
            commands[i].Disabled = true;
        }
    }
    else {
        commands[startIndex].Disabled = true;
    }
    unselectCommand(event);
    updateCommandPanel(event);
    displayCodeContainer();
}

function commentIn(commandId, event) {
    var startIndex = commands.findIndex((item) => item.Id == commandId);
    var endIndex = commands.findIndex((item) => item.SourceId == commandId);
    if (endIndex >= 0) {
        for (var i = endIndex; i >= startIndex; i--) {
            commands[i].Disabled = false;
        }
    }
    else {
        commands[startIndex].Disabled = false;
    }
    unselectCommand(event);
    updateCommandPanel(event);
    displayCodeContainer();
}

function selectCommand(commandId, event) {
    if (selectionMode) {
        var existingIndex = selectedCommands.findIndex((item) => item.Id == commandId);
        if (existingIndex >= 0) {
            selectedCommands.splice(existingIndex, 1);
        }
        else
        {
            var selectedCommand = commands.find((item) => item.Id == commandId);
            if (selectedCommand.Type == "command" || selectedCommand.Type == "empty")
                selectedCommands.push(selectedCommand);
        }
    }
    else {
        if (currentCommand == null || currentCommand.Id != commandId) {
            var command = commands.find((item) => item.Id == commandId);
            if (command.Type == "end")
                command = commands.find((item) => item.Id == command.SourceId);
            currentCommand = command;
            currentExpression = null;
            currentParentExpression = null;
            functionSelected = false;
            currentFunctionParameter = null;
            updateCommandPanel(event);
            displayCodeContainer();
            if (document.getElementById("cmd-" + commandId) != null)
                setTimeout(function() { document.getElementById("cmd-" + commandId).scrollIntoView({ "behavior" : "smooth", "block": "center"}); }, 250);
        }
    }
}

function selectFunctionHeader(event) {
    var functionIndex = currentProject.Functions.findIndex((item) => item.Id == currentFunction.Id);
    if (functionIndex > 0 && functions[functionIndex].Type == "function") {
        currentCommand = null;
        currentExpression = null;
        functionSelected = true;
        currentFunctionParameter = null;
        updateCommandPanel(event);
        displayCodeContainer();
    }
}

function selectFunctionParameter(id, event) {
    unselectCommand(event);
    functionSelected = false;
    currentFunctionParameter = currentFunction.Parameters.find((item) => item.Id == id);
    updateCommandPanel(event);
    displayCodeContainer();
}

function updateCommandPanel(event) {
    hideCommandPanel(event);

    var commandPanelName = "";
    if (currentCommand != null) {
        if (currentCommand.Type == "command" && currentCommand.Expressions != null && currentCommand.Expressions.length > 0) {
            if (currentExpression != null) {
                commandPanelName = currentExpression.DataType;
            }
            else if (currentCommand.Expressions[0].Type == "namespace") {
                commandPanelName = currentCommand.Expressions[0].Name;
            }
            else if (currentCommand.Expressions[0].Type == "variable-assign" || currentCommand.Expressions[0].Type == "variable-assign-namespace") {
                var variableName = currentCommand.Expressions[0].Name;
                var variableType = "";
                if (currentCommand.ClassName != null && currentCommand.ClassName.length > 0) {
                    variableType = getVariableTypeByClass(currentCommand.ClassName, variableName);
                }
                else {
                    var varCommand = getVariableCommand(currentFunction, variableName);
                    if (varCommand != null) {
                        variableType = varCommand.VariableType;
                    }
                }
                if (variableType.endsWith(" list")) {
                    commandPanelName = "list"
                }
                else {
                    if (variableType == "collection") {
                        commandPanelName = "collection";
                    }
                    else if (variableType == "dictionary") {
                        commandPanelName = "dictionary";
                    }
                    else if (variableType == "datetime") {
                        commandPanelName = "datetime";
                    }
                    else if (variableType == "web request") {
                        commandPanelName = "webrequest";
                    }
                    else if (variableType == "web response") {
                        commandPanelName = "webresponse";
                    }
                    else if (variableType == "image") {
                        commandPanelName = "image";
                    }
                    else if (variableType == "button") {
                        commandPanelName = "button";
                    }
                    else if (variableType == "text input") {
                        commandPanelName = "textinput";
                    }
                    else if (variableType == "number input") {
                        commandPanelName = "numberinput";
                    }
                    else if (variableType == "text box") {
                        commandPanelName = "textbox";
                    }
                    else if (variableType == "check box") {
                        commandPanelName = "checkbox";
                    }
                    else if (variableType == "buttonbar") {
                        commandPanelName = "buttonbar";
                    }
                    else if (variableType == "data grid") {
                        commandPanelName = "datagrid";
                    }
                    else if (variableType == "app") {
                        commandPanelName = "app";
                    }
                    else if (variableType == "geolocation") {
                        commandPanelName = "geolocation";
                    }
                    else if (variableType == "string") {
                        commandPanelName = "string-ns";
                    }
                    else if (variableType == "number") {
                        commandPanelName = "number-ns";
                    }
                    else if (variableType == "boolean") {
                        commandPanelName = "boolean-ns";
                    }
                    else if (variableType == "object") {
                        commandPanelName = "object-ns";
                    }
                }
            }
            else if (currentCommand.Expressions[currentCommand.Expressions.length - 1].Type != "namespace" && currentCommand.Expressions[currentCommand.Expressions.length - 1].Expressions != null && currentCommand.Expressions[currentCommand.Expressions.length - 1].Expressions.length == 0) {
                commandPanelName = "";
            }
        }
        else if (currentCommand.Type == "for" && currentExpression != null) {
            commandPanelName = "number";
        }
        else if (currentCommand.Type == "foreach") {
            commandPanelName = "foreach";
        }
        else if (currentCommand.Type == "if" ||
                currentCommand.Type == "else if" ||
                currentCommand.Type == "while" ||
                (currentCommand.Type == "command" && currentCommand.Expressions != null && currentCommand.Expressions.length > 0 && currentCommand.Expressions[0].Name == "return")) {
            if (currentExpression != null)
                commandPanelName = "conditions";
            else
                commandPanelName = "";
        }
        else if (currentCommand.Type == "loop" && currentExpression != null) {
            commandPanelName = "loop";
        }
        else if (currentCommand.Type == "comment") {
            commandPanelName = "comment";
        }
        else if (currentCommand.Type == "empty") {
            if (currentFunction.Type == "function") {
                commandPanelName = "general";
            }
            else if (currentFunction.Type == "class") {
                commandPanelName = "general-class";
            }
        }
    }
    else if (functionSelected) {
        commandPanelName = "function";
    }
    else if (currentFunctionParameter != null) {
        commandPanelName = "function-parameter";
    }

    if (commandPanelName != null && commandPanelName.length > 0)
        showCommandPanel(commandPanelName, event);
}

function unselectCommand(event) {
    currentExpression = null;
    currentCommand = null;
    functionSelected = false;
    currentFunctionParameter = null;
    hideCommandPanel(event);
    displayCodeContainer();
}

function generateEmptyCommand()
{
    return {
        "Id": generateGuid(),
        "Type": "empty",
        "AllowAddBefore": false,
        "AllowAddAfter": false,
        "Expressions": []
    }
}

function renderExpression(command, expression, html) {
    var divExtComment = "";
    if (command.Disabled)
        divExtComment = "commented-out";
    if (expression.Type == "namespace") {
        html += "<div class=\"reserved-word " + divExtComment + "\">" + expression.Name + "</div>";
        html += "<div class=\"operator " + divExtComment + "\">&rarr;</div>";
    }
    else if (expression.Type == "function") {
        html += "<div class=\"reserved-word " + divExtComment + "\">" + expression.Name + "</div>";
    }
    else if (expression.Type == "reserved-word") {
        var styleAdd = "";
        if (expression.Name == "return")
            styleAdd = "style=\"padding-right:8px;\"";
        html += "<div class=\"reserved-word " + divExtComment + "\" " + styleAdd + ">" + expression.Name + "</div>";
    }
    else if (expression.Type == "method") {
        html += "<div class=\"reserved-word " + divExtComment + "\">" + expression.Name + "</div>";
    }
    else if (expression.Type == "value") {
        var selectedExpressionClass = "";
        if (currentExpression != null && currentExpression.Id == expression.Id)
            selectedExpressionClass = "selected-expression";
        if (expression.DataType == "number")
            html += "<div class=\"var-number " + selectedExpressionClass + " " + divExtComment + "\" onclick=\"selectExpression('" + command.Id + "', '" + expression.Id + "', event)\">" + expression.Name + "</div>";
        else if (expression.DataType == "string")
            html += "<div class=\"var-string " + selectedExpressionClass + " " + divExtComment + "\" onclick=\"selectExpression('" + command.Id + "', '" + expression.Id + "', event)\">\"" + expression.Name + "\"</div>";
        else if (expression.DataType == "boolean")
            html += "<div class=\"reserved-word " + selectedExpressionClass + " " + divExtComment + "\" onclick=\"selectExpression('" + command.Id + "', '" + expression.Id + "', event)\">" + expression.Name + "</div>";
        else if (expression.DataType == "object")
            html += "<div class=\"var-object " + selectedExpressionClass + " " + divExtComment + "\" onclick=\"selectExpression('" + command.Id + "', '" + expression.Id + "', event)\">" + expression.Name + "</div>";
    }
    else if (expression.Type == "variable-declaration") {
        if (currentFunction.Type == "function") {
            html += "<div class=\"reserved-word " + divExtComment + "\" style=\"margin-right:8px;\">var</div>";
        }
        else if (currentFunction.Type == "class") {
            html += "<div class=\"reserved-word " + divExtComment + "\" style=\"margin-right:8px;\">property</div>";
        }
        html += "<div class=\"variable-name " + divExtComment + "\">" + expression.Name + "</div>";
        html += "<div class=\"operator " + divExtComment + "\">=</div>";
    }
    else if (expression.Type == "variable-assign") {
        html += "<div class=\"variable-name " + divExtComment + "\">" + expression.Name + "</div>";
        if (command.Type == "for")
        html += "<div class=\"operator " + divExtComment + "\">&lt;=</div>";
        else
            html += "<div class=\"operator " + divExtComment + "\">=</div>";
    }
    else if (expression.Type == "variable-assign-namespace") {
        if (command.ClassVariable != null && command.ClassVariable.length > 0) {
            html += "<div class=\"variable-name " + divExtComment + "\">" + command.ClassVariable + "</div>";
            html += "<div class=\"operator " + divExtComment + "\">&rarr;</div>";    
        }
        html += "<div class=\"variable-name " + divExtComment + "\">" + expression.Name + "</div>";
        html += "<div class=\"operator " + divExtComment + "\">&rarr;</div>";
    }
    else if (expression.Type == "variable") {
        var selectedExpressionClass = "";
        if (currentExpression != null && currentExpression.Id == expression.Id)
            selectedExpressionClass = "selected-expression";
        var displayName = "";
        if (expression.ClassVariable != null && expression.ClassVariable.length > 0) {
            displayName += expression.ClassVariable + " &rarr; ";
        }
        displayName += expression.Name;
        html += "<div class=\"variable-name " + selectedExpressionClass + " " + divExtComment + "\" onclick=\"selectExpression('" + command.Id + "', '" + expression.Id + "', event)\">" + displayName + "</div>";
    }
    else if (expression.Type == "operator") {
        html += "<div class=\"operator " + divExtComment + "\">" + expression.Name + "</div>";
    }

    if (expression.Expressions != null && expression.Expressions.length > 0) {
        if (expression.Type != "variable-declaration" && expression.Type != "variable-assign")
            html += "<div class=\"operator-open " + divExtComment + "\">(</div>";
        for (var e = 0; e < expression.Expressions.length; e++) {
            html = renderExpression(command, expression.Expressions[e], html);
        }
        if (expression.Type != "variable-declaration" && expression.Type != "variable-assign")
            html += "<div class=\"operator-close " + divExtComment + "\">)</div>";
        if (command.Type == "for")
            html += "<div class=\"operator " + divExtComment + "\">;</div>";
    }
    return html;
}

function displayCodeContainer() {
    var container = document.getElementById("code-container");

    var containerHTML = "<div class=\"wrapper-top\">";
    containerHTML += "<div class=\"reserved-word\">" + currentFunction.Type + "</div>";
    containerHTML += "<div class=\"function-name\" onclick=\"selectFunctionHeader(event)\">" + currentFunction.Name + "</div>";
    containerHTML += "<div class=\"operator-open\">(</div>";
    for (var i = 0; i < currentFunction.Parameters.length; i++) {
        var functionParameterSelected = "";
        if (currentFunctionParameter != null && currentFunctionParameter.Id == currentFunction.Parameters[i].Id)
            functionParameterSelected = "selected-expression"
        containerHTML += "<div class=\"variable-name " + functionParameterSelected + "\" onclick=\"selectFunctionParameter('" + currentFunction.Parameters[i].Id + "', event)\">" + currentFunction.Parameters[i].Name + "</div>";
        if (i < currentFunction.Parameters.length - 1)
            containerHTML += "<div class=\"operator\">,</div>";
    }
    containerHTML += "<div class=\"operator-open\">)</div>";
    containerHTML += "</div>";
    containerHTML += "<div class=\"function-wrapper\">";
    for (var i = 0; i < commands.length; i++) {
        var divExt = "";
        var divExt2 = "";
        var divExtComment = "";
        var commentPrefix = "";
        var isSelected = false;
        if (currentCommand != null && (commands[i].Id == currentCommand.Id || commands[i].SourceId == currentCommand.Id)) {
            divExt = "selected";
            isSelected = true;
        }
        if (selectionMode && selectedCommands.includes(commands[i])) {
            divExt2 = "selected-marked";
        }
        if (commands[i].Disabled) {
            divExtComment = "commented-out";
            commentPrefix = "<div class=\"commented-out\" style=\"margin-right:8px;\">//</div>";
        }

        if (isSelected && commands[i].AllowAddBefore) {
            containerHTML += "<div class=\"btn-control-container\">"
            containerHTML += "<div class=\"btn-round\" onclick=\"addCommandBefore('" + commands[i].Id + "', event)\">+</div>"
            containerHTML += "</div>"
        }

        var containerCommandHTML = "";
        var commandControls = "onclick=\"selectCommand('" + commands[i].Id + "', event)\" "
                            + "ontouchstart=\"commandTouchStart('" + commands[i].Id + "', event)\" "
                            + "ontouchmove=\"commandTouchMove('" + commands[i].Id + "', event)\" "
                            + "ontouchend=\"commandTouchEnd('" + commands[i].Id + "', event)\" "
                            + "onmousedown=\"commandMouseDown('" + commands[i].Id + "', event)\" "
                            + "onmouseup=\"commandMouseUp('" + commands[i].Id + "', event)\" ";
        if (commands[i].Type == "if" || commands[i].Type == "else if") {
            containerCommandHTML = "<div class=\"wrapper-top " + divExt2 + "\" " + commandControls + ">";
            containerCommandHTML += "<div class=\"reserved-word " + divExtComment + "\">" + commentPrefix + commands[i].Type + "</div>";
            containerCommandHTML += "<div class=\"operator-open\">(</div>";
            for (var j = 0; j < commands[i].Expressions.length; j++) {
                containerCommandHTML = renderExpression(commands[i], commands[i].Expressions[j], containerCommandHTML);
            }
            containerCommandHTML += "<div class=\"operator-close\">)</div>";
            containerCommandHTML += "</div>";
            containerCommandHTML += "<div class=\"wrapper-content if-wrapper\">";

            if (isSelected)
                containerCommandHTML += "<div class=\"btn btn-round btn-delete-command\" onclick=\"deleteCommand('" + commands[i].Id + "', event)\"></div>";

        }
        else if (commands[i].Type == "else") {
            containerCommandHTML = "<div class=\"wrapper-top " + divExt2 + "\" " + commandControls + ">";
            containerCommandHTML += "<div class=\"reserved-word " + divExtComment + "\">" + commentPrefix + commands[i].Type + "</div>";
            containerCommandHTML += "</div>";
            containerCommandHTML += "<div class=\"wrapper-content if-wrapper\">";

            if (isSelected)
                containerCommandHTML += "<div class=\"btn btn-round btn-delete-command\" onclick=\"deleteCommand('" + commands[i].Id + "', event)\"></div>";

        }
        else if (commands[i].Type == "for") {
            containerCommandHTML = "<div class=\"wrapper-top " + divExt2 + "\" " + commandControls + ">";
            containerCommandHTML += "<div class=\"reserved-word " + divExtComment + "\" style=\"margin-right:8px;\">" + commentPrefix + commands[i].Type + "</div>";

            for (var j = 0; j < commands[i].Expressions.length; j++) {
                containerCommandHTML = renderExpression(commands[i], commands[i].Expressions[j], containerCommandHTML);
            }
            containerCommandHTML += "</div>";
            containerCommandHTML += "<div class=\"wrapper-content for-wrapper\">";

            if (isSelected)
                containerCommandHTML += "<div class=\"btn btn-round btn-delete-command\" onclick=\"deleteCommand('" + commands[i].Id + "', event)\"></div>";

        }
        else if (commands[i].Type == "foreach") {
            containerCommandHTML = "<div class=\"wrapper-top " + divExt2 + "\" " + commandControls + ">";
            containerCommandHTML += "<div class=\"reserved-word " + divExtComment + "\">" + commentPrefix + commands[i].Type + "</div>";
            containerCommandHTML += "<div class=\"operator-open\">(</div>";
            containerCommandHTML += "<div class=\"reserved-word " + divExtComment + "\" style=\"margin-right:8px;\">var</div>";
            containerCommandHTML += "<div class=\"variable-name\">" + commands[i].Expressions[0].Name + "</div>";
            containerCommandHTML += "<div class=\"reserved-word " + divExtComment + "\" style=\"margin-left:8px;margin-right:8px;\">in</div>";
            if (commands[i].Expressions[1].ClassVariable != null && commands[i].Expressions[1].ClassVariable.length > 0)
                containerCommandHTML += "<div class=\"variable-name\">" + commands[i].Expressions[1].ClassVariable + " &rarr; " + commands[i].Expressions[1].Name + "</div>";
            else
                containerCommandHTML += "<div class=\"variable-name\">" + commands[i].Expressions[1].Name + "</div>";
            containerCommandHTML += "<div class=\"operator-close\">)</div>";
            containerCommandHTML += "</div>";
            containerCommandHTML += "<div class=\"wrapper-content for-wrapper\">";

            if (isSelected)
                containerCommandHTML += "<div class=\"btn btn-round btn-delete-command\" onclick=\"deleteCommand('" + commands[i].Id + "', event)\"></div>";

        }
        else if (commands[i].Type == "while") {
            containerCommandHTML = "<div class=\"wrapper-top " + divExt2 + "\" " + commandControls + ">";
            containerCommandHTML += "<div class=\"reserved-word " + divExtComment + "\">" + commentPrefix + commands[i].Type + "</div>";
            containerCommandHTML += "<div class=\"operator-open\">(</div>";
            for (var j = 0; j < commands[i].Expressions.length; j++) {
                containerCommandHTML = renderExpression(commands[i], commands[i].Expressions[j], containerCommandHTML);
            }
            containerCommandHTML += "<div class=\"operator-close\">)</div>";
            containerCommandHTML += "</div>";
            containerCommandHTML += "<div class=\"wrapper-content while-wrapper\">";

            if (isSelected)
                containerCommandHTML += "<div class=\"btn btn-round btn-delete-command\" onclick=\"deleteCommand('" + commands[i].Id + "', event)\"></div>";

        }
        else if (commands[i].Type == "loop") {
            containerCommandHTML = "<div class=\"wrapper-top " + divExt2 + "\" " + commandControls + ">";
            containerCommandHTML += "<div class=\"reserved-word " + divExtComment + "\">" + commentPrefix + commands[i].Type + "</div>";
            containerCommandHTML += "<div class=\"operator-open\">(</div>";
            for (var j = 0; j < commands[i].Expressions.length; j++) {
                containerCommandHTML = renderExpression(commands[i], commands[i].Expressions[j], containerCommandHTML);
            }
            containerCommandHTML += "<div class=\"operator-close\">)</div>";
            containerCommandHTML += "</div>";
            containerCommandHTML += "<div class=\"wrapper-content loop-wrapper\">";

            if (isSelected)
                containerCommandHTML += "<div class=\"btn btn-round btn-delete-command\" onclick=\"deleteCommand('" + commands[i].Id + "', event)\"></div>";

        }
        else if (commands[i].Type == "end") {
            containerCommandHTML += "</div>";
        }
        else if (commands[i].Type == "empty") {
            containerCommandHTML = "<div id=\"cmd-" + commands[i].Id + "\" class=\"command " + divExt + divExt2 + "\" onclick=\"selectCommand('" + commands[i].Id + "', event)\">";
            containerCommandHTML += "<div class=\"comment\">Do nothing</div>";
            containerCommandHTML += "</div>";
        }
        else if (commands[i].Type == "comment") {
            containerCommandHTML = "<div class=\"command " + divExt + divExt2 + "\" onclick=\"selectCommand('" + commands[i].Id + "', event)\">";
            containerCommandHTML += "<div class=\"comment\">// " + commands[i].Comment + "</div>";

            if (isSelected)
                containerCommandHTML += "<div class=\"btn btn-round btn-delete-command\" onclick=\"deleteCommand('" + commands[i].Id + "', event)\"></div>";

            containerCommandHTML += "</div>";
        }
        else {
            containerCommandHTML = "<div id=\"cmd-" + commands[i].Id + "\" class=\"command " + divExt + divExt2 + "\" " + commandControls + ">" + commentPrefix;
            for (var j = 0; j < commands[i].Expressions.length; j++) {
                containerCommandHTML = renderExpression(commands[i], commands[i].Expressions[j], containerCommandHTML);
            }

            if (isSelected)
                containerCommandHTML += "<div class=\"btn btn-round btn-delete-command\" onclick=\"deleteCommand('" + commands[i].Id + "', event)\"></div>";

            containerCommandHTML += "</div>";
        }

        containerHTML += containerCommandHTML;

        if (isSelected && commands[i].AllowAddAfter) {
            containerHTML += "<div class=\"btn-control-container\">";
            if (commands[i].Type == "end" && i < commands.length - 1 && (commands[i + 1].Type == "else" || commands[i + 1].Type == "else if")) {
                containerHTML += "<div style=\"width:12px\">&nbsp;</div>";
            }
            else {
                containerHTML += "<div class=\"btn-round\" onclick=\"addCommandAfter('" + commands[i].Id + "', event)\">+</div>";
            }
            if (commands[i].Type == "end" && commands[i].SourceId != null) {
                var sourceCommand = commands.find((item) => item.Id == commands[i].SourceId);
                if (sourceCommand.Type == "if" || sourceCommand.Type == "else" || sourceCommand.Type == "else if") {
                    containerHTML += "<div class=\"btn-rect\" style=\"left:10px;\" onclick=\"addElse('" + commands[i].Id + "', event)\">+else</div>";
                    containerHTML += "<div class=\"btn-rect\" style=\"left:10px;\" onclick=\"addElseIf('" + commands[i].Id + "', event)\">+else if</div>";
                }
                else if (sourceCommand.Type == "for" || sourceCommand.Type == "foreach") {
                    containerHTML += "<div class=\"btn-rect\" style=\"left:10px;\" onclick=\"showModalInput('New name for variable:', '" + sourceCommand.Expressions[0].Name + "', 'text', renameVariable, event)\">rename variable</div>";
                }
            }
            if (commands[i].AllowMakeVariable) {
                containerHTML += "<div class=\"btn-rect\" style=\"left:10px;\" onclick=\"showVariablesList('" + commands[i].VariableType + "', convertToVariable, event)\">&rarr; to var</div>";
                containerHTML += "<div class=\"btn-rect\" style=\"left:10px;\" onclick=\"makeVariable(event)\">+ new var</div>";
            }
            else if (commands[i].Expressions != null && (commands[i].Expressions.find((item) => item.Type == "variable-assign") != null || commands[i].Expressions.find((item) => item.Type == "variable-assign-namespace") != null || commands[i].Expressions.find((item) => item.Type == "variable-declaration") != null)) {
                var expression = commands[i].Expressions.find((item) => item.Type == "variable-assign");
                if (expression == null)
                    expression = commands[i].Expressions.find((item) => item.Type == "variable-assign-namespace");
                if (expression == null)
                    expression = commands[i].Expressions.find((item) => item.Type == "variable-declaration");
                if (currentFunction.Type == "function" && expression.Type != "variable-assign-namespace") {
                    containerHTML += "<div class=\"btn-rect\" style=\"left:10px;\" onclick=\"showModalInput('New name for variable:', '" + expression.Name + "', 'text', renameVariable, event)\">rename variable</div>";
                }
                else if (currentFunction.Type == "class") {
                    containerHTML += "<div class=\"btn-rect\" style=\"left:10px;\" onclick=\"showModalInput('New name for property:', '" + expression.Name + "', 'text', renameVariable, event)\">rename property</div>";
                }
            }
            containerHTML += "</div>";
        }
    }
    containerHTML += "</div>";
    container.innerHTML = containerHTML;
    putObjects("projects", projects);

    desktopMode = (window.innerWidth > window.innerHeight) && options.enableSplitView;
    if (desktopMode && liveexecution)
        updateOutput();
}

function createIf(event) {
    var expressions = [{ "Id": generateGuid(), "Type": "value", "DataType": "boolean", "Name": "true" }];
    currentCommand.Type = "if";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = false;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    currentExpression = expressions[0];

    var endCommand = {
        "Id": generateGuid(),
        "SourceId": currentCommand.Id,
        "AllowAddAfter": true,
        "Type": "end",
        "Expressions": []
    };

    var index = commands.findIndex((item) => item.Id == currentCommand.Id);
    commands.splice(index + 1, 0, generateEmptyCommand());
    commands.splice(index + 2, 0, endCommand);
    selectCommand(commands[index].Id, event)

    updateCommandPanel(event);
    displayCodeContainer();
}

function addElse(commandId, event) {
    addCommandAfter(commandId, event);
    currentCommand.Type = "else";
    currentCommand.Expressions = [];
    currentCommand.AllowAddBefore = false;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";

    var endCommand = {
        "Id": generateGuid(),
        "SourceId": currentCommand.Id,
        "AllowAddAfter": true,
        "Type": "end",
        "Expressions": []
    };

    var index = commands.findIndex((item) => item.Id == currentCommand.Id);
    commands.splice(index + 1, 0, generateEmptyCommand());
    commands.splice(index + 2, 0, endCommand);
    selectCommand(commands[index].Id, event)

    updateCommandPanel(event);
    displayCodeContainer();
}

function addElseIf(commandId, event) {
    addCommandAfter(commandId, event);
    var expressions = [{ "Id": generateGuid(), "Type": "value", "DataType": "boolean", "Name": "true" }];
    currentCommand.Type = "else if";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = false;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    currentExpression = expressions[0];

    var endCommand = {
        "Id": generateGuid(),
        "SourceId": currentCommand.Id,
        "AllowAddAfter": true,
        "Type": "end",
        "Expressions": []
    };

    var index = commands.findIndex((item) => item.Id == currentCommand.Id);
    commands.splice(index + 1, 0, generateEmptyCommand());
    commands.splice(index + 2, 0, endCommand);
    selectCommand(commands[index].Id, event)

    updateCommandPanel(event);
    displayCodeContainer();
}

function createFor(event) {
    var variableName = getFreeVariableName("i");
    currentCommand.Type = "for";
    //currentCommand.Variable = getFreeVariableName("i");
    currentCommand.Expressions = [
        { "Id": generateGuid(), "Type": "variable-declaration", "Name": variableName, "Expressions": [
            { "Id": generateGuid(), "Type": "value", "DataType": "number", "Name": "1" }
        ]},
        { "Id": generateGuid(), "Type": "variable-assign", "Name": variableName, "Expressions": [
            { "Id": generateGuid(), "Type": "value", "DataType": "number", "Name": "10" }
        ]},
    ];
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = false;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "number";

    var endCommand = {
        "Id": generateGuid(),
        "SourceId": currentCommand.Id,
        "AllowAddAfter": true,
        "Type": "end",
        "Expressions": []
    };

    var index = commands.findIndex((item) => item.Id == currentCommand.Id);
    commands.splice(index + 1, 0, generateEmptyCommand());
    commands.splice(index + 2, 0, endCommand);
    selectCommand(commands[index].Id, event)

    updateCommandPanel(event);
    displayCodeContainer();
}

function createForEach(event) {
    var variableName = getFreeVariableName("item");
    currentCommand.Type = "foreach";
    currentCommand.Expressions = [
        { "Id": generateGuid(), "Type": "variable-declaration", "Name": variableName, "Expressions": [
            { "Id": generateGuid(), "Type": "value", "DataType": "collection", "Name": "" }
        ]},
        { "Id": generateGuid(), "Type": "variable", "Name": "collection", "DataType": "collection" }
    ];
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = false;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "string";

    var endCommand = {
        "Id": generateGuid(),
        "SourceId": currentCommand.Id,
        "AllowAddAfter": true,
        "Type": "end",
        "Expressions": []
    };

    var index = commands.findIndex((item) => item.Id == currentCommand.Id);
    commands.splice(index + 1, 0, generateEmptyCommand());
    commands.splice(index + 2, 0, endCommand);
    selectCommand(commands[index].Id, event)

    updateCommandPanel(event);
    displayCodeContainer();
}

function createWhile(event) {
    var expressions = [{ "Id": generateGuid(), "Type": "value", "DataType": "boolean", "Name": "true" }];
    currentCommand.Type = "while";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = false;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    currentExpression = expressions[0];

    var endCommand = {
        "Id": generateGuid(),
        "SourceId": currentCommand.Id,
        "AllowAddAfter": true,
        "Type": "end",
        "Expressions": []
    };

    var index = commands.findIndex((item) => item.Id == currentCommand.Id);
    commands.splice(index + 1, 0, generateEmptyCommand());
    commands.splice(index + 2, 0, endCommand);
    selectCommand(commands[index].Id, event)

    updateCommandPanel(event);
    displayCodeContainer();
}

function createLoop(event) {
    var expressions = [{ "Id": generateGuid(), "Type": "value", "DataType": "number", "Name": "200" }];
    currentCommand.Type = "loop";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = false;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    currentExpression = expressions[0];

    var endCommand = {
        "Id": generateGuid(),
        "SourceId": currentCommand.Id,
        "AllowAddAfter": true,
        "Type": "end",
        "Expressions": []
    };

    var index = commands.findIndex((item) => item.Id == currentCommand.Id);
    commands.splice(index + 1, 0, generateEmptyCommand());
    commands.splice(index + 2, 0, endCommand);
    selectCommand(commands[index].Id, event)

    updateCommandPanel(event);
    displayCodeContainer();
}

function createBreak(event) {
    var expressions = [{ "Id": generateGuid(), "Type": "reserved-word", "Name": "break" }];
    currentCommand.Type = "command";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    currentExpression = null;
    updateCommandPanel(event);
    displayCodeContainer();
}

function createReturn(dataType, name, event) {
    var expressions = [
        { "Id": generateGuid(), "Type": "reserved-word", "Name": "return" },
       { "Id": generateGuid(), "Type": "value", "DataType": dataType, "Name": name }
    ];
    currentCommand.Type = "command";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    currentExpression = expressions[1];

    updateCommandPanel(event);
    displayCodeContainer();
}

function createFunction(id, event) {
    closeFunctionsList();
    var func = functions.find((item) => item.Id == id);
    var returnCommand = func.Commands.find((item) => item.Expressions != null && item.Expressions.length > 0 && item.Expressions[0].Name == "return");
    var variableType = "";
    if (returnCommand != null)
        variableType = returnCommand.Expressions[1].DataType;
    var expressions = [
        {
            "Id": generateGuid(),
            "Type": "function",
            "Name": func.Name,
            "Expressions": []
        }
    ];
    for (var i = 0; i < func.Parameters.length; i++) {
        if (func.Parameters[i].DataType == "string")
            expressions[0].Expressions.push({ "Id": generateGuid(), "Type": "value", "DataType": func.Parameters[i].DataType, "Name": "text" });
        else if (func.Parameters[i].DataType == "number")
            expressions[0].Expressions.push({ "Id": generateGuid(), "Type": "value", "DataType": func.Parameters[i].DataType, "Name": "0" });
        else if (func.Parameters[i].DataType == "boolean")
            expressions[0].Expressions.push({ "Id": generateGuid(), "Type": "value", "DataType": func.Parameters[i].DataType, "Name": "true" });
        if (i < func.Parameters.length - 1)
            expressions[0].Expressions.push({ "Id": generateGuid(), "Type": "operator", "Name": "," });
    }
    currentCommand.Type = "command";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    currentCommand.AllowMakeVariable = true;
    currentCommand.VariableType = variableType;
    currentExpression = null;
    updateCommandPanel(event);
    displayCodeContainer();
}

function createClass(id, event) {
    closeClassList();
    var func = functions.find((item) => item.Id == id);
    var expressions = [
        {
            "Id": generateGuid(),
            "Type": "variable-declaration",
            "Name": getFreeVariableName(func.Name),
            "Expressions": [
                {
                    "Id": generateGuid(),
                    "Type": "namespace",
                    "Name": func.Name
                },
                {
                    "Id": generateGuid(),
                    "Type": "method",
                    "Name": "create"
                }
            ]
        }
    ];
    currentCommand.Type = "command";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = func.Name;
    currentExpression = null;
    updateCommandPanel(event);
    displayCodeContainer();
}

function createClassList(id, event) {
    closeClassList();
    var func = functions.find((item) => item.Id == id);
    var newVariableName = func.Name + "s";
    if (func.Name.endsWith("s"))
        newVariableName = func.Name + "es";
    var expressions = [
        {
            "Id": generateGuid(),
            "Type": "variable-declaration",
            "Name": getFreeVariableName(newVariableName),
            "Expressions": [
                {
                    "Id": generateGuid(),
                    "Type": "namespace",
                    "Name": func.Name + " list"
                },
                {
                    "Id": generateGuid(),
                    "Type": "method",
                    "Name": "create"
                }
            ]
        }
    ];
    currentCommand.Type = "command";
    currentCommand.Expressions = expressions;
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = func.Name + " list";
    currentExpression = null;
    updateCommandPanel(event);
    displayCodeContainer();
}

function createCommentLine(event) {
    currentCommand.Type = "comment";
    currentCommand.Expressions = [];
    currentCommand.Comment = "Comment line";
    currentCommand.AllowAddBefore = true;
    currentCommand.AllowAddAfter = true;
    currentCommand.AllowMakeVariable = false;
    currentCommand.VariableType = "";
    updateCommandPanel(event);
    displayCodeContainer();
}

function addCommandBefore(commandId, event) {
    var index = commands.findIndex((item) => item.Id == commandId);
    var command = null;
    if (index == 0) {
        commands.unshift(generateEmptyCommand());
        command = commands[0];
    }
    else {
        commands.splice(index, 0, generateEmptyCommand());
        command = commands[index];
    }
    selectCommand(command.Id, event);
}

function addCommandAfter(commandId, event) {
    var index = commands.findIndex((item) => item.Id == commandId);
    commands.splice(index + 1, 0, generateEmptyCommand());
    selectCommand(commands[index + 1].Id, event);
}

function deleteCommand(commandId, event) {
    var startIndex = commands.findIndex((item) => item.Id == commandId);
    commands.splice(startIndex, 1);
    var endIndex = commands.findIndex((item) => item.SourceId == commandId);
    if (endIndex >= 0) {
        for (var i = endIndex; i >= startIndex; i--) {
            commands.splice(i, 1);
        }
    }
    if (commands.length == 0)
        commands.push(generateEmptyCommand());
    
    unselectCommand(event);
    event.stopImmediatePropagation();
}

function switchOutput(event) {
    var outputContainer = document.getElementById("output-container");
    if (outputContainer.style.display == "none") {
        unselectCommand(event);
        outputContainer.style.display = "";
        document.getElementById("nav-bar-container-1").style.visibility = "hidden";
        document.getElementById("nav-bar-container-2").style.visibility = "hidden";
        updateOutput();
        document.getElementById("btn-play").className = "nav-bar-button btn-stop";
    }
    else {
        document.getElementById("nav-bar-container-1").style.visibility = "visible";
        document.getElementById("nav-bar-container-2").style.visibility = "visible";
        document.getElementById("output-window").contentWindow.document.open();
        document.getElementById("output-window").contentWindow.document.write("<!DOCTYPE html><html><body></body></html>");
        document.getElementById("output-window").contentWindow.document.close();
        outputContainer.style.display = "none";
        document.getElementById("btn-play").className = "nav-bar-button btn-play";
    }
}

function updateOutput() {
    document.getElementById("output-window").contentWindow.document.open();
    document.getElementById("output-window").contentWindow.document.write("<!DOCTYPE html><html><body></body></html>");
    document.getElementById("output-window").contentWindow.document.close();

    var outputHTML = generateOutputHTML();
    document.getElementById("output-window").contentWindow.document.open();
    document.getElementById("output-window").contentWindow.document.write(outputHTML);
    document.getElementById("output-window").contentWindow.document.close();
    if (desktopMode)
        document.getElementById("output-container").style.display = '';
}

function switchMenu(menuName) {
    var menu = document.getElementById(menuName);
    if (menu.style.left == "-100%")
        menu.style.left = "0";
    else
        menu.style.left = "-100%";
}

function downloadApp() {
    var outputHTML = generateOutputHTML();
    var element1 = document.createElement('a');
    element1.setAttribute('href', 'data:text/plain;,' + encodeURIComponent(outputHTML));
    element1.setAttribute('download', currentProject.Name.replaceAll2(" ", "") + ".html");
  
    element1.style.display = 'none';
    document.body.appendChild(element1);
  
    element1.click();
  
    document.body.removeChild(element1);

    // ---------------------------------------------------------------------------
    var desktopContent = "[Desktop Entry]\n"
                       + "Name=" + currentProject.Name + "\n"
                       + "Type=Application\n"
                       + "Icon=/usr/share/icons/suru/apps/scalable/preferences-desktop-apps-media-symbolic.svg\n"
                       + "Exec=webapp-container file:///home/phablet/.local/share/touchide.daniel/Downloads/" + currentProject.Name.replaceAll2(" ", "") + ".html\n"
                       + "Terminal=false\n"
                       + "X-Ubuntu-Touch=true";
    var element2 = document.createElement('a');
    element2.setAttribute('href', 'data:text/plain;,' + encodeURIComponent(desktopContent));
    element2.setAttribute('download', currentProject.Name.replaceAll2(" ", "") + ".desktop");
  
    element2.style.display = 'none';
    document.body.appendChild(element2);
  
    element2.click();
  
    document.body.removeChild(element2);
}

function backToProjects() {
    if (syncenabled && currentProject.Sync) {
        var username = localStorage.getItem("username");
        var appid = localStorage.getItem("appid");
        var req = new XMLHttpRequest();
        req.onreadystatechange = function() {
            if (this.readyState == 4) {
            // currentVariableName + ".status = " + xhttp + ".status;\n";
            // currentVariableName + ".statusText = " + xhttp + ".statusText;\n";
            // currentVariableName + ".responseText = " + xhttp + ".responseText;\n";
            }
        }
        req.onerror = function(e) {
            showModalDialog("Warning: This project could not be pushed to the cloud. Sync has been disabled for this project.");
            currentProject.Sync = false;
            var currentProjectIdx = projects.findIndex((item) => item.Id == currentProject.Id);
            if (currentProjectIdx > -1)
                projects[currentProjectIdx] = currentProject;
            putObjects("projects", projects);
            getProjects();
            currentProject = projects.find(item => item.Id == currentProject.Id);
        }
        req.onabort = function(e) {
            console.log('ABORT');
        }
        req.open("POST", "https://touchide.net?u=" + encodeURIComponent(username) + "&a=" + encodeURIComponent(appid), true);
        req.send(JSON.stringify(currentProject));
    }
    document.getElementById("output-window").contentWindow.document.open();
    document.getElementById("output-window").contentWindow.document.write("<!DOCTYPE html><html><body></body></html>");
    document.getElementById("output-window").contentWindow.document.close();
    switchMenu('ide-menu');
    document.getElementById("projects").style.display = "";
}

function iterateExpressionForSelect(id, expression) {
    if (expression.Id == id) {
        currentExpression = expression;
    }
    else if (expression.Expressions != null && expression.Expressions.length > 0) {
        for (var e = 0; e < expression.Expressions.length; e++) {
            currentParentExpression = expression;
            iterateExpressionForSelect(id, expression.Expressions[e]);
            if (currentExpression != null)
                break;
        }
    }
}

function selectExpression(commandId, expressionId, event) {
    if (!selectionMode) {
        currentExpression = null;

        if (currentCommand == null || currentCommand.Id != commandId)
            selectCommand(commandId, event);
        
        if (currentCommand.Expressions != null && currentCommand.Expressions.length > 0) {
            for (var i = 0; i < currentCommand.Expressions.length; i++) {
                currentParentExpression = currentCommand.Expressions[i];
                iterateExpressionForSelect(expressionId, currentCommand.Expressions[i]);
                if (currentExpression != null)
                    break;
            }
        }

        updateCommandPanel(event);
        displayCodeContainer();
        event.stopImmediatePropagation();
    }
}

function switchSelectionMode(event) {
    unselectCommand(event);
    selectionMode = !selectionMode;
    if (selectionMode) {
        selectedCommands = [];
        copiedCommands = [];
        document.getElementById("ide-default").style.display = "none";
        document.getElementById("ide-selection-mode").style.display = "";
    }
    else {
        document.getElementById("ide-default").style.display = "";
        document.getElementById("ide-selection-mode").style.display = "none";
    }
    displayCodeContainer();
}

function cutCommands(event) {
    copiedCommands = [];
    for (var i = 0; i < commands.length; i++) {
        var index = selectedCommands.findIndex((item) => item.Id == commands[i].Id);
        if (index >= 0 && commands[i].Type != "empty") {
            copiedCommands.push(commands[i]);
        }
    }
    for (var i = commands.length - 1; i >= 0; i--) {
        var index = selectedCommands.findIndex((item) => item.Id == commands[i].Id);
        if (index >= 0 && commands[i].Type != "empty") {
            deleteCommand(commands[i].Id, event);
        }
    }
    selectedCommands = [];
    displayCodeContainer();
}

function copyCommands(event) {
    copiedCommands = [];
    for (var i = 0; i < commands.length; i++) {
        var index = selectedCommands.findIndex((item) => item.Id == commands[i].Id);
        if (index >= 0 && commands[i].Type != "empty") {
            copiedCommands.push(commands[i]);
        }
    }
    selectedCommands = [];
    displayCodeContainer();
}

function pasteCommands(event) {
    if (selectedCommands.length > 0) {
        var lastCommandId = selectedCommands[selectedCommands.length - 1].Id;
        for (var i = 0; i < copiedCommands.length; i++) {
            var index = commands.findIndex((item) => item.Id == lastCommandId);
            var newCommandString = JSON.stringify(copiedCommands[i]);
            var newCommand = JSON.parse(newCommandString);
            newCommand.Id = generateGuid();
            commands.splice(index + 1, 0, newCommand);
            lastCommandId = newCommand.Id;
        }
    }
    selectedCommands = [];
    copiedCommands = [];
    displayCodeContainer();
}

function switchAboutWindow() {
    var elem = document.getElementById("about");
    if (elem.style.display == "none")
        elem.style.display = "";
    else
        elem.style.display = "none";
}

function switchCredentialsWindow() {
    document.getElementById("username").value = localStorage.getItem("username");
    document.getElementById("appid").value = localStorage.getItem("appid");
    var elem = document.getElementById("credentials");
    if (elem.style.display == "none")
        elem.style.display = "";
    else
        elem.style.display = "none";
}

function saveCredentials() {
    localStorage.setItem("username", document.getElementById("username").value);
    localStorage.setItem("appid", document.getElementById("appid").value);
    setSync();
    var elem = document.getElementById("credentials");
    elem.style.display = "none";
}

function switchOptionsWindow() {
    document.getElementById("smallerfont").checked = options.smallerFont;
    document.getElementById("enablesplitview").checked = options.enableSplitView;
    document.getElementById("wraplines").checked = options.wrapLines;
    if (options.apikey != null)
        document.getElementById("apikey").value = options.apikey;
    if (options.author != null)
        document.getElementById("author").value = options.author;
    if (options.email != null)
        document.getElementById("email").value = options.email;

    var elem = document.getElementById("options");
    if (elem.style.display == "none")
        elem.style.display = "";
    else
        elem.style.display = "none";
}

function saveOptions() {
    options.smallerFont = document.getElementById("smallerfont").checked;
    options.enableSplitView = document.getElementById("enablesplitview").checked;
    options.wrapLines = document.getElementById("wraplines").checked;
    options.apikey = document.getElementById("apikey").value;
    options.author = document.getElementById("author").value;
    options.email = document.getElementById("email").value;
    localStorage.setItem("options", JSON.stringify(options));
    setOptions();
    switchOptionsWindow();
}

function switchProjectOptionsWindow() {
    if (currentProject.Options == null)
        currentProject.Options = {};
    if (currentProject.Options.UbuntuTheme == null)
        currentProject.Options.UbuntuTheme = false;
    if (currentProject.Options.AppIcon == null)
        currentProject.Options.AppIcon = "data:image/svg+xml,%3Csvg style='enable-background:new' xmlns='http://www.w3.org/2000/svg' height='512' viewBox='0 0 512.40002 512' width='512.4' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cdefs%3E%3ClinearGradient id='a' y2='494' gradientUnits='userSpaceOnUse' x2='256' gradientTransform='matrix(1 0 0 1.076 0 -19.36)' y1='18' x1='256'%3E%3Cstop style='stop-color:%23eaeaea' offset='0'/%3E%3Cstop style='stop-color:%23f4f4f4' offset='1'/%3E%3C/linearGradient%3E%3C/defs%3E%3Crect style='color:%23000000;enable-background:accumulate;fill:url(%23a)' height='512' width='512' y='0' x='0'/%3E%3Cpath style='opacity:.1' d='m256 112c-79.5 0-144 64.5-144 144s64.5 144 144 144 144-64.5 144-144-64.5-144-144-144zm49.47 39.98c3.098 0.0797 6.222 0.9104 9.096 2.568 9.197 5.312 12.34 17.07 7.029 26.26-5.305 9.196-17.06 12.34-26.25 7.029-9.19-5.305-12.34-17.06-7.039-26.25 3.653-6.322 10.35-9.787 17.17-9.611zm-49.48 20.5c7.731 0 15.21 1.074 22.31 3.035 1.253 7.726 5.837 14.84 13.15 19.06 7.298 4.207 15.74 4.629 23.05 1.867 14.22 13.98 23.47 32.99 24.81 54.17l-27.39 0.4043c-2.5-28.7-26.6-51.2-55.9-51.2-8.451 0-16.47 1.88-23.65 5.225l-13.4-23.9c11.16-5.522 23.72-8.645 37.01-8.645zm-46.33 14.03 14.06 23.54c-14.44 10.16-23.88 26.95-23.88 45.95s9.437 35.78 23.88 45.94l-14.06 23.55c-16.83-11.24-29.34-28.43-34.54-48.55 6.069-4.952 9.953-12.49 9.953-20.94 0-8.451-3.884-15.99-9.953-20.94 5.201-20.12 17.72-37.3 34.54-48.55zm-51.58 50.26c10.62 0 19.22 8.608 19.22 19.23s-8.605 19.22-19.22 19.22-19.23-8.605-19.23-19.22 8.607-19.23 19.23-19.23zm153.8 24.21 27.4 0.4023c-1.348 21.18-10.6 40.19-24.81 54.17-7.31-2.762-15.75-2.347-23.05 1.867-7.309 4.227-11.89 11.34-13.15 19.06-7.104 1.967-14.58 3.041-22.31 3.041-13.29 0-25.86-3.124-37.01-8.652l13.36-23.94c7.186 3.344 15.2 5.225 23.65 5.225 29.34 0 53.41-22.49 55.93-51.18zm-7.482 60.6c6.816-0.1758 13.51 3.289 17.16 9.607 5.311 9.197 2.16 20.95-7.035 26.25-9.196 5.311-20.95 2.159-26.25-7.037-5.312-9.191-2.159-20.94 7.037-26.25 2.872-1.658 5.994-2.49 9.092-2.57z'/%3E%3Cg transform='translate(0,18)'%3E%3Cpath style='opacity:.3;color:%23000000;enable-background:accumulate;fill:%23fff' d='m256 256l-147.8 256h403.8v-256h-122.7-133.3z' transform='translate(0,-18)'/%3E%3Cpath style='opacity:.15;color:%23000000;enable-background:accumulate;fill:%23fff' d='m0 0v512h108.2l76.4-132.3 71.4-123.7-68.9-120-0.2 0.3-78.7-136.3h-108.2z' transform='translate(0,-18)'/%3E%3C/g%3E%3Cpath d='m-208.3 605.6c-7.842 0-14.55 5.029-17.32 12.19-0.0612-0.00067-0.1208 0-0.1823 0-7.088 0-12.83 6.002-12.83 13.41 0 7.404 5.746 13.41 12.83 13.41h17.5 16.33c5.155 0 9.333-4.365 9.333-9.75 0-4.543-2.974-8.363-7-9.445 0.001-0.1029 0-0.2015 0-0.3047 0-10.77-8.357-19.5-18.67-19.5z' style='stroke-width:4;color:%23000000;stroke:%23ccc;enable-background:accumulate;display:none;fill:%23fff'/%3E%3Cpath style='stroke-width:4;color:%23000000;stroke:%23ccc;enable-background:accumulate;display:none;fill:none' d='m288.9 828.1c-6.349 0-11.78 4.072-14.02 9.867-0.0496-0.00055-0.0978 0-0.1476 0-5.738 0-10.39 4.86-10.39 10.85 0 5.995 4.652 10.85 10.39 10.85h14.17 13.22c4.173 0 7.557-3.534 7.557-7.894 0-3.678-2.408-6.771-5.667-7.647 0.001-0.0833 0-0.1632 0-0.2467 0-8.719-6.766-15.79-15.11-15.79z'/%3E%3Cg style='display:none' transform='translate(0 -558.4)'%3E%3Cpath d='m166.1 576.4h179.9c145.3 0 166.1 20.74 166.1 165.9v144.2c0 145.2-20.76 165.9-166.1 165.9h-179.9c-145.3 0-166.1-20-166.1-165.5v-144.2c0-145.2 20.76-165.9 166.1-165.9z' style='stroke:%2300f;fill:%2300f;display:inline;fill-opacity:.08118'/%3E%3Ccircle style='stroke-width:.8333;fill-opacity:.08118;color:%23000000;stroke:%2300f;enable-background:accumulate;display:inline;fill:%2300f' cx='256' transform='matrix(1.2 0 0 1.2 -51.2 528.8)' cy='238' r='120'/%3E%3Crect style='stroke-linejoin:round;fill-opacity:.08118;color:%23000000;stroke:%2300f;stroke-linecap:round;enable-background:accumulate;display:inline;fill:%2300f' height='272' width='272' y='678.4' x='120'/%3E%3Crect style='stroke-linejoin:round;fill-opacity:.08118;color:%23000000;stroke:%2300f;stroke-linecap:round;enable-background:accumulate;display:inline;fill:%2300f' height='240' width='304' y='694.4' x='104'/%3E%3Crect style='stroke-linejoin:round;fill-opacity:.08118;color:%23000000;stroke:%2300f;stroke-linecap:round;enable-background:accumulate;display:inline;fill:%2300f' height='304' width='240' y='662.4' x='136'/%3E%3C/g%3E%3C/svg%3E";
    if (currentProject.Options.PackageName == null) {
        var author = options.author;
        if (author == null || author.length == 0) {
            author = "author";
        }
        else {
            var authorSpaceIndex = author.indexOf(" ");
            if (authorSpaceIndex > -1)
                author = author.substring(0, authorSpaceIndex);
            else
                author = author.replaceAll2(" ", "");
        }
        currentProject.Options.PackageName = currentProject.Name.toLowerCase().replaceAll2(" ", "") + "." + author.toLowerCase();
    }
    if (currentProject.Options.Version == null)
        currentProject.Options.Version = "1.0.0.0";
    if (currentProject.Options.Summary == null)
        currentProject.Options.Summary = "";
    // if (currentProject.Options.Screenshots == null)
    //     currentProject.Options.Screenshots = [];

    document.getElementById("ubuntutheme").checked = currentProject.Options.UbuntuTheme;
    document.getElementById("app-icon").style.backgroundImage = "url(\"" + currentProject.Options.AppIcon + "\")";
    document.getElementById("packagename").value = currentProject.Options.PackageName;
    document.getElementById("version").value = currentProject.Options.Version;
    document.getElementById("summary").value = currentProject.Options.Summary;
    // updateScreenshots();

    var elem = document.getElementById("project-options");
    if (elem.style.display == "none") {
        elem.style.display = "";
        elem.scrollTop = 0;
    }
    else {
        elem.style.display = "none";
    }
}

function loadAppIcon() {
    var fileInput = recreateFileInput();
    fileInput.addEventListener("change", function() { loadAppIcon_do(); });
    fileInput.click();
}

function loadAppIcon_do() {
    var input = document.getElementById("file");
    if (input.files.length > 0) {
        var reader = new FileReader();
        reader.addEventListener("load", function() {
            currentProject.Options.AppIcon = reader.result;
            document.getElementById("app-icon").style.backgroundImage = "url(\"" + currentProject.Options.AppIcon + "\")";
        });
        reader.readAsDataURL(input.files[0]);
    }
}

function addScreenshot() {
    var fileInput = recreateFileInput();
    fileInput.addEventListener("change", function() { addScreenshot_do(); });
    fileInput.click();
}

function addScreenshot_do() {
    var input = document.getElementById("file");
    if (input.files.length > 0) {
        var reader = new FileReader();
        reader.addEventListener("load", function() {
            currentProject.Options.Screenshots.push(reader.result);
            updateScreenshots();
        });
        reader.readAsDataURL(input.files[0]);
    }
}

function updateScreenshots() {
    var screenshotHtml = "";
    for (var i = 0; i < currentProject.Options.Screenshots.length; i++) {
        screenshotHtml += "<div style=\"width:100px;height:200px;background-repeat:no-repeat;background-position:center;background-size:contain;background-image:url(" + currentProject.Options.Screenshots[i] + ");\" onclick=\"removeScreenshot(" + i + ")\"></div>";
    }
    document.getElementById("screenshots").innerHTML = screenshotHtml;
    if (currentProject.Options.Screenshots.length >= 5)
        document.getElementById("add-screenshot-button").style.display = "none";
    else
        document.getElementById("add-screenshot-button").style.display = "";
    if (currentProject.Options.Screenshots.length == 0)
        document.getElementById("remove-screenshot-hint").style.display = "none";
    else
        document.getElementById("remove-screenshot-hint").style.display = "";
}

function removeScreenshot(index) {
    currentProject.Options.Screenshots.splice(index, 1);
    updateScreenshots();
}

function saveProjectOptions() {
    currentProject.Options.UbuntuTheme = document.getElementById("ubuntutheme").checked;
    currentProject.Options.PackageName = document.getElementById("packagename").value;
    currentProject.Options.Version = document.getElementById("version").value;
    currentProject.Options.Summary = document.getElementById("summary").value;
    putObjects("projects", projects);
    getProjects();
    currentProject = projects.find(item => item.Id == currentProject.Id);
    switchProjectOptionsWindow();
}

function restoreCurrentProject() {
    getProjects();
    currentProject = projects.find(item => item.Id == currentProject.Id);
}

function switchLiveExec() {
    liveexecution = !liveexecution;
    document.getElementById("liveexec").value = liveexecution;
    if (!liveexecution) {
        document.getElementById("output-window").contentWindow.document.open();
        document.getElementById("output-window").contentWindow.document.write("<!DOCTYPE html><html><body></body></html>");
        document.getElementById("output-window").contentWindow.document.close();    
    }
}

function prepare_PublishProject() {
    if (currentProject.Options != null) {
        putObjects("projects", projects);
        getProjects();
        currentProject = projects.find(item => item.Id == currentProject.Id);

        if (options.apikey == null || options.apikey == '') {
            showModalDialog("OpenStore API key is missing. Please enter the API key in the options dialog.");
            return;
        }
        var msg = "";
        if (currentProject.Options.UbuntuTheme == null || !currentProject.Options.UbuntuTheme)
            msg += "<li>Ubuntu Theme has to be enabled in the project options.</li>";
        if (currentProject.Options.Summary == null || currentProject.Options.Summary == '')
            msg += "<li>Summary may not be empty.</li>";
        if (currentProject.Options.PackageName == null || currentProject.Options.PackageName == '' || !currentProject.Options.PackageName.includes("."))
            msg += "<li>Package name should be provided in the form appname.author. E.g. <b>touchide.daniel</b></li>";
        if (currentProject.Options.Version == null || currentProject.Options.Version == '')
            msg += "<li>Version may not be empty.</li>";
        if (options.author == null || options.author == '')
            msg += "<li>Author may not be empty.</li>";
        if (options.email == null || options.email == '')
            msg += "<li>E-Mail may not be empty.</li>";
        if (currentProject.Functions[0].Commands.length < 5)
            msg += "<li>The app should do something (too less commands).</li>";
        
        if (msg.length == 0) {
            showModalConfirm("Your project is now ready for publishing. Start now?", publishProject);
        }
        else {
            showModalDialog("Your app can not yet be published. Please fix the following problems:<ul>" + msg + "</ul>");
        }
    }
    else {
        showModalDialog("This project is not yet prepared for submission to the OpenStore. Please see project options. Refer to <a href='https://gitlab.com/Schlicki2808/touch-ide/-/wikis/Publish-to-OpenStore' target='_new'>this documentation</a> for more information.");
    }
}

async function publishProject() {
    closeModalConfirm();
    showWaitDialog();
    var helper = currentProject.Options.PackageName.split(".");
    var rawName = helper[0];

    var zip = new JSZip();
    zip.file(rawName + ".desktop.in", getDesktopFile());
    zip.file(rawName + ".apparmor", getAppArmorFile());
    zip.file("clickable.yaml", getClickableYaml());
    zip.file("CMakeLists.txt", getCMakeListsFile(rawName));
    zip.file("manifest.json.in", getManifestJson(rawName));
    zip.file("contenthub.json", getContentHubJson());
    zip.file(".gitignore", getGitIgnore());
    var appIcon = decodeURIComponent(currentProject.Options.AppIcon);
    if (appIcon.startsWith("data:image/svg+xml;base64,"))
        zip.file("assets/logo.svg", atob(appIcon.replace("data:image/svg+xml;base64,", "")));
    else
        zip.file("assets/logo.svg", appIcon.replace("data:image/svg+xml,", ""));
    zip.file("qml/ImportPage.qml", getImportPageQml());
    zip.file("qml/Main.qml", getMainQml());
    zip.file("qml/www/index.html", generateOutputHTML());
    var zipContent = await zip.generateAsync({type:"base64"});

    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
        if (this.readyState == 4) {
            closeWaitDialog();
            if (req.status == 200) {
                var versionHelper = currentProject.Options.Version.split(".");
                if (versionHelper.length == 4) {
                    versionHelper[3] = (versionHelper[3] * 1) + 1;
                    currentProject.Options.Version = versionHelper[0] + "." + versionHelper[1] + "." + versionHelper[2] + "." + versionHelper[3];
                    putObjects("projects", projects);
                    getProjects();
                    currentProject = projects.find(item => item.Id == currentProject.Id);
                }
                showModalDialog("App was successfully published to OpenStore.");
            }
            else {
                showModalDialog("Publishing to OpenStore failed. " + req.statusText);
            }
        }
    }
    req.onerror = function(e) {
        closeWaitDialog();
        showModalDialog("Publishing to OpenStore failed.");
    }
    req.onabort = function(e) {
        closeWaitDialog();
        console.log('ABORT');
    }
    var requestData = {
        "Name": currentProject.Name,
        "Maintainer": options.author + " <" + options.email + ">",
        "ApiKey": options.apikey,
        "ZipFile": zipContent
    };
    req.open("POST", "https://www.touchide.net/api/publish", true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(JSON.stringify(requestData));
}