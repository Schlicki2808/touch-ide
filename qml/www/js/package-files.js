function getDesktopFile() {
    return "[Desktop Entry]\n"
         + "Name=" + currentProject.Name + "\n"
         + "Exec=qmlscene %U qml/Main.qml\n"
         + "Icon=assets/logo.svg\n"
         + "Terminal=false\n"
         + "Type=Application\n"
         + "X-Lomiri-Touch=true";
}

function getAppArmorFile() {
    return "{\n"
         + "    \"policy_groups\": [\n"
         + "        \"webview\",\n"
         + "        \"networking\",\n"
         + "        \"location\",\n"
         + "        \"content_exchange\",\n"
         + "        \"content_exchange_source\"\n"
         + "    ],\n"
         + "    \"policy_version\": 20.04\n"
         + "}\n"
}

function getImportPageQml() {
    return "/*\n"
    + " * Copyright (C) 2022  Daniel Schlieckmann\n"
    + " *\n"
    + " * This program is free software: you can redistribute it and/or modify\n"
    + " * it under the terms of the GNU General Public License as published by\n"
    + " * the Free Software Foundation; version 3.\n"
    + " *\n"
    + " * " + currentProject.Name + " is distributed in the hope that it will be useful,\n"
    + " * but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    + " * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    + " * GNU General Public License for more details.\n"
    + " *\n"
    + " * You should have received a copy of the GNU General Public License\n"
    + " * along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"
    + " */\n"
    + "\n"
    + "import QtQuick 2.4\n"
    + "import Lomiri.Components 1.3\n"
    + "import Lomiri.Content 1.3\n"
    + "\n"
    + "\n"
    + "\n"
    + "Page {\n"
    + "  id: picker\n"
    + "	property var activeTransfer\n"
    + "\n"
    + "	property var url\n"
    + "	property var handler\n"
    + "	property var contentType\n"
    + "\n"
    + "    signal cancel()\n"
    + "    signal imported(string fileUrl)\n"
    + "\n"
    + "    header: PageHeader {\n"
    + "        title: i18n.tr(\"Choose\")\n"
    + "    }\n"
    + "\n"
    + "    ContentPeerPicker {\n"
    + "        anchors { fill: parent; topMargin: picker.header.height }\n"
    + "        visible: parent.visible\n"
    + "        showTitle: false\n"
    + "        contentType: picker.contentType //ContentType.Pictures\n"
    + "        handler: picker.handler //ContentHandler.Source\n"
    + "\n"
    + "        onPeerSelected: {\n"
    + "            peer.selectionType = ContentTransfer.Single\n"
    + "            picker.activeTransfer = peer.request()\n"
    + "            picker.activeTransfer.stateChanged.connect(function() {\n"
    + "				        if (picker.activeTransfer.state === ContentTransfer.InProgress) {\n"
    + "					           picker.activeTransfer.items = picker.activeTransfer.items[0].url = url;\n"
    + "					           picker.activeTransfer.state = ContentTransfer.Charged;\n"
    + "				        }\n"
    + "                if (picker.activeTransfer.state === ContentTransfer.Charged) {\n"
    + "                     picker.imported(picker.activeTransfer.items[0].url)\n"
    + "                     picker.activeTransfer = null\n"
    + "                }\n"
    + "            })\n"
    + "        }\n"
    + "\n"
    + "\n"
    + "        onCancelPressed: {\n"
    + "            picker.cancel();\n"
    + "            pageStack.pop();\n"
    + "        }\n"
    + "    }\n"
    + "\n"
    + "    ContentTransferHint {\n"
    + "        id: transferHint\n"
    + "        anchors.fill: parent\n"
    + "        activeTransfer: picker.activeTransfer\n"
    + "    }\n"
    + "    Component {\n"
    + "        id: resultComponent\n"
    + "        ContentItem {}\n"
    + "	}\n"
    + "}\n";
}

function getMainQml() {
    return "/*\n"
    + " * Copyright (C) 2022  Daniel Schlieckmann\n"
    + " *\n"
    + " * This program is free software: you can redistribute it and/or modify\n"
    + " * it under the terms of the GNU General Public License as published by\n"
    + " * the Free Software Foundation; version 3.\n"
    + " *\n"
    + " * " + currentProject.Name + " is distributed in the hope that it will be useful,\n"
    + " * but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    + " * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    + " * GNU General Public License for more details.\n"
    + " *\n"
    + " * You should have received a copy of the GNU General Public License\n"
    + " * along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"
    + " */\n"
    + "\n"
    + "import QtQuick 2.7\n"
    + "import Lomiri.Components 1.3\n"
    + "import Lomiri.Components.Popups 1.3\n"
    + "import QtQuick.Controls 2.2\n"
    + "import QtQuick.Layouts 1.3\n"
    + "import Qt.labs.settings 1.0\n"
    + "import Morph.Web 0.1\n"
    + "import QtWebEngine 1.7\n"
    + "import Lomiri.DownloadManager 1.2\n"
    + "import Lomiri.Content 1.3\n"
    + "\n"
    + "MainView {\n"
    + "    id: root\n"
    + "    objectName: 'mainView'\n"
    + "    applicationName: '" + currentProject.Options.PackageName + "'\n"
    + "    automaticOrientation: true\n"
    + "\n"
    + "   PageStack {\n"
    + "        id: mainPageStack\n"
    + "        anchors.fill: parent\n"
    + "        Component.onCompleted: mainPageStack.push(pageMain)\n"
    + "\n"
    + "        Page {\n"
    + "            id: pageMain\n"
    + "            anchors.fill: parent\n"
    + "\n"
    + "            WebEngineView {\n"
    + "                id: mainWebView\n"
    + "                focus: true\n"
    + "                anchors {\n"
    + "                    top: parent.top\n"
    + "                    left: parent.left\n"
    + "                    right: parent.right\n"
    + "                    bottom: parent.bottom\n"
    + "                }\n"
    + "                settings {\n"
    + "                    javascriptCanAccessClipboard: true\n"
    + "                    localContentCanAccessFileUrls: true\n"
    + "                    localContentCanAccessRemoteUrls: true\n"
    + "                    allowRunningInsecureContent: true\n"
    + "                    allowWindowActivationFromJavaScript : true\n"
    + "                    pluginsEnabled: true\n"
    + "                }\n"
    + "                Component.onCompleted: {\n"
    + "                    settings.localStorageEnabled = true;\n"
    + "                }\n"
    + "\n"
    + "                profile: WebEngineProfile {\n"
    + "                    id: webContext\n"
    + "                    httpUserAgent: \"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\"\n"
    + "                    storageName: \"Storage\"\n"
    + "                    persistentStoragePath: \"/home/phablet/.local/share/" + currentProject.Options.PackageName + "\"\n"
    + "\n"
    + "                    onDownloadRequested: {\n"
    + "                        var fileUrl = \"/home/phablet/.local/share/" + currentProject.Options.PackageName + "/\" + download.downloadFileName;\n"
    + "                        var request = new XMLHttpRequest();\n"
    + "                        request.open(\"PUT\", fileUrl, false);\n"
    + "                        var downloadUrl = decodeURIComponent(download.url.toString());\n"
    + "                        if (downloadUrl.startsWith(\"data:image/png;base64,\")) {\n"
    + "                            var arr = base64DecToArr(downloadUrl.replace(\"data:image/png;base64,\", \"\"), 512);\n"
    + "                            request.send(arr);\n"
    + "                        }\n"
    + "                        else if (downloadUrl.startsWith(\"data:image/jpeg;base64,\")) {\n"
    + "                            var arr = base64DecToArr(downloadUrl.replace(\"data:image/jpeg;base64,\", \"\"), 512);\n"
    + "                            request.send(arr);\n"
    + "                        }\n"
    + "                        else {\n"
    + "                            request.send(downloadUrl.replace(\"data:text/plain;,\", \"\"));\n"
    + "                        }\n"
    + "                        fileExporter.file = fileUrl\n"
    + "                        PopupUtils.open(downloadedMessage);\n"
    + "                    }\n"
    + "\n"
    + "                    function b64ToUint6(nChr) {\n"
    + "                        return nChr > 64 && nChr < 91\n"
    + "                            ? nChr - 65\n"
    + "                            : nChr > 96 && nChr < 123\n"
    + "                            ? nChr - 71\n"
    + "                            : nChr > 47 && nChr < 58\n"
    + "                            ? nChr + 4\n"
    + "                            : nChr === 43\n"
    + "                            ? 62\n"
    + "                            : nChr === 47\n"
    + "                            ? 63\n"
    + "                            : 0;\n"
    + "                    }\n"
    + "\n"
    + "                    function base64DecToArr(sBase64, nBlocksSize) {\n"
    + "                        const sB64Enc = sBase64.replace(/[^A-Za-z0-9+/]/g, \"\");\n"
    + "                        const nInLen = sB64Enc.length;\n"
    + "                        const nOutLen = nBlocksSize\n"
    + "                            ? Math.ceil(((nInLen * 3 + 1) >> 2) / nBlocksSize) * nBlocksSize\n"
    + "                            : (nInLen * 3 + 1) >> 2;\n"
    + "                        const taBytes = new Uint8Array(nOutLen);\n"
    + "\n"
    + "                        let nMod3;\n"
    + "                        let nMod4;\n"
    + "                        let nUint24 = 0;\n"
    + "                        let nOutIdx = 0;\n"
    + "                        for (let nInIdx = 0; nInIdx < nInLen; nInIdx++) {\n"
    + "                            nMod4 = nInIdx & 3;\n"
    + "                            nUint24 |= b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << (6 * (3 - nMod4));\n"
    + "                            if (nMod4 === 3 || nInLen - nInIdx === 1) {\n"
    + "                            nMod3 = 0;\n"
    + "                            while (nMod3 < 3 && nOutIdx < nOutLen) {\n"
    + "                                taBytes[nOutIdx] = (nUint24 >>> ((16 >>> nMod3) & 24)) & 255;\n"
    + "                                nMod3++;\n"
    + "                                nOutIdx++;\n"
    + "                            }\n"
    + "                            nUint24 = 0;\n"
    + "                            }\n"
    + "                        }\n"
    + "\n"
    + "                        return taBytes.buffer;\n"
    + "                    }\n"
    + "\n"
    + "\n"
    + "                }\n"
    + "\n"
    + "                zoomFactor: units.gu(1) / 8.4\n"
    + "                url: Qt.resolvedUrl('www/index.html')\n"
    + "\n"
    + "                onFileDialogRequested: function(request) {\n"
    + "                    request.accepted = true;\n"
    + "                    var importPage = mainPageStack.push(Qt.resolvedUrl(\"ImportPage.qml\"),{\"contentType\": ContentType.All, \"handler\": ContentHandler.Source});\n"
    + "                    importPage.imported.connect(function(fileUrl) {\n"
    + "                        request.dialogAccept(String(fileUrl).replace(\"file://\", \"\"));\n"
    + "                        mainPageStack.push(pageMain)\n"
    + "                    });\n"
    + "                }\n"
    + "                onNewViewRequested: {\n"
    + "                    request.action = WebEngineNavigationRequest.IgnoreRequest\n"
    + "                    if(request.userInitiated) {\n"
    + "                        Qt.openUrlExternally(request.requestedUrl)\n"
    + "                    }\n"
    + "                }\n"
    + "                onFeaturePermissionRequested: function(url, feature) {\n"
    + "                    grantFeaturePermission(url, feature, true);\n"
    + "                }\n"
    + "            }\n"
    + "\n"
    + "            Component {\n"
    + "                id: downloadedMessage\n"
    + "                Popover {\n"
    + "                    id: popover\n"
    + "                    Rectangle {\n"
    + "                        color: \"green\"\n"
    + "                        width: popover.width\n"
    + "                        height: units.gu(10)\n"
    + "                        anchors {\n"
    + "                            centerIn: parent\n"
    + "                            margins: units.gu(1)\n"
    + "                        }\n"
    + "\n"
    + "                        Column {\n"
    + "                            anchors {\n"
    + "                                centerIn: parent\n"
    + "                            }\n"
    + "                            spacing: units.gu(1.5)\n"
    + "                            width: parent.width - units.gu(4)\n"
    + "\n"
    + "                            Label {\n"
    + "                                text: \"Download completed.\"\n"
    + "                                anchors.horizontalCenter: parent.horizontalCenter\n"
    + "                                color: \"white\"\n"
    + "                            }\n"
    + "                            Icon {\n"
    + "                                color: theme.palette.normal.baseText\n"
    + "                                anchors.horizontalCenter: parent.horizontalCenter\n"
    + "                                height: width\n"
    + "                                name: \"share\"\n"
    + "                                width: units.gu(3)\n"
    + "\n"
    + "                                MouseArea {\n"
    + "                                    anchors.fill: parent\n"
    + "                                    onClicked: {\n"
    + "                                        popover.hide();\n"
    + "                                        fileExporter.visible = true\n"
    + "                                    }\n"
    + "                                }\n"
    + "                            }\n"
    + "                        }\n"
    + "                    }\n"
    + "                }\n"
    + "            }\n"
    + "\n"
    + "            Component {\n"
    + "                id: fileExporterItemComponent\n"
    + "                ContentItem {}\n"
    + "            }\n"
    + "\n"
    + "            ContentPeerPicker {\n"
    + "                id: fileExporter\n"
    + "\n"
    + "                property string file : \"\"\n"
    + "                property var activeTransfer : null\n"
    + "\n"
    + "                anchors.fill: parent\n"
    + "                visible: false\n"
    + "\n"
    + "                handler: ContentHandler.Destination\n"
    + "                contentType: ContentType.Documents\n"
    + "\n"
    + "                onPeerSelected: {\n"
    + "                    activeTransfer = peer.request();\n"
    + "                    activeTransfer.items = [ fileExporterItemComponent.createObject(root, {\"url\": file}) ];\n"
    + "                    activeTransfer.state = ContentTransfer.Charged;\n"
    + "                    visible = false\n"
    + "                }\n"
    + "\n"
    + "                onCancelPressed: {\n"
    + "                    visible = false\n"
    + "                }\n"
    + "            }\n"
    + "\n"
    + "        }\n"
    + "\n"
    + "    }\n"
    + "}\n";    
}

function getClickableYaml() {
    return "clickable_minimum_required: 8.0.0\n"
         + "builder: pure-qml-cmake\n"
         + "kill: qmlscene";
}

function getCMakeListsFile(rawName) {
    return "cmake_minimum_required(VERSION 3.0.0)\n"
    + "project(" + rawName + " C CXX)\n"
    + "\n"
    + "# Automatically create moc files\n"
    + "set(CMAKE_AUTOMOC ON)\n"
    + "\n"
    + "execute_process(\n"
    + "    COMMAND dpkg-architecture -qDEB_HOST_MULTIARCH\n"
    + "    OUTPUT_VARIABLE ARCH_TRIPLET\n"
    + "    OUTPUT_STRIP_TRAILING_WHITESPACE\n"
    + ")\n"
    + "set(QT_IMPORTS_DIR \"lib/${ARCH_TRIPLET}\")\n"
    + "\n"
    + "set(PROJECT_NAME \"" + rawName + "\")\n"
    + "set(FULL_PROJECT_NAME \"" + currentProject.Options.PackageName + "\")\n"
    + "set(DATA_DIR /)\n"
    + "set(DESKTOP_FILE_NAME ${PROJECT_NAME}.desktop)\n"
    + "\n"
    + "# This command figures out the minimum SDK framework for use in the manifest\n"
    + "# file via the environment variable provided by Clickable or sets a default value otherwise.\n"
    + "if(DEFINED ENV{SDK_FRAMEWORK})\n"
    + "    set(CLICK_FRAMEWORK \"$ENV{SDK_FRAMEWORK}\")\n"
    + "else()\n"
    + "    set(CLICK_FRAMEWORK \"ubuntu-sdk-20.04\")\n"
    + "endif()\n"
    + "\n"
    + "# This figures out the target architecture for use in the manifest file.\n"
    + "if(DEFINED ENV{ARCH})\n"
    + "    set(CLICK_ARCH \"$ENV{ARCH}\")\n"
    + "else()\n"
    + "    execute_process(\n"
    + "        COMMAND dpkg-architecture -qDEB_HOST_ARCH\n"
    + "        OUTPUT_VARIABLE CLICK_ARCH\n"
    + "        OUTPUT_STRIP_TRAILING_WHITESPACE\n"
    + "    )\n"
    + "endif()\n"
    + "\n"
    + "configure_file(manifest.json.in ${CMAKE_CURRENT_BINARY_DIR}/manifest.json)\n"
    + "install(FILES ${CMAKE_CURRENT_BINARY_DIR}/manifest.json DESTINATION ${CMAKE_INSTALL_PREFIX})\n"
    + "install(FILES ${PROJECT_NAME}.apparmor DESTINATION ${DATA_DIR})\n"
    + "install(FILES contenthub.json DESTINATION ${CMAKE_INSTALL_PREFIX})\n"
    + "\n"
    + "install(DIRECTORY assets DESTINATION ${DATA_DIR})\n"
    + "\n"
    + "install(DIRECTORY qml DESTINATION ${DATA_DIR})\n"
    + "\n"
    + "# Translations\n"
    + "file(GLOB_RECURSE I18N_SRC_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/po qml/*.qml qml/*.js)\n"
    + "list(APPEND I18N_SRC_FILES ${DESKTOP_FILE_NAME}.in.h)\n"
    + "\n"
    + "find_program(INTLTOOL_MERGE intltool-merge)\n"
    + "if(NOT INTLTOOL_MERGE)\n"
    + "    message(FATAL_ERROR \"Could not find intltool-merge, please install the intltool package\")\n"
    + "endif()\n"
    + "find_program(INTLTOOL_EXTRACT intltool-extract)\n"
    + "if(NOT INTLTOOL_EXTRACT)\n"
    + "    message(FATAL_ERROR \"Could not find intltool-extract, please install the intltool package\")\n"
    + "endif()\n"
    + "\n"
    + "add_custom_target(${DESKTOP_FILE_NAME} ALL\n"
    + "    COMMENT \"Merging translations into ${DESKTOP_FILE_NAME}...\"\n"
    + "    COMMAND LC_ALL=C ${INTLTOOL_MERGE} -d -u ${CMAKE_SOURCE_DIR}/po ${CMAKE_SOURCE_DIR}/${DESKTOP_FILE_NAME}.in ${DESKTOP_FILE_NAME}\n"
    + "    COMMAND sed -i 's/${PROJECT_NAME}-//g' ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME}\n"
    + ")\n"
    + "\n"
    + "install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME} DESTINATION ${DATA_DIR})\n"
    + "\n"
    + "\n"
    + "# Make source files visible in qtcreator\n"
    + "file(GLOB_RECURSE PROJECT_SRC_FILES\n"
    + "    RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}\n"
    + "    qml/*.qml\n"
    + "    qml/*.js\n"
    + "    src/*\n"
    + "    *.json\n"
    + "    *.json.in\n"
    + "    *.apparmor\n"
    + "    *.desktop.in\n"
    + ")\n"
    + "\n"
    + "add_custom_target(${PROJECT_NAME}_FILES ALL SOURCES ${PROJECT_SRC_FILES})\n";
}

function getManifestJson(rawName) {
    return "{\n"
    + "    \"name\": \"" + currentProject.Options.PackageName + "\",\n"
    + "    \"description\": \"" + currentProject.Options.Summary + "\",\n"
    + "    \"architecture\": \"@CLICK_ARCH@\",\n"
    + "    \"title\": \"" + currentProject.Name + "\",\n"
    + "    \"hooks\": {\n"
    + "        \"" + rawName + "\": {\n"
    + "            \"apparmor\": \"" + rawName + ".apparmor\",\n"
    + "            \"desktop\":  \"" + rawName + ".desktop\",\n"
    + "            \"content-hub\": \"contenthub.json\"\n"
    + "        }\n"
    + "    },\n"
    + "    \"version\": \"" + currentProject.Options.Version + "\",\n"
    + "    \"maintainer\": \"" + options.author + " <" + options.email + ">\",\n"
    + "    \"framework\" : \"@CLICK_FRAMEWORK@\"\n"
    + "}\n"
}

function getContentHubJson() {
    return "{\n"
         + "    \"source\": [\n"
         + "        \"text\"\n"
         + "    ]\n"
         + "}\n";
}

function getGitIgnore() {
    return "/build/\n"
         + "/.clickable/"
}