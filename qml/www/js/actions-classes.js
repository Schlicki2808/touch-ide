function showClassList(mode) {
    var command = "selectClass";
    if (mode == "set")
        command = "createClass";
    else if (mode == "setList")
        command = "createClassList";
    var classListHTML = "<div class=\"panel-header panel-header-classes\">Classes</div>";
    for (var i = 0; i < functions.length; i++) {
        if (functions[i].Type == "class") {
            classListHTML += "<div onclick=\"" + command + "('" + functions[i].Id + "', event)\">";
            classListHTML += functions[i].Name;
            classListHTML += "</div>";
        }
    }
    document.getElementById("class-list").innerHTML = classListHTML;
    document.getElementById("class-list-container").style.display = "";
}

function closeClassList() {
    document.getElementById("class-list-container").style.display = "none";
}

function addClass(event) {
    closeModalInput();
    var className = document.getElementById("modal-input-value").value;
    var newClass = { "Id" : generateGuid(), "Type": "class", "Name": className, "Commands": [ generateEmptyCommand() ], "Parameters": [] }
    functions.push(newClass);
    selectClass(newClass.Id, event);
}

function selectClass(classId, event) {
    closeClassList();
    currentFunction = functions.find((item) => item.Id == classId);
    commands = currentFunction.Commands;
    unselectCommand(event);
    if (currentFunction.Type == "function" && functions.findIndex((item) => item.Id == functionId) == 0) {
        document.getElementById("btn-func-rename").style.display = "none";
        document.getElementById("btn-func-delete").style.display = "none";
    }
    else {
        document.getElementById("btn-func-rename").style.display = "";
        document.getElementById("btn-func-delete").style.display = "";
    }
}