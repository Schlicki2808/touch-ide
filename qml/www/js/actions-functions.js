function showFunctionsList(mode) {
    var command = "selectFunction";
    if (mode == "set")
        command = "createFunction";
    var functionListHTML = "<div class=\"panel-header panel-header-functions\">Functions</div>";
    for (var i = 0; i < functions.length; i++) {
        if (functions[i].Type == "function") {
            functionListHTML += "<div onclick=\"" + command + "('" + functions[i].Id + "', event)\">";
            functionListHTML += functions[i].Name;
            functionListHTML += "</div>";
        }
    }
    document.getElementById("functions-list").innerHTML = functionListHTML;
    document.getElementById("functions-list-container").style.display = "";
}

/*

        <div id="testitem" class="slidebutton" style="position:relative;margin-top:300px;background-color:red;z-index: 100;">
            <button id="testitem-button" style="position:relative;left:0;top:0;height:100%;width:0;background-color:blue;transition:0.3s;"></button>
        </div>

*/

function closeFunctionsList() {
    document.getElementById("functions-list-container").style.display = "none";
}

function addFunction(event) {
    closeModalInput();
    var functionName = document.getElementById("modal-input-value").value;
    var newFunction = { "Id" : generateGuid(), "Type": "function", "Name": functionName, "Commands": [ generateEmptyCommand() ], "Parameters": [] }
    functions.push(newFunction);
    selectFunction(newFunction.Id, event);
}

function selectFunction(functionId, event) {
    closeFunctionsList();
    currentFunction = functions.find((item) => item.Id == functionId);
    commands = currentFunction.Commands;
    unselectCommand(event);
    if (functions.findIndex((item) => item.Id == functionId) == 0) {
        document.getElementById("btn-func-rename").style.display = "none";
        document.getElementById("btn-func-delete").style.display = "none";
    }
    else {
        document.getElementById("btn-func-rename").style.display = "";
        document.getElementById("btn-func-delete").style.display = "";
    }
}

function iterateExpressionForFunctionRename(expression, oldFunctionName, newFunctionName) {
    if (expression.Type == "function" && expression.Name == oldFunctionName)
        expression.Name = newFunctionName;
    
    if (expression.Expressions != null && expression.Expressions.length > 0) {
        for (var e = 0; e < expression.Expressions.length; e++) {
            iterateExpressionForFunctionRename(expression.Expressions[e], oldFunctionName, newFunctionName);
        }
    }
}

function iterateExpressionForClassRename(expression, oldFunctionName, newFunctionName) {
    //console.log(expression);
    if (expression.Type == "namespace" && expression.Name == oldFunctionName)
        expression.Name = newFunctionName;

    if (expression.DataType != null && expression.DataType == oldFunctionName)
        expression.DataType = newFunctionName;

    if (expression.ClassName != null && expression.ClassName == oldFunctionName)
        expression.ClassName = newFunctionName;
    
    if (expression.Expressions != null && expression.Expressions.length > 0) {
        for (var e = 0; e < expression.Expressions.length; e++) {
            iterateExpressionForClassRename(expression.Expressions[e], oldFunctionName, newFunctionName);
        }
    }
}

function renameFunction(event) {
    closeModalInput();
    var oldFunctionName = currentFunction.Name;
    var newFunctionName = document.getElementById("modal-input-value").value;
    var currentType = currentFunction.Type;
    currentFunction.Name = newFunctionName;

    for (var f = 0; f < functions.length; f++) {
        if (currentType == "function" && functions[f].Type == currentType) {
            for (var c = 0; c < functions[f].Commands.length; c++) {
                for (var e = 0; e < functions[f].Commands[c].Expressions.length; e++) {
                    iterateExpressionForFunctionRename(functions[f].Commands[c].Expressions[e], oldFunctionName, newFunctionName);
                }    
            }
        }
        else if (currentType == "class" && functions[f].Type == "function") {
            for (var c = 0; c < functions[f].Commands.length; c++) {
                var cmd = functions[f].Commands[c];
                if (cmd.ClassName != null && cmd.ClassName.length > 0 && cmd.ClassName == oldFunctionName)
                    cmd.ClassName = newFunctionName;
                if (cmd.VariableType != null && cmd.VariableType.length > 0 && cmd.VariableType == oldFunctionName)
                    cmd.VariableType = newFunctionName;
                for (var e = 0; e < functions[f].Commands[c].Expressions.length; e++) {
                    iterateExpressionForClassRename(functions[f].Commands[c].Expressions[e], oldFunctionName, newFunctionName);
                }    
            }
        }
    }

    selectFunction(currentFunction.Id, event);
}

function deleteCurrentFunction(event) {
    var index = functions.findIndex((item) => item.Id == currentFunction.Id);
    functions.splice(index, 1);
    selectFunction(functions[0].Id, event);
}

function addFunctionParameterAsString() {
    closeModalInput();
    var newParameter = {
        "Id": generateGuid(),
        "Name": document.getElementById("modal-input-value").value,
        "DataType": "string"
    };
    currentFunction.Parameters.push(newParameter);
    displayCodeContainer();
}

function addFunctionParameterAsNumber() {
    closeModalInput();
    var newParameter = {
        "Id": generateGuid(),
        "Name": document.getElementById("modal-input-value").value,
        "DataType": "number"
    };
    currentFunction.Parameters.push(newParameter);
    displayCodeContainer();
}

function addFunctionParameterAsBoolean() {
    closeModalInput();
    var newParameter = {
        "Id": generateGuid(),
        "Name": document.getElementById("modal-input-value").value,
        "DataType": "boolean"
    };
    currentFunction.Parameters.push(newParameter);
    displayCodeContainer();
}

function renameCurrentFunctionParameter() {
    closeModalInput();
    currentFunctionParameter.Name = document.getElementById("modal-input-value").value;
    displayCodeContainer();
}

function deleteCurrentFunctionParameter(event) {
    var index = currentFunction.Parameters.findIndex((item) => item.Id == currentFunctionParameter.Id);
    currentFunction.Parameters.splice(index, 1);
    currentFunctionParameter = null;
    displayCodeContainer();
    hideCommandPanel(event);
}