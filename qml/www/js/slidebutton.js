var lastX = 0;
var buttonName = "";

function touchDown(sender, event) {
    buttonName = sender.id + "-button";
    lastX = event.touches[0].pageX;
}

function mouseDown(sender) {
    buttonName = sender.id + "-button";
}

function touchMove(event) {
    if (buttonName.length > 0) {
        var element = document.getElementById(buttonName);
        var x = event.touches[0].pageX;
        if (x > lastX && element.style.width == "0px")
            element.style.width = "52px";
        else if (x < lastX && element.style.width == "52px")
            element.style.width = "0px";
    }
}

function mouseMove(event) {
    if (buttonName.length > 0) {
        var element = document.getElementById(buttonName);
        if (event.movementX > 0 && element.style.width == "0px")
            element.style.width = "52px";
        else if (event.movementX < 0 && element.style.width == "52px")
            element.style.width = "0px";
    }
}

function touchUp() {
    buttonName = "";
}


function updateSlideButtons() {
    var slideButtons = document.getElementsByClassName("slidebutton");
    for (var i = 0; i < slideButtons.length; i++) {
        slideButtons[i].removeEventListener("touchstart", function(event) { touchDown(this, event); });
        slideButtons[i].removeEventListener("touchmove", function(event) { touchMove(event); });
        slideButtons[i].removeEventListener("touchend", function() { touchUp(); });
        slideButtons[i].removeEventListener("mousedown", function() { mouseDown(this); });
        slideButtons[i].removeEventListener("mousemove", function(event) { mouseMove(event); });
        slideButtons[i].removeEventListener("mouseup", function() { touchUp(); });

        slideButtons[i].addEventListener("touchstart", function(event) { touchDown(this, event); });
        slideButtons[i].addEventListener("touchmove", function(event) { touchMove(event); });
        slideButtons[i].addEventListener("touchend", function() { touchUp(); });
        slideButtons[i].addEventListener("mousedown", function() { mouseDown(this); });
        slideButtons[i].addEventListener("mousemove", function(event) { mouseMove(event); });
        slideButtons[i].addEventListener("mouseup", function() { touchUp(); });
    }
}