var commandsReference = [
    {
        "Type": "help-text",
        "Name": "if",
        "Category": "Basic commands",
        "Description": "With if conditions, you can implement routines that should only be executed if certain conditions are met. This means that the whole condition specified should return true. You can also extend an if condition with else or else if.<br><br>The following example shows the different possibilities to use the if structure:<br><br><img src=\"if.png\"><br><br>As you can see, you can't use function calls directly to check for conditions. Instead, store the results of the functions into variables and check the variables. In the example above, you see, how the given number is stored into a variable named \"number\"."
    },
    {
        "Type": "help-text",
        "Name": "for",
        "Category": "Basic commands",
        "Description": "With a for loop you can count a number variable from a start value to an end value. The start value and the end value can either be a fixed number or it can be a statement containing variables. The for loop will always count from a lower value up to a higher value. If the start value and the end value are equal, the for loop will not be executed.<br><br>The following example counts the variable i from 1 to 10 and outputs the value.<br><br><img src=\"for.png\"><br><br>There is also a <a href=\"#foreach\">foreach</a> loop that does not count a value, but iterates through a <a href=\"#collection\">collection</a>."
    },
    {
        "Type": "help-text",
        "Name": "foreach",
        "Category": "Basic commands",
        "Description": "A foreach loop iterates the items of a <a href=\"#collection\">collection</a>.<br><br>The following example prepares a collection with a few names and prints them on the screen.<br><br><img src=\"foreach.png\">"
    },
    {
        "Type": "help-text",
        "Name": "while",
        "Category": "Basic commands",
        "Description": "A while loop is executed as long as the condition specified returns true. Be careful: it is possible to create an endless loop so that you have to kill the app and possibly lose data.<br><br>The following example shows how to add a number while it is smaller than 10.<br><br><img src=\"while.png\">"
    },
    {
        "Type": "help-text",
        "Name": "loop",
        "Category": "Basic commands",
        "Description": "The loop is an endless loop that iterates forever. The difference to a while loop is that it doesn't freeze the app. This loop will execute, then take a breath for a few milliseconds and then start over. The number of milliseconds has to be set in the argument.<br><br>If you call other functions from within a loop, be sure to <a href=\"#break\">break</a> the loop to avoid unexpected effects.<br><br>For example, you can instantly display the current time without freezing the app. The update delay is 200 milliseconds in this example.<br><br><img src=\"loop.png\">"
    },
    {
        "Type": "help-text",
        "Name": "return",
        "Category": "Basic commands",
        "Description": "Functions can return a value that can be used for further processing. This value can be a string, a number, a boolean or an object defined by a custom class."
    },
    {
        "Type": "help-text",
        "Name": "break",
        "Category": "Basic commands",
        "Description": "A break command interrupts a <a href=\"#for\">for</a>, <a href=\"#foreach\">foreach</a>, <a href=\"#while\">while</a> loop and an endless <a href=\"#loop\">loop</a>. Interrupting means that the loop will be canceled and the commands after the loop will be executed.<br><br>The following example shows how to break an endless loop by clicking a button.<br><br><img src=\"break.png\">"
    },
    {
        "Type": "namespace",
        "Name": "boolean",
        "Category": "Basic Data Types",
        "Description": "A boolean can be used as a switch which can hold the values true and false. All conditions specified in an <a href=\"#if\">if</a> statement or a <a href=\"#while\">while</a> loop have to return true in order to be validated.",
    },
    {
        "Type": "method",
        "Name": "switch",
        "BelongsToNamespace": "boolean",
        "Description": "Switches between the values true and false.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set true",
        "BelongsToNamespace": "boolean",
        "Description": "Sets the boolean to true.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set false",
        "BelongsToNamespace": "boolean",
        "Description": "Sets the boolean to false.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set",
        "BelongsToNamespace": "boolean",
        "Description": "Sets the boolean to a specific value which can be true, false or a boolean variable.",
        "Arguments": [
            { "Name": "value", "Description": "Value to set.", "DataType": "boolean", "DefaultValue": "true" }
        ]
    },
    {
        "Type": "namespace",
        "Name": "datetime",
        "Category": "Basic Data Types",
        "Description": "With a datetime object, you can return the current date and time and set a specific date and time. Please notice that you do not set the date and time of your device but only of the object in the script.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "datetime",
        "Description": "Returns an instance of a new datetime object that contains the current date and time by default.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set",
        "BelongsToNamespace": "datetime",
        "Description": "Sets the date and time of the object.",
        "Arguments": [
            { "Name": "year", "Description": "The year (e.g. 2023)", "DataType": "number", "DefaultValue": "2022" },
            { "Name": "month", "Description": "The month between 1 and 12", "DataType": "number", "DefaultValue": "5" },
            { "Name": "day", "Description": "The day of the month", "DataType": "number", "DefaultValue": "1" },
            { "Name": "hour", "Description": "The hour between 0 and 23", "DataType": "number", "DefaultValue": "16" },
            { "Name": "minute", "Description": "The minute between 0 and 59", "DataType": "number", "DefaultValue": "10" },
            { "Name": "second", "Description": "The second between 0 and 59", "DataType": "number", "DefaultValue": "0" },
            { "Name": "millisecond", "Description": "The millisecond between 0 and 999", "DataType": "number", "DefaultValue": "0" },
        ]
    },
    {
        "Type": "function",
        "Name": "get year",
        "BelongsToNamespace": "datetime",
        "Description": "Returns the year that is stored in the object. By default, it is the current year.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get month",
        "BelongsToNamespace": "datetime",
        "Description": "Returns the month that is stored in the object. By default, it is the current month.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get day",
        "BelongsToNamespace": "datetime",
        "Description": "Returns the day of the month that is stored in the object. By default, it is the current day.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get hours",
        "BelongsToNamespace": "datetime",
        "Description": "Returns the hours that are stored in the object. By default, these are the current hours.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get minutes",
        "BelongsToNamespace": "datetime",
        "Description": "Returns the minutes that are stored in the object. By default, these are the current minutes.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get seconds",
        "BelongsToNamespace": "datetime",
        "Description": "Returns the seconds that are stored in the object. By default, these are the current seconds.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get milliseconds",
        "BelongsToNamespace": "datetime",
        "Description": "Returns the milliseconds that are stored in the object. By default, these are the current milliseconds.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get date string",
        "BelongsToNamespace": "datetime",
        "Description": "Returns the date part as a local string. The date format depends on the current location settings.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "get time string",
        "BelongsToNamespace": "datetime",
        "Description": "Returns the time part as a local string. The time format depends on the current location settings.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "namespace",
        "Name": "number",
        "Category": "Basic Data Types",
        "Description": "A number can be an integer or a decimal number. You can calculate with numbers and use them for counting. The <a href=\"#math\">math</a> namespace contains commands that are mathematical functions that will all return an integer or decimal number.",
    },
    {
        "Type": "method",
        "Name": "add",
        "BelongsToNamespace": "number",
        "Description": "Adds a value to the number.",
        "Arguments": [
            { "Name": "value", "Description": "Value to add.", "DataType": "number", "DefaultValue": "1" }
        ]
    },
    {
        "Type": "method",
        "Name": "subtract",
        "BelongsToNamespace": "number",
        "Description": "Subtracts a value from the number.",
        "Arguments": [
            { "Name": "value", "Description": "Value to subtract.", "DataType": "number", "DefaultValue": "1" }
        ]
    },
    {
        "Type": "method",
        "Name": "multiply",
        "BelongsToNamespace": "number",
        "Description": "Multiplies the number with a value.",
        "Arguments": [
            { "Name": "value", "Description": "Value to multiply.", "DataType": "number", "DefaultValue": "1" }
        ]
    },
    {
        "Type": "method",
        "Name": "divide",
        "BelongsToNamespace": "number",
        "Description": "Divides the number by a value.",
        "Arguments": [
            { "Name": "value", "Description": "Value to divide.", "DataType": "number", "DefaultValue": "1" }
        ]
    },
    {
        "Type": "method",
        "Name": "set",
        "BelongsToNamespace": "number",
        "Description": "Sets the number to a value.",
        "Arguments": [
            { "Name": "value", "Description": "Value to set.", "DataType": "number", "DefaultValue": "1" }
        ]
    },
    {
        "Type": "namespace",
        "Name": "string",
        "Category": "Basic Data Types",
        "Description": "The string datatype contains plain text. You can not calculate with a string, but join strings together.<br><br>Use \"\\n\" to make a line feed. For example:<br>\"Hello world,\\nThis is the second line of my string.\\nAnd this is the third line.\"",
    },
    {
        "Type": "function",
        "Name": "to lowercase",
        "BelongsToNamespace": "string",
        "Description": "Returns a string with lowercase characters.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "to uppercase",
        "BelongsToNamespace": "string",
        "Description": "Returns a string with uppercase characters.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "substring",
        "BelongsToNamespace": "string",
        "Description": "Returns a string which is a part of another string.",
        "Arguments": [
            { "Name": "start", "Description": "The index where the part of the string should start. 0 is the first character, 1 the second etc.", "DataType": "number", "DefaultValue": "0" },
            { "Name": "length", "Description": "The number of characters that should be added to the string part.", "DataType": "number", "DefaultValue": "3" },
        ],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "replace",
        "BelongsToNamespace": "string",
        "Description": "Replaces all occurrences of a value within a string by another value.",
        "Arguments": [
            { "Name": "old", "Description": "The value that should be replaced.", "DataType": "string", "DefaultValue": "old value" },
            { "Name": "new", "Description": "The replacement value.", "DataType": "string", "DefaultValue": "new value" },
        ],
        "ReturnType": "string"
    },
    {
        "Type": "method",
        "Name": "append",
        "BelongsToNamespace": "string",
        "Description": "Adds some text to the string.",
        "Arguments": [
            { "Name": "text", "Description": "The text that will be added to the end of the string.", "DataType": "string", "DefaultValue": "Hello world" }
        ]
    },
    {
        "Type": "method",
        "Name": "set",
        "BelongsToNamespace": "string",
        "Description": "Sets a new text for the string.",
        "Arguments": [
            { "Name": "text", "Description": "The text that will be set for the string.", "DataType": "string", "DefaultValue": "Hello world" }
        ]
    },
    {
        "Type": "method",
        "Name": "clear",
        "BelongsToNamespace": "string",
        "Description": "Sets the string to empty and clears the content of it.",
        "Arguments": []
    },
    {
        "Type": "function",
        "Name": "get length",
        "BelongsToNamespace": "string",
        "Description": "Returns the number of chars of the string.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "split",
        "BelongsToNamespace": "string",
        "Description": "Splits up the string by a specific delimiter and returns the parts as a string collection.",
        "Arguments": [
            { "Name": "delimiter", "Description": "The char that should be used as the delimiter.", "DataType": "string", "DefaultValue": "," }
        ],
        "ReturnType": "collection"
    },
    {
        "Type": "function",
        "Name": "contains",
        "BelongsToNamespace": "string",
        "Description": "Returns true if text is part of the string, otherwise false.",
        "Arguments": [
            { "Name": "text", "Description": "String value that should be checked.", "DataType": "string", "DefaultValue": "search" }
        ],
        "ReturnType": "boolean"
    },
    {
        "Type": "function",
        "Name": "starts with",
        "BelongsToNamespace": "string",
        "Description": "Returns true if the string starts with a specific text, otherwise false.",
        "Arguments": [
            { "Name": "text", "Description": "String value to check.", "DataType": "string", "DefaultValue": "string" }
        ],
        "ReturnType": "boolean"
    },
    {
        "Type": "function",
        "Name": "ends with",
        "BelongsToNamespace": "string",
        "Description": "Returns true if the string ends with a specific text, otherwise false.",
        "Arguments": [
            { "Name": "text", "Description": "String value to check.", "DataType": "string", "DefaultValue": "string" }
        ],
        "ReturnType": "boolean"
    },
    {
        "Type": "function",
        "Name": "to object",
        "BelongsToNamespace": "string",
        "Description": "Returns an object that can me made out of the JSON. The resulting object can be a class object or a list. Please note that the object's structure has to match with the JSON.",
        "Arguments": [],
        "ReturnType": "object"
    },
    {
        "Type": "method",
        "Name": "download",
        "BelongsToNamespace": "string",
        "Description": "Downloads the string contents as a file to the file storage of the device.",
        "Arguments": [
            { "Name": "filename", "Description": "The name for the file.", "DataType": "string", "DefaultValue": "text.txt" },
        ]
    },
    {
        "Type": "namespace",
        "Name": "collection",
        "Category": "Lists and dictionaries",
        "Description": "A collection can contain a list of string values. You can iterate through the items of a collection (e.g. with a <a href=\"#foreach\">foreach</a> loop), add and remove items and ask for specific items.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "collection",
        "Description": "Returns the instance of a new, empty collection.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "clear",
        "BelongsToNamespace": "collection",
        "Description": "Clears the collection which means that all items will be removed.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "add",
        "BelongsToNamespace": "collection",
        "Description": "Adds a new item to the collection.",
        "Arguments": [
            { "Name": "item", "Description": "A string that should be added to the collection. This can also be a statement containing string variables.", "DataType": "string", "DefaultValue": "item" }
        ]
    },
    {
        "Type": "method",
        "Name": "insert",
        "BelongsToNamespace": "collection",
        "Description": "Inserts an item at a specific position in the collection.",
        "Arguments": [
            { "Name": "index", "Description": "The number of the position where the item should be inserted. 0 is the first position.", "DataType": "number", "DefaultValue": "0" },
            { "Name": "item", "Description": "A string that should be inserted into the collection. This can also be a statement containing string variables.", "DataType": "string", "DefaultValue": "item" }
        ]
    },
    {
        "Type": "method",
        "Name": "remove",
        "BelongsToNamespace": "collection",
        "Description": "Removes an item from the collection.",
        "Arguments": [
            { "Name": "index", "Description": "The position that should be removed from the collection where 0 is the first item, 1 the second etc.", "DataType": "number", "DefaultValue": "0" }
        ]
    },
    {
        "Type": "function",
        "Name": "get by index",
        "BelongsToNamespace": "collection",
        "Description": "Returns the item (as string) from a specific position of the collection.",
        "Arguments": [
            { "Name": "index", "Description": "The position whose element has to be returned. 0 is the first item, 1 the second etc.", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "get length",
        "BelongsToNamespace": "collection",
        "Description": "Returns the number of items in the collection.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get first",
        "BelongsToNamespace": "collection",
        "Description": "Returns the first item in the collection or an empty string if the collection is empty.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "get last",
        "BelongsToNamespace": "collection",
        "Description": "Returns the last item in the collection or an empty string if the collection is empty.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "namespace",
        "Name": "dictionary",
        "Category": "Lists and dictionaries",
        "Description": "A dictionary can contain a list of key/value-pairs which can be accessed by their key. You can directly pick a value by key or iterate through the keys (e.g. with a <a href=\"#foreach\">foreach</a> loop).",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "dictionary",
        "Description": "Returns the instance of a new, empty dictionary.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "clear",
        "BelongsToNamespace": "dictionary",
        "Description": "Clears the dictionary which means that all items will be removed.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "add",
        "BelongsToNamespace": "dictionary",
        "Description": "Adds a new item to the dictionary.",
        "Arguments": [
            { "Name": "key", "Description": "A unique key for this item.", "DataType": "string", "DefaultValue": "key" },
            { "Name": "value", "Description": "A value for this item which can be any text or variable.", "DataType": "string", "DefaultValue": "value" }
        ]
    },
    {
        "Type": "method",
        "Name": "add object",
        "BelongsToNamespace": "dictionary",
        "Description": "Adds a new item to the dictionary.",
        "Arguments": [
            { "Name": "key", "Description": "A unique key for this item.", "DataType": "string", "DefaultValue": "key" },
            { "Name": "item", "Description": "An object that can be any type of data or a list.", "DataType": "object", "DefaultValue": "item" }
        ]
    },
    {
        "Type": "method",
        "Name": "remove",
        "BelongsToNamespace": "dictionary",
        "Description": "Removes an item from the dictionary.",
        "Arguments": [
            { "Name": "key", "Description": "The key of the item which should be removed from the dictionary.", "DataType": "string", "DefaultValue": "key" }
        ]
    },
    {
        "Type": "function",
        "Name": "get by key",
        "BelongsToNamespace": "dictionary",
        "Description": "Returns the item (as string) with the specific key.",
        "Arguments": [
            { "Name": "key", "Description": "The key whose element has to be returned.", "DataType": "string", "DefaultValue": "key" }
        ],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "get object by key",
        "BelongsToNamespace": "dictionary",
        "Description": "Returns the item (as object) with the specific key.",
        "Arguments": [
            { "Name": "key", "Description": "The key whose element has to be returned.", "DataType": "string", "DefaultValue": "key" }
        ],
        "ReturnType": "object"
    },
    {
        "Type": "function",
        "Name": "get length",
        "BelongsToNamespace": "dictionary",
        "Description": "Returns the number of items in the dictionary.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get keys",
        "BelongsToNamespace": "dictionary",
        "Description": "Returns the keys of the dictionary as a collection which you can iterate.",
        "Arguments": [],
        "ReturnType": "collection"
    },
    {
        "Type": "namespace",
        "Name": "list",
        "Category": "Lists and dictionaries",
        "Description": "A list can contain a list of objects that are defined by classes. You can iterate through the items of a list (e.g. with a <a href=\"#foreach\">foreach</a> loop), add and remove items and ask for specific items.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "list",
        "Description": "Returns the instance of a new, empty list.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "clear",
        "BelongsToNamespace": "list",
        "Description": "Clears the list which means that all items will be removed.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "add",
        "BelongsToNamespace": "list",
        "Description": "Adds a new item to the list.",
        "Arguments": [
            { "Name": "item", "Description": "A class object that should be added to the list.", "DataType": "object", "DefaultValue": "item" }
        ]
    },
    {
        "Type": "method",
        "Name": "insert",
        "BelongsToNamespace": "list",
        "Description": "Inserts an item at a specific position in the list.",
        "Arguments": [
            { "Name": "index", "Description": "The number of the position where the item should be inserted. 0 is the first position.", "DataType": "number", "DefaultValue": "0" },
            { "Name": "item", "Description": "A class object that should be inserted into the list.", "DataType": "object", "DefaultValue": "item" }
        ]
    },
    {
        "Type": "method",
        "Name": "remove",
        "BelongsToNamespace": "list",
        "Description": "Removes an item from the list.",
        "Arguments": [
            { "Name": "index", "Description": "The position that should be removed from the list where 0 is the first item, 1 the second etc.", "DataType": "number", "DefaultValue": "0" }
        ]
    },
    {
        "Type": "function",
        "Name": "get by index",
        "BelongsToNamespace": "list",
        "Description": "Returns the item from a specific position of the list. Be sure that you create the object variable first so that 'get by index' knows which class you'll use.",
        "Arguments": [
            { "Name": "index", "Description": "The position whose element has to be returned. 0 is the first item, 1 the second etc.", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "object"
    },
    {
        "Type": "function",
        "Name": "get length",
        "BelongsToNamespace": "list",
        "Description": "Returns the number of items in the object list.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get first",
        "BelongsToNamespace": "list",
        "Description": "Returns the first item in the list or an empty string if the collection is empty.<br><b>Attention:</b> Be sure that the list contains element before you rely on 'get first'. If the list is empty, you don't get back a valid object. If you check the properties, the script will throw an ",
        "Arguments": [],
        "ReturnType": "object"
    },
    {
        "Type": "function",
        "Name": "get last",
        "BelongsToNamespace": "list",
        "Description": "Returns the last item in the list or an empty string if the collection is empty.<br><b>Attention:</b> Be sure that the list contains element before you rely on 'get last'. If the list is empty, you don't get back a valid object. If you check the properties, the script will throw an exception.",
        "Arguments": [],
        "ReturnType": "object"
    },
    {
        "Type": "function",
        "Name": "to string",
        "BelongsToNamespace": "list",
        "Description": "Returns the list as a JSON string.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "namespace",
        "Name": "button",
        "Category": "GUI Controls",
        "Description": "A button can be used to react if the user clicks on it. You have a more flexible user interface and you can present as much buttons as you want."
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "button",
        "Description": "Returns a new instance of a button.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set text",
        "BelongsToNamespace": "button",
        "Description": "Sets the text on a button.",
        "Arguments": [
            { "Name": "text", "Description": "The text that should be displayed on the button.", "DataType": "string", "DefaultValue": "Click me" }
        ]
    },
    {
        "Type": "method",
        "Name": "show",
        "BelongsToNamespace": "button",
        "Description": "Shows the button on the page.",
        "Arguments": []
    },
    {
        "Type": "function",
        "Name": "is clicked",
        "BelongsToNamespace": "button",
        "Description": "Returns true if the button has been clicked by the user, otherwise false. Use this function inside a loop to permanently check if the button has been clicked.",
        "Arguments": [],
        "ReturnType": "boolean"
    },
    {
        "Type": "namespace",
        "Name": "buttonbar",
        "Category": "GUI Controls",
        "Description": "A buttonbar represents a horizontal list of buttons that can be used to perform actions.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "buttonbar",
        "Description": "Returns a new instance of a buttonbar.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "add",
        "BelongsToNamespace": "buttonbar",
        "Description": "Adds a new button to the buttonbar.",
        "Arguments": [
            { "Name": "text", "Description": "The text of the button.", "DataType": "string", "DefaultValue": "Click me" }
        ]
    },
    {
        "Type": "method",
        "Name": "show",
        "BelongsToNamespace": "buttonbar",
        "Description": "Shows the buttonbar on the screen. If the bar does not fit to the width of the screen, it will be wrapped.",
        "Arguments": []
    },
    {
        "Type": "function",
        "Name": "clicked index",
        "BelongsToNamespace": "buttonbar",
        "Description": "Returns the index of the last clicked button in the buttonbar where 0 is the first button, 1 the second etc. If no button has been clicked, it will return -1.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "clicked name",
        "BelongsToNamespace": "buttonbar",
        "Description": "Returns the name of the last clicked button in the buttonbar. If no button has been clicked, it will return an empty string.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "namespace",
        "Name": "check box",
        "Category": "GUI Controls",
        "Description": "A check box presents a field which can be checked or unchecked.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "check box",
        "Description": "Returns a new instance of a check box control.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set text",
        "BelongsToNamespace": "check box",
        "Description": "Sets a text which will be shown near the check box. It should explain what the check box is for.",
        "Arguments": [
            { "Name": "text", "Description": "The text that should be displayed near the check box.", "DataType": "string", "DefaultValue": "Hello world" }
        ]
    },
    {
        "Type": "method",
        "Name": "set value",
        "BelongsToNamespace": "check box",
        "Description": "Sets a boolean value for the check box.",
        "Arguments": [
            { "Name": "value", "Description": "A boolean value. true means that the check box is checked, false means unchecked.", "DataType": "boolean", "DefaultValue": "true" }
        ]
    },
    {
        "Type": "function",
        "Name": "get value",
        "BelongsToNamespace": "check box",
        "Description": "Returns the value (true/false) of the check box.",
        "Arguments": [],
        "ReturnType": "boolean"
    },
    {
        "Type": "method",
        "Name": "show",
        "BelongsToNamespace": "check box",
        "Description": "Shows the check box control on the page.",
        "Arguments": []
    },
    {
        "Type": "namespace",
        "Name": "data grid",
        "Category": "GUI Controls",
        "Description": "A data grid displays the content of an object list as a human-readable grid on the page. It parses all properties automatically and uses the property names as column headers.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "data grid",
        "Description": "Returns a new instance of a data grid.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set column count",
        "BelongsToNamespace": "data grid",
        "Description": "Sets the number of columns which should be displayed in the data grid. If the number is 0 or if this command is not used, all columns will be shown.",
        "Arguments": [
            { "Name": "count", "Description": "The number of columns that should be displayed in the data grid or 0 for all columns.", "DataType": "number", "DefaultValue": "3" }
        ]
    },
    {
        "Type": "function",
        "Name": "selected row",
        "BelongsToNamespace": "data grid",
        "Description": "Returns the index of the row that has been clicked or tapped. If no row has been selected, the return value will be -1. Otherwise, the return value is zero-based, so the first row is returned as 0, the second as 1 etc.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "selected cell",
        "BelongsToNamespace": "data grid",
        "Description": "Returns the index of the cell that has been clicked or tapped. If no cell has been selected, the return value will be -1. Otherwise, the return value is zero-based, so the first cell is returned as 0, the second as 1 etc.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "method",
        "Name": "show",
        "BelongsToNamespace": "data grid",
        "Description": "Shows the data grid on the page.",
        "Arguments": [
            { "Name": "object list", "Description": "The list of objects that should be used as the data source for the grid.", "DataType": "object", "DefaultValue": "item list" }
        ]
    },
    {
        "Type": "namespace",
        "Name": "number input",
        "Category": "GUI Controls",
        "Description": "A number input presents a field where the user can enter numbers only.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "number input",
        "Description": "Returns a new instance of a number input field.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set placeholder",
        "BelongsToNamespace": "number input",
        "Description": "Sets a helping text that will appear if nothing has been entered into the number input line. It should be an indication of what the user should enter here, e.g.: \"Enter your age\".",
        "Arguments": [
            { "Name": "text", "Description": "The text that should be displayed as a placeholder for the input line.", "DataType": "string", "DefaultValue": "Input value" }
        ]
    },
    {
        "Type": "method",
        "Name": "set value",
        "BelongsToNamespace": "number input",
        "Description": "Sets the value for the number input field. This should be a number.",
        "Arguments": [
            { "Name": "value", "Description": "The value for the number input line.", "DataType": "number", "DefaultValue": "0" }
        ]
    },
    {
        "Type": "function",
        "Name": "get value",
        "BelongsToNamespace": "number input",
        "Description": "Returns the value that has been entered into the number input line.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "method",
        "Name": "show",
        "BelongsToNamespace": "number input",
        "Description": "Shows the number input field on the page.",
        "Arguments": []
    },
    {
        "Type": "namespace",
        "Name": "text box",
        "Category": "GUI Controls",
        "Description": "A text box is a simple field which can contain some text.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "text box",
        "Description": "Returns a new instance of a text box.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set text",
        "BelongsToNamespace": "text box",
        "Description": "Sets the text of the text box.",
        "Arguments": [
            { "Name": "text", "Description": "The text that should be displayed in the text box. If the text box is already visible and contains some text, it will be replaced.", "DataType": "string", "DefaultValue": "Hello world" }
        ]
    },
    {
        "Type": "method",
        "Name": "show",
        "BelongsToNamespace": "text box",
        "Description": "Shows the text box on the page.",
        "Arguments": []
    },
    {
        "Type": "namespace",
        "Name": "text input",
        "Category": "GUI Controls",
        "Description": "A text input presents a field where the user can enter some text.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "text input",
        "Description": "Returns a new instance of a text input field.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set placeholder",
        "BelongsToNamespace": "text input",
        "Description": "Sets a helping text that will appear if nothing has been entered into the text input line. It should be an indication of what the user should enter here, e.g.: \"Enter your first name\".",
        "Arguments": [
            { "Name": "text", "Description": "The text that should be displayed as a placeholder for the input line.", "DataType": "string", "DefaultValue": "Input value" }
        ]
    },
    {
        "Type": "method",
        "Name": "set value",
        "BelongsToNamespace": "text input",
        "Description": "Sets the value for the text input field. This should be a string.",
        "Arguments": [
            { "Name": "text", "Description": "The value for the text input line.", "DataType": "string", "DefaultValue": "value" }
        ]
    },
    {
        "Type": "function",
        "Name": "get value",
        "BelongsToNamespace": "text input",
        "Description": "Returns the value that has been entered into the text input line.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "method",
        "Name": "show",
        "BelongsToNamespace": "text input",
        "Description": "Shows the text input field on the page.",
        "Arguments": []
    },
    {
        "Type": "namespace",
        "Name": "app",
        "Category": "Other namespaces",
        "Description": "The app namespace contains commands that can interact with the app itself.",
    },
    {
        "Type": "method",
        "Name": "restart",
        "BelongsToNamespace": "app",
        "Description": "Restarts the app.",
        "Arguments": []
    },
    {
        "Type": "function",
        "Name": "duration",
        "BelongsToNamespace": "app",
        "Description": "Returns the number of seconds the app is already running as a decimal number.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "namespace",
        "Name": "device",
        "Category": "Other namespaces",
        "Description": "The device namespace contains device-specific commands. Using these commands may require granting permission of the system.<br><br><b>Please note:</b> If you are using geo location, the following resources will be loaded inside your script to enable map features:<ul><li>https://openlayers.org/en/v4.6.5/css/ol.css<li>https://openlayers.org/en/v4.6.5/build/ol.js</ul>",
    },
    {
        "Type": "function",
        "Name": "get orientation",
        "BelongsToNamespace": "device",
        "Description": "Returns the orientation of the device. Possible values are: 'portrait', 'landscape'.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "scan code",
        "BelongsToNamespace": "device",
        "Description": "Opens a QR- and barcode scanner and returns the scanned value as string.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "method",
        "Name": "send email",
        "BelongsToNamespace": "device",
        "Description": "Sends an e-mail with the default e-mail-client on the phone.",
        "Arguments": [
            { "Name": "email", "Description": "E-Mail address", "DataType": "string", "DefaultValue": "email" },
            { "Name": "subject", "Description": "E-Mail subject", "DataType": "string", "DefaultValue": "subject" },
            { "Name": "body", "Description": "E-Mail text. Use \"\\n\" for line breaks within the string.", "DataType": "string", "DefaultValue": "text" }
        ]
    },
    {
        "Type": "method",
        "Name": "call",
        "BelongsToNamespace": "device",
        "Description": "Calls a phone number.",
        "Arguments": [
            { "Name": "number", "Description": "The phone number. On some devices, you have to set the country code (e.g. +49 ...) before the number.", "DataType": "string", "DefaultValue": "number" },
        ]
    },
    {
        "Type": "method",
        "Name": "open url",
        "BelongsToNamespace": "device",
        "Description": "Opens a url in the default web browser of the device.",
        "Arguments": [
            { "Name": "url", "Description": "The url that should be opened in the browser.", "DataType": "string", "DefaultValue": "https://touchide.net" },
        ]
    },
    {
        "Type": "function",
        "Name": "get language",
        "BelongsToNamespace": "device",
        "Description": "Returns the current language (e.g. en, de, fr...).",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "get geolocation",
        "BelongsToNamespace": "device",
        "Description": "Returns an object that contains the current location. Using this command requires granting permission to location. See next section for the returning location object.",
        "Arguments": [],
        "ReturnType": "geolocation"
    },
    {
        "Type": "namespace",
        "Name": "geolocation",
        "Category": "Other namespaces",
        "Description": "The geolocation object is returned after 'device' -> 'get geolocation'. It contains the following members:",
    },
    {
        "Type": "function",
        "Name": "latitude",
        "BelongsToNamespace": "geolocation",
        "Description": "Returns the latitude as a decimal number.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "longitude",
        "BelongsToNamespace": "geolocation",
        "Description": "Returns the longitude as a decimal number.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "accuracy",
        "BelongsToNamespace": "geolocation",
        "Description": "Returns the accuracy of the position.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "altitude",
        "BelongsToNamespace": "geolocation",
        "Description": "Returns the altitude in meters above the mean sea level (if available).",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "altitude accuracy",
        "BelongsToNamespace": "geolocation",
        "Description": "Returns the altitude accuracy of position (if available).",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "heading",
        "BelongsToNamespace": "geolocation",
        "Description": "Returns the heading as degrees clockwise from north (if available).",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "speed",
        "BelongsToNamespace": "geolocation",
        "Description": "Returns the speed in meters per second (if available).",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "method",
        "Name": "update geolocation",
        "BelongsToNamespace": "geolocation",
        "Description": "Updates the geolocation object with the current position.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "activate tracking",
        "BelongsToNamespace": "geolocation",
        "Description": "Activates tracking mode, so the geo coordinates are fetched permanently in the background. Use 'update geolocation' to get these coordinates and to update the current object.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "show map",
        "BelongsToNamespace": "geolocation",
        "Description": "Shows a map on the page with the current position marked. Data have to be fetched online from openstreetmap to use this feature.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "update map",
        "BelongsToNamespace": "geolocation",
        "Description": "Updates an already shown map by displaying the most current coordinates.",
        "Arguments": []
    },
    {
        "Type": "namespace",
        "Name": "image",
        "Category": "Other namespaces",
        "Description": "With the image namespace, you can access and load images.",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "image",
        "Description": "Returns an instance of a new, empty image.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "from url",
        "BelongsToNamespace": "image",
        "Description": "Loads an image from a specific url. Please keep in mind that not every url is working because off CORS. CORS is a security feature that allowes fetching web resources only from specific domains.",
        "Arguments": [
            { "Name": "url", "Description": "A url that points to an image (e.g. a jpg or png file).", "DataType": "string", "DefaultValue": "https://..." },
        ]
    },
    {
        "Type": "method",
        "Name": "from camera",
        "BelongsToNamespace": "image",
        "Description": "Opens the camera of the device and allows to take a picture that will be stored in the image object. You may grant access to the camera to use this feature.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "from file",
        "BelongsToNamespace": "image",
        "Description": "Allows to select an image file from the device.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "qrcode",
        "BelongsToNamespace": "image",
        "Description": "Creates a QR code with the given text. Use 'show' to show it on the page.",
        "Arguments": [
            { "Name": "text", "Description": "The text for the QR code.", "DataType": "string", "DefaultValue": "https://touchide.net" },
        ]
    },
    {
        "Type": "method",
        "Name": "show",
        "BelongsToNamespace": "image",
        "Description": "Shows the currently stored image on the screen.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "show at",
        "BelongsToNamespace": "image",
        "Description": "Shows the currently stored image on the screen at a specified position.",
        "Arguments": [
            { "Name": "x", "Description": "The x coordinate where the image should be positioned.", "DataType": "number", "DefaultValue": "0" },
            { "Name": "y", "Description": "The y coordinate where the image should be positioned.", "DataType": "number", "DefaultValue": "0" }
        ]
    },
    {
        "Type": "method",
        "Name": "move to",
        "BelongsToNamespace": "image",
        "Description": "Moves the currently stored image to a new position on the screen.",
        "Arguments": [
            { "Name": "x", "Description": "The x coordinate where the image should be positioned.", "DataType": "number", "DefaultValue": "0" },
            { "Name": "y", "Description": "The y coordinate where the image should be positioned.", "DataType": "number", "DefaultValue": "0" }
        ]
    },
    {
        "Type": "method",
        "Name": "download",
        "BelongsToNamespace": "image",
        "Description": "Downloads the currently stored image to the file storage of the device.",
        "Arguments": [
            { "Name": "filename", "Description": "The name for the image file.", "DataType": "string", "DefaultValue": "image.png" },
        ]
    },
    {
        "Type": "method",
        "Name": "zoom",
        "BelongsToNamespace": "image",
        "Description": "Sets the zoom factor of the image to a given percentage where 100% is the original size.",
        "Arguments": [
            { "Name": "percent", "Description": "The zoom factor in percent for the image.", "DataType": "number", "DefaultValue": "50" },
        ]
    },
    {
        "Type": "method",
        "Name": "resize",
        "BelongsToNamespace": "image",
        "Description": "Resizes the image to a new width and height given in pixels.",
        "Arguments": [
            { "Name": "width", "Description": "The new width for the image in pixels.", "DataType": "number", "DefaultValue": "75" },
            { "Name": "height", "Description": "The new height for the image in pixels.", "DataType": "number", "DefaultValue": "20" },
        ]
    },
    {
        "Type": "function",
        "Name": "is pointer down",
        "BelongsToNamespace": "image",
        "Description": "Returns true if a mouse button is pressed over the image or if the finger is placed on the image.",
        "Arguments": [],
        "ReturnType": "boolean"
    },
    {
        "Type": "function",
        "Name": "left",
        "BelongsToNamespace": "image",
        "Description": "Returns the left position (x) of the currently stored image in pixels or 0 if no image is loaded. This function returns a value only when the image has been shown with the 'show at' command.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "top",
        "BelongsToNamespace": "image",
        "Description": "Returns the top position (y) of the currently stored image in pixels or 0 if no image is loaded. This function returns a value only when the image has been shown with the 'show at' command.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "width",
        "BelongsToNamespace": "image",
        "Description": "Returns the width of the currently stored image in pixels or 0 if no image is loaded.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "height",
        "BelongsToNamespace": "image",
        "Description": "Returns the height of the currently stored image in pixels or 0 if no image is loaded.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "namespace",
        "Name": "json",
        "Category": "Other namespaces",
        "Description": "The json namespace contains commands to parse json strings and to convert existing objects to json strings. This is very useful if you want to consume data from a REST service.",
    },
    {
        "Type": "function",
        "Name": "parse",
        "BelongsToNamespace": "json",
        "Description": "Parses a json string and returns an object. Be sure that the object represents the structure of the json string.",
        "Arguments": [
            { "Name": "string", "Description": "The json string.", "DataType": "string", "DefaultValue": "value" }
        ],
        "ReturnType": "object"
    },
    {
        "Type": "function",
        "Name": "to string",
        "BelongsToNamespace": "json",
        "Description": "Returns a json string from an object that can be a single object from a class or a list.",
        "Arguments": [
            { "Name": "item", "Description": "The object or the list that should be returned as a json string.", "DataType": "object", "DefaultValue": "item" }
        ],
        "ReturnType": "string"
    },
    {
        "Type": "namespace",
        "Name": "math",
        "Category": "Other namespaces",
        "Description": "The math namespace contains all math functions known from JavaScript like acos, asin, random, round. All members of the math namespace are functions that will return a number variable.",
    },
    {
        "Type": "function",
        "Name": "abs",
        "BelongsToNamespace": "math",
        "Description": "Returns the absolute value of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "acos",
        "BelongsToNamespace": "math",
        "Description": "Returns the arccosine of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "acosh",
        "BelongsToNamespace": "math",
        "Description": "Returns the hyperbolic arccosine of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "asin",
        "BelongsToNamespace": "math",
        "Description": "Returns the arcsine of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "asinh",
        "BelongsToNamespace": "math",
        "Description": "Returns the hyperbolic arcsine of a number.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "atan",
        "BelongsToNamespace": "math",
        "Description": "Returns the arctangent of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "atan2",
        "BelongsToNamespace": "math",
        "Description": "Returns the arctangent of the quotient of its arguments.",
        "Arguments": [
            { "Name": "y", "Description": "", "DataType": "number", "DefaultValue": "0" },
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "atanh",
        "BelongsToNamespace": "math",
        "Description": "Returns the hyperbolic arctangent of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "cbrt",
        "BelongsToNamespace": "math",
        "Description": "Returns the cube root of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "ceil",
        "BelongsToNamespace": "math",
        "Description": "Returns the smallest integer greater than or equal to x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "clz32",
        "BelongsToNamespace": "math",
        "Description": "Returns the number of leading zero bits of the 32-bit integer x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "cos",
        "BelongsToNamespace": "math",
        "Description": "Returns the cosine of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "cosh",
        "BelongsToNamespace": "math",
        "Description": "Returns the hyperbolic cosine of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "exp",
        "BelongsToNamespace": "math",
        "Description": "Returns e^x, where x is the argument, and e is Euler's constant  (2.718…, the base of the natural logarithm).",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "expm1",
        "BelongsToNamespace": "math",
        "Description": "Returns subtracting 1 from exp (x).",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "floor",
        "BelongsToNamespace": "math",
        "Description": "Returns the largest integer less than or equal to x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "fround",
        "BelongsToNamespace": "math",
        "Description": "Returns the nearest single precision float representation of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "hypot",
        "BelongsToNamespace": "math",
        "Description": "Returns the square root of the sum of squares of its arguments.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "imul",
        "BelongsToNamespace": "math",
        "Description": "Returns the result of the 32-bit integer multiplication of x and y.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" },
            { "Name": "y", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "log",
        "BelongsToNamespace": "math",
        "Description": "Returns the natural logarithm  (㏒e; also, ㏑) of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "log1p",
        "BelongsToNamespace": "math",
        "Description": "Returns the natural logarithm  (㏒e; also ㏑) of 1 + x for the number x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "log10",
        "BelongsToNamespace": "math",
        "Description": "Returns the base-10 logarithm of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "log2",
        "BelongsToNamespace": "math",
        "Description": "Returns the base-2 logarithm of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "max",
        "BelongsToNamespace": "math",
        "Description": "Returns the largest of two numbers.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" },
            { "Name": "y", "Description": "", "DataType": "number", "DefaultValue": "1" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "min",
        "BelongsToNamespace": "math",
        "Description": "Returns the smallest of two numbers.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" },
            { "Name": "y", "Description": "", "DataType": "number", "DefaultValue": "1" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "pow",
        "BelongsToNamespace": "math",
        "Description": "Returns base x to the exponent power y (that is, x^y).",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" },
            { "Name": "y", "Description": "", "DataType": "number", "DefaultValue": "1" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "random",
        "BelongsToNamespace": "math",
        "Description": "Returns a pseudo-random number between 0 and 1.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "round",
        "BelongsToNamespace": "math",
        "Description": "Returns the value of the number x rounded to the nearest integer.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "sign",
        "BelongsToNamespace": "math",
        "Description": "Returns the sign of the x, indicating whether x is positive, negative, or zero.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "sin",
        "BelongsToNamespace": "math",
        "Description": "Returns the sine of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "sinh",
        "BelongsToNamespace": "math",
        "Description": "Returns the hyperbolic sine of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "sqrt",
        "BelongsToNamespace": "math",
        "Description": "Returns the positive square root of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "tan",
        "BelongsToNamespace": "math",
        "Description": "Returns the tangent of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "tanh",
        "BelongsToNamespace": "math",
        "Description": "Returns the hyperbolic tangent of x.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "trunc",
        "BelongsToNamespace": "math",
        "Description": "Returns the integer portion of x, removing any fractional digits.",
        "Arguments": [
            { "Name": "x", "Description": "", "DataType": "number", "DefaultValue": "0" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "namespace",
        "Name": "page",
        "Category": "Other namespaces",
        "Description": "The page namespace contains commands for displaying data on the screen or to ask for data by the user."
    },
    {
        "Type": "method",
        "Name": "print",
        "BelongsToNamespace": "page",
        "Description": "Writes some text on the screen. The text can be a fixed string value or a statement containing of several string- and number variables.",
        "Arguments": [
            { "Name": "text", "Description": "The text to be displayed on the screen. This can be a composition of multiple values and variables joined together.", "DataType": "string", "DefaultValue": "Hello world" }
        ]
    },
    {
        "Type": "function",
        "Name": "get string",
        "BelongsToNamespace": "page",
        "Description": "Opens a dialog box which asks for a string value. It returns the string value entered by the user. The result will be a string variable.",
        "Arguments": [
            { "Name": "prompt", "Description": "A message that will appear in the dialog box to give the user some information (e.g. \"Please enter your name:\").", "DataType": "string", "DefaultValue": "Enter string:" }
        ],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "get number",
        "BelongsToNamespace": "page",
        "Description": "Opens a dialog box which asks for a numeric value. It returns the number entered by the user. The result will be a number variable.",
        "Arguments": [
            { "Name": "prompt", "Description": "A message that will appear in the dialog box to give the user some information (e.g. \"Please enter the quantity:\").", "DataType": "string", "DefaultValue": "Enter number:" }
        ],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "ask",
        "BelongsToNamespace": "page",
        "Description": "Opens a dialog box which shows a text and up to four buttons that can be clicked/tapped by the user. The function will return the name of the clicked button. Buttons which do not have a text (= empty string) will not be displayed.",
        "Arguments": [
            { "Name": "prompt", "Description": "A message that will appear in the dialog box to give the user some information (e.g. \"Please select an option:\").", "DataType": "string", "DefaultValue": "Select an option." },
            { "Name": "button1", "Description": "Text of the first button.", "DataType": "string", "DefaultValue": "Button 1" },
            { "Name": "button2", "Description": "Text of the second button.", "DataType": "string", "DefaultValue": "Button 2" },
            { "Name": "button3", "Description": "Text of the third button.", "DataType": "string", "DefaultValue": "Button 3" },
            { "Name": "button4", "Description": "Text of the fourth button.", "DataType": "string", "DefaultValue": "Button 4" }
        ],
        "ReturnType": "string"
    },
    {
        "Type": "method",
        "Name": "clear",
        "BelongsToNamespace": "page",
        "Description": "Clears the content of the page and makes it blank."
    },
    {
        "Type": "method",
        "Name": "set title",
        "BelongsToNamespace": "page",
        "Description": "Sets the text of the title bar of the script. The title bar appears on the top of the screen. If the title bar has been hidden by the 'hide title' command, it will get visible again.",
        "Arguments": [
            { "Name": "text", "Description": "The text that should appear in the title bar.", "DataType": "string", "DefaultValue": "Title text" }
        ]
    },
    {
        "Type": "method",
        "Name": "hide title",
        "BelongsToNamespace": "page",
        "Description": "Hides the title bar."
    },
    {
        "Type": "function",
        "Name": "get pointer x",
        "BelongsToNamespace": "page",
        "Description": "Returns a number that represents the x coordinate of the mouse pointer or the touched down finger on the screen.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "get pointer y",
        "BelongsToNamespace": "page",
        "Description": "Returns a number that represents the y coordinate of the mouse pointer or the touched down finger on the screen.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "is pointer down",
        "BelongsToNamespace": "page",
        "Description": "Returns true if either a mouse button is pressed down or the finger touches the touch screen of the device.",
        "Arguments": [],
        "ReturnType": "boolean"
    },
    {
        "Type": "function",
        "Name": "width",
        "BelongsToNamespace": "page",
        "Description": "Returns the width of the page in pixels.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "height",
        "BelongsToNamespace": "page",
        "Description": "Returns the height of the page in pixels.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "method",
        "Name": "add menu item",
        "BelongsToNamespace": "page",
        "Description": "Adds a new item to the app menu. The app menu appears as a hamburger menu in the upper left corner of your app if it has a minimum of one item. If the title bar has been hidden with the 'hide title' command, the app menu will not be shown.",
        "Arguments": [
            { "Name": "text", "Description": "The text for the new menu item.", "DataType": "string", "DefaultValue": "Item" }
        ]
    },
    {
        "Type": "function",
        "Name": "menu item index",
        "BelongsToNamespace": "page",
        "Description": "Returns the index of the last clicked app menu item where 0 is the first, 1 the second etc. If no menu item has been clicked, it will return -1.",
        "Arguments": [],
        "ReturnType": "number"
    },
    {
        "Type": "function",
        "Name": "menu item name",
        "BelongsToNamespace": "page",
        "Description": "Returns the name of the last clicked app menu item. If no menu item has been clicked, it will return an empty string.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "namespace",
        "Name": "storage",
        "Category": "Other namespaces",
        "Description": "The storage namespace contains commands to store and fetch data. Data stored via the storage namespace is retained even if the app is terminated and restarted. The storage can be filled, changed and cleared.",
    },
    {
        "Type": "method",
        "Name": "put",
        "BelongsToNamespace": "storage",
        "Description": "Stores data in the storage.",
        "Arguments": [
            { "Name": "name", "Description": "A name under which the data should be stored.", "DataType": "string", "DefaultValue": "name" },
            { "Name": "item", "Description": "The object that should be stored.", "DataType": "object", "DefaultValue": "item" }
        ]
    },
    {
        "Type": "function",
        "Name": "get",
        "BelongsToNamespace": "storage",
        "Description": "Returns data from the storage. If no data exists by the given name, then nothing will be returned.",
        "Arguments": [
            { "Name": "name", "Description": "The name of the stored object which should be returned.", "DataType": "string", "DefaultValue": "name" }
        ],
        "ReturnType": "object"
    },
    {
        "Type": "method",
        "Name": "remove",
        "BelongsToNamespace": "storage",
        "Description": "Removes stored data from the storage.",
        "Arguments": [
            { "Name": "name", "Description": "The name of the stored data which should be removed from storage.", "DataType": "string", "DefaultValue": "name" }
        ]
    },
    {
        "Type": "method",
        "Name": "upload",
        "BelongsToNamespace": "storage",
        "Description": "Uploads the contents of the local storage of the script to the cloud. See \"Cloud sync of the storage\" for more details.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "download",
        "BelongsToNamespace": "storage",
        "Description": "Downloads the contents for the local storage of the script from the cloud. If there is nothing, the local storage remains unchanged. See \"Cloud sync of the storage\" for more details.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set global mode",
        "BelongsToNamespace": "storage",
        "Description": "Sets the mode for storage cloud sync to global.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set session mode",
        "BelongsToNamespace": "storage",
        "Description": "Sets the mode for storage cloud sync to session.<br><br><h2>Cloud sync of the storage</h2><p>To use cloud synchronization of the storage, the following requirements must be met:</p><ul><li>You need an account on <a href=\"https://touchide.net\" target=\"_new\">touchide.net</a>.<li>You must enter your username and your app ID in the \"Cloud Sync\" section of Touch IDE.<li>The script must have cloud sync enabled (represented by the cloud icon on the tile).<li>If you use the script editor on <a href=\"https://touchide.net\" target=\"_new\">touchide.net</a> directly, the above conditions are met already.</ul><p>If the requirements are met, you can decide which synchronization mode you will use:</p><p><b>Global mode (default)</b><br>Global Mode means that the same script has access to the same data on different devices and in different browser sessions. So, the data storage is script-related, but cross-device and cross-browser.</p><p><b>Session mode</b><br>Session mode means that the same script has its own data store on different devices and in different browser sessions. Each device and browser session is given a unique Session ID, which is used internally to control the membership of the data.</p>",
        "Arguments": []
    },
    {
        "Type": "namespace",
        "Name": "web request",
        "Category": "Other namespaces",
        "Description": "With a web request, you can get the contents of a web resource, like a web site or a web service. Keep in mind that some resources are using CORS so you can't access them from within the app. CORS is a security feature that allows requests only from certain domains. You can get more information about CORS from this link: <a href=\"https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS\" target=\"_new\">What is CORS?</a>",
    },
    {
        "Type": "method",
        "Name": "create",
        "BelongsToNamespace": "web request",
        "Description": "Returns an instance of a new web request object with GET as the default method.",
        "Arguments": []
    },
    {
        "Type": "method",
        "Name": "set url",
        "BelongsToNamespace": "web request",
        "Description": "Sets the url of the web resource that you will access.",
        "Arguments": [
            { "Name": "url", "Description": "A valid url to a web site or a web service", "DataType": "string", "DefaultValue": "https://..." },
        ]
    },
    {
        "Type": "method",
        "Name": "set method",
        "BelongsToNamespace": "web request",
        "Description": "Sets the http method for the request which indicates the type of communication with the web resource. The most important valid methods are: GET, POST, PUT, DELETE. You may need another method, depending on the web resource.",
        "Arguments": [
            { "Name": "method", "Description": "A string that contains the http method (e.g. GET, POST, PUT, DELETE).", "DataType": "string", "DefaultValue": "GET" },
        ]
    },
    {
        "Type": "method",
        "Name": "set content type",
        "BelongsToNamespace": "web request",
        "Description": "Sets the content type of the http body you are going to send. The content-type depends on the data and what the web resource expects. For json data, you should mostly use \"application/json\".",
        "Arguments": [
            { "Name": "content type", "Description": "A string that contains the content type for the http request.", "DataType": "string", "DefaultValue": "application/json" },
        ]
    },
    {
        "Type": "method",
        "Name": "set body",
        "BelongsToNamespace": "web request",
        "Description": "Sets the body for the request. The body contains data that are sent mostly with a POST or PUT method.",
        "Arguments": [
            { "Name": "body", "Description": "A string that contains the body for the http request.", "DataType": "string", "DefaultValue": "some data to send" },
        ]
    },
    {
        "Type": "method",
        "Name": "add header",
        "BelongsToNamespace": "web request",
        "Description": "Adds a new http header to the request.",
        "Arguments": [
            { "Name": "name", "Description": "The name of the header", "DataType": "string", "DefaultValue": "Name" },
            { "Name": "value", "Description": "The value for the header", "DataType": "string", "DefaultValue": "Value" },
        ]
    },
    {
        "Type": "function",
        "Name": "get response",
        "BelongsToNamespace": "web request",
        "Description": "Starts the request to the web resource and returns a web response object.",
        "Arguments": [],
        "ReturnType": "web response"
    },
    {
        "Type": "namespace",
        "Name": "web response",
        "Category": "Other namespaces",
        "Description": "The web response contains information about the processed request that was called by 'web request' -> 'get response'.",
    },
    {
        "Type": "function",
        "Name": "status",
        "BelongsToNamespace": "web response",
        "Description": "Returns the http status of the response, e.g. 200 for success. For a list of http status codes, see the <a href=\"https://developer.mozilla.org/en-US/docs/Web/HTTP/Status\" target=\"_new\">status codes here</a>.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "status text",
        "BelongsToNamespace": "web response",
        "Description": "Returns the description of the http status as string.",
        "Arguments": [],
        "ReturnType": "string"
    },
    {
        "Type": "function",
        "Name": "response text",
        "BelongsToNamespace": "web response",
        "Description": "Returns the response text of the http request.",
        "Arguments": [],
        "ReturnType": "string"
    },
];