/*
 * Copyright (C) 2022  Daniel Schlieckmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Touch IDE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Morph.Web 0.1
import QtWebEngine 1.7
import Lomiri.DownloadManager 1.2
import Lomiri.Content 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'touchide.daniel'
    automaticOrientation: true

   PageStack {
        id: mainPageStack
        anchors.fill: parent
        Component.onCompleted: mainPageStack.push(pageMain)

        Page {
            id: pageMain
            anchors.fill: parent

            WebEngineView {
                id: mainWebView
                focus: true
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                settings {
                    javascriptCanAccessClipboard: true
                    localContentCanAccessFileUrls: true
                    localContentCanAccessRemoteUrls: true
                    allowRunningInsecureContent: true
                    allowWindowActivationFromJavaScript : true
                    pluginsEnabled: true
                }
                Component.onCompleted: {
                    settings.localStorageEnabled = true;
                }

                profile: WebEngineProfile {
                    id: webContext
                    httpUserAgent: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36"
                    storageName: "Storage"
                    persistentStoragePath: "/home/phablet/.local/share/touchide.daniel"

                    onDownloadRequested: {
                        var fileUrl = "/home/phablet/.local/share/touchide.daniel/" + download.downloadFileName;
                        var request = new XMLHttpRequest();
                        request.open("PUT", fileUrl, false);
                        var downloadUrl = decodeURIComponent(download.url.toString());
                        if (downloadUrl.startsWith("data:image/png;base64,")) {
                            var arr = base64DecToArr(downloadUrl.replace("data:image/png;base64,", ""), 512);
                            request.send(arr);
                        }
                        else if (downloadUrl.startsWith("data:image/jpeg;base64,")) {
                            var arr = base64DecToArr(downloadUrl.replace("data:image/jpeg;base64,", ""), 512);
                            request.send(arr);
                        }
                        else {
                            request.send(downloadUrl.replace("data:text/plain;,", ""));
                        }
                        fileExporter.file = fileUrl
                        PopupUtils.open(downloadedMessage);
                    }

                    function b64ToUint6(nChr) {
                        return nChr > 64 && nChr < 91
                            ? nChr - 65
                            : nChr > 96 && nChr < 123
                            ? nChr - 71
                            : nChr > 47 && nChr < 58
                            ? nChr + 4
                            : nChr === 43
                            ? 62
                            : nChr === 47
                            ? 63
                            : 0;
                    }

                    function base64DecToArr(sBase64, nBlocksSize) {
                        const sB64Enc = sBase64.replace(/[^A-Za-z0-9+/]/g, "");
                        const nInLen = sB64Enc.length;
                        const nOutLen = nBlocksSize
                            ? Math.ceil(((nInLen * 3 + 1) >> 2) / nBlocksSize) * nBlocksSize
                            : (nInLen * 3 + 1) >> 2;
                        const taBytes = new Uint8Array(nOutLen);

                        let nMod3;
                        let nMod4;
                        let nUint24 = 0;
                        let nOutIdx = 0;
                        for (let nInIdx = 0; nInIdx < nInLen; nInIdx++) {
                            nMod4 = nInIdx & 3;
                            nUint24 |= b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << (6 * (3 - nMod4));
                            if (nMod4 === 3 || nInLen - nInIdx === 1) {
                            nMod3 = 0;
                            while (nMod3 < 3 && nOutIdx < nOutLen) {
                                taBytes[nOutIdx] = (nUint24 >>> ((16 >>> nMod3) & 24)) & 255;
                                nMod3++;
                                nOutIdx++;
                            }
                            nUint24 = 0;
                            }
                        }

                        return taBytes.buffer;
                    }


                }

                zoomFactor: units.gu(1) / 8.4
                url: Qt.resolvedUrl('www/index.html')

                onFileDialogRequested: function(request) {
                    request.accepted = true;
                    var importPage = mainPageStack.push(Qt.resolvedUrl("ImportPage.qml"),{"contentType": ContentType.All, "handler": ContentHandler.Source});
                    importPage.imported.connect(function(fileUrl) {
                        request.dialogAccept(String(fileUrl).replace("file://", ""));
                        mainPageStack.push(pageMain)
                    });
                }
                onNewViewRequested: {
                    request.action = WebEngineNavigationRequest.IgnoreRequest
                    if(request.userInitiated) {
                        Qt.openUrlExternally(request.requestedUrl)
                    }
                }
                onFeaturePermissionRequested: function(url, feature) {
                    grantFeaturePermission(url, feature, true);
                }
            }

            Component {
                id: downloadedMessage
                Popover {
                    id: popover
                    Rectangle {
                        color: "green"
                        width: popover.width
                        height: units.gu(10)
                        anchors {
                            centerIn: parent
                            margins: units.gu(1)
                        }

                        Column {
                            anchors {
                                centerIn: parent
                            }
                            spacing: units.gu(1.5)
                            width: parent.width - units.gu(4)

                            Label {
                                text: "Download completed."
                                anchors.horizontalCenter: parent.horizontalCenter
                                color: "white"
                            }
                            Icon {
                                color: theme.palette.normal.baseText
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: width
                                name: "share"
                                width: units.gu(3)

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        popover.hide();
                                        fileExporter.visible = true
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Component {
                id: fileExporterItemComponent
                ContentItem {}
            }

            ContentPeerPicker {
                id: fileExporter

                property string file : ""
                property var activeTransfer : null

                anchors.fill: parent
                visible: false

                handler: ContentHandler.Destination
                contentType: ContentType.Documents

                onPeerSelected: {
                    activeTransfer = peer.request();
                    activeTransfer.items = [ fileExporterItemComponent.createObject(root, {"url": file}) ];
                    activeTransfer.state = ContentTransfer.Charged;
                    visible = false
                }

                onCancelPressed: {
                    visible = false
                }
            }

        }

    }
}
